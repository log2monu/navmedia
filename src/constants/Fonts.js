import {scale} from './Scale';

const family={
  light: 'Roboto-Light',
  regular:'Robot-Regular',
  bold: 'Roboto-Bold',
  medium: 'Roboto-Medium',
  condensed_regular: 'RobotoCondensed-Regular',
  condensed_bold: 'RobotoCondensed-Bold',
  condensed_light: 'RobotoCondensed-Light',
  thin: 'Roboto-Thin',
  italic: 'Roboto-Italic'
}

const size={
  hl1:  scale(32),
  hl2:  scale(30),
  hl3:  scale(28),
  hl4:  scale(26),
  hl5:  scale(24),
  hl6: scale(22),
  h1: scale(20),
  h2: scale(19),
  h3: scale(18),
  h4: scale(17),
  h5: scale(16),
  h6: scale(15),
  h7: scale(14),
  h8: scale(13),
  h9: scale(12),
  h10: scale(11),
  h11: scale(10),
  h12: scale(9),
  h13: scale(8),
}

export {family,size};