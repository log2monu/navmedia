export const getToken = state => state.token;
export const getRefreshToken = state => state.refresh_token;

export const base_url = "http://navmedia.sketchappsolutions.com";

// export const razor_order_url = "https://api.razorpay.com/v1/orders/";

// export const razor_api_key = "rzp_test_jz39vLdtyUKvj4";
// export const razor_api_sec_key = "q45Pg8CzIHpEP2fg0b27A1hM";

// export const fcm_server_key = "AIzaSyBH805UlgibIF1oM5L9epBH9Ii6DWgKcQ0";
// export const fcm_sender_id = "484516368232";


export const getId = url => {
  var regExp = /^.*(youtu.be\/|v\/|u\/\w\/|embed\/|watch\?v=|\&v=)([^#\&\?]*).*/;
  var match = url.match(regExp);

  if (match && match[2].length == 11) {
    return match[2];
  } else {
    return "error";
  }
};
