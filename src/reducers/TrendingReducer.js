import { FETCH_TRENDING, FETCH_TRENDING_SUCCESS, FETCH_TRENDING_ERROR, FETCH_TRENDING_LOADING, FETCH_FEATURED, FETCH_FEATURED_SUCCESS, FETCH_FEATURED_ERROR, FETCH_FEATURED_LOADING } from '../actions/ActionTypes';
  
  const INIT_STATE = {
    trendingList: [],
    error: null,
    loading: false,
    success: null
  };
  
  export const trending = (state = INIT_STATE, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_TRENDING:
        return state;
        break;
      case FETCH_TRENDING_SUCCESS:
        return {
          ...state,
          trendingList: action.payload.page==1?action.payload.data:[...state.data,...action.payload.data],
          success: true
        };
        break;
      case FETCH_TRENDING_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_TRENDING_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };
  

  const INIT_STATE_FEATURED = {
    featuredList: [],
    error: null,
    loading: false,
  };
  
  export const featured = (state = INIT_STATE_FEATURED, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_FEATURED:
        return state;
        break;
      case FETCH_FEATURED_SUCCESS:
        return {
          ...state,
          featuredList: action.data,
        };
        break;
      case FETCH_FEATURED_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_FEATURED_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };
  