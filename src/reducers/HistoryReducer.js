import { FETCH_HISTORY, FETCH_HISTORY_SUCCESS, FETCH_HISTORY_ERROR, FETCH_HISTORY_LOADING, DELETE_HISTORY, DELETE_HISTORY_SUCCESS, DELETE_HISTORY_ERROR, DELETE_HISTORY_LOADING, DELETE_SINGLE_HISTORY, DELETE_SINGLE_HISTORY_SUCCESS, DELETE_SINGLE_HISTORY_ERROR, DELETE_SINGLE_HISTORY_LOADING } from '../actions/ActionTypes';

const INIT_STATE = {
  data: [],
  error: null,
  loading: false,
  success: null
};

export const history = (state = INIT_STATE, action) => {
  if (!action) {
    return state;
  }

  switch (action.type) {
    case FETCH_HISTORY:
      return state;
      break;
    case FETCH_HISTORY_SUCCESS:
      return {
        ...state,
        data: action.data,
        success: true
      };
      break;
    case FETCH_HISTORY_ERROR:
      return {
        ...state,
        error: action.state,
      };
      break;
    case FETCH_HISTORY_LOADING:
      return {
        ...state,
        loading: action.state,
      };
      break;
    default:
      return state;
  }
};

const INIT_STATE_DEL = {
  data: [],
  error: null,
  loading: false,
  success: null
};

export const del_history = (state = INIT_STATE_DEL, action) => {
  if (!action) {
    return state;
  }

  switch (action.type) {
    case DELETE_HISTORY:
      return state;
      break;
    case DELETE_HISTORY_SUCCESS:
      return {
        ...state,
        data: action.data,
        success: true
      };
      break;
    case DELETE_HISTORY_ERROR:
      return {
        ...state,
        error: action.state,
      };
      break;
    case DELETE_HISTORY_LOADING:
      return {
        ...state,
        loading: action.state,
      };
      break;
    default:
      return state;
  }
};

const INIT_STATE_DEL1 = {
  data: [],
  error: null,
  loading: false,
  success: null
};

export const del_single_history = (state = INIT_STATE_DEL1, action) => {
  if (!action) {
    return state;
  }

  switch (action.type) {
    case DELETE_SINGLE_HISTORY:
      return state;
      break;
    case DELETE_SINGLE_HISTORY_SUCCESS:
      return {
        ...state,
        data: action.data,
        success: true
      };
      break;
    case DELETE_SINGLE_HISTORY_ERROR:
      return {
        ...state,
        error: action.state,
      };
      break;
    case DELETE_SINGLE_HISTORY_LOADING:
      return {
        ...state,
        loading: action.state,
      };
      break;
    default:
      return state;
  }
};


