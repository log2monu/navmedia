import {
    FETCH_PROFILE,
    POST_CREATE_CHANNEL,
    POST_CREATE_CHANNEL_ERROR,
    POST_CREATE_CHANNEL_LOADING,
    POST_CREATE_CHANNEL_SUCCESS,
    FETCH_PROFILE_ERROR,
    FETCH_PROFILE_LOADING,
    FETCH_PROFILE_SUCCESS,
    POST_UPDATE_PROFILE,
    POST_UPDATE_PROFILE_ERROR,
    POST_UPDATE_PROFILE_LOADING,
    POST_UPDATE_PROFILE_SUCCESS,
    DELETE_USER,
    DELETE_USER_ERROR,
    DELETE_USER_LOADING,
    DELETE_USER_SUCCESS
  } from "../actions/ActionTypes";
  
  const INIT_STATE_1 = {
    success: null,
    error: null,
    loading: false,
    userData: null
  };
  export const profile = (state = INIT_STATE_1, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case FETCH_PROFILE:
        return state;
      case FETCH_PROFILE_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
      case FETCH_PROFILE_LOADING:
        return {
          ...state,
          loading: action.state
        };
      case FETCH_PROFILE_SUCCESS:
        return {
          ...state,
          userData: action.data,
          success: true
        };
      default:
        return state;
    }
  };

  

  const INIT_STATE_4 = {
    success: null,
    error: null,
    loading: false,
    data: null
  };
  export const create_channel = (state = INIT_STATE_4, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case POST_CREATE_CHANNEL:
        return state;
      case POST_CREATE_CHANNEL_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
      case POST_CREATE_CHANNEL_LOADING:
        return {
          ...state,
          loading: action.state
        };
      case POST_CREATE_CHANNEL_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
      default:
        return state;
    }
  };

  const INIT_STATE_3 = {
    success: null,
    error: null,
    loading: false,
    userData: null
  };
  export const update_profile = (state = INIT_STATE_3, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case POST_UPDATE_PROFILE:
        return state;
      case POST_UPDATE_PROFILE_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
      case POST_UPDATE_PROFILE_LOADING:
        return {
          ...state,
          loading: action.state
        };
      case POST_UPDATE_PROFILE_SUCCESS:
        return {
          ...state,
          userData: action.data,
          success: true
        };
      default:
        return state;
    }
  };

  const INIT_STATE_DELETE = {
    success: null,
    error: null,
    loading: false,
    data: null
  };
  export const delete_user = (state = INIT_STATE_DELETE, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case DELETE_USER:
        return state;
      case DELETE_USER_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
      case DELETE_USER_LOADING:
        return {
          ...state,
          loading: action.state
        };
      case DELETE_USER_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
      default:
        return state;
    }
  };