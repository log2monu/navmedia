import { FETCH_VIDEO, FETCH_VIDEO_SUCCESS, FETCH_VIDEO_ERROR, FETCH_VIDEO_LOADING, POST_VIDEO, POST_VIDEO_SUCCESS, POST_VIDEO_ERROR, POST_VIDEO_LOADING, DEL_VIDEO, DEL_VIDEO_SUCCESS, DEL_VIDEO_ERROR, DEL_VIDEO_LOADING } from '../actions/ActionTypes';
  
  const INIT_STATE = {
    videoList: [],
    error: null,
    loading: false,
    success: null
  };
  
  export const video = (state = INIT_STATE, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_VIDEO:
        return state;
        break;
      case FETCH_VIDEO_SUCCESS:
        return {
          ...state,
          videoList: action.data,
          success: true
        };
        break;
      case FETCH_VIDEO_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
        break;
      case FETCH_VIDEO_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };
  
    
  const INIT_STATE_UPLOAD = {
    data: [],
    error: null,
    loading: false,
    success: null
  };
  
  export const video_upload = (state = INIT_STATE_UPLOAD, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case POST_VIDEO:
        return state;
        break;
      case POST_VIDEO_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
        break;
      case POST_VIDEO_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case POST_VIDEO_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_DEL = {
    data: [],
    error: null,
    loading: false,
    success :null
  };
  
  export const del_video = (state = INIT_STATE_DEL, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case DEL_VIDEO:
        return state;
        break;
      case DEL_VIDEO_SUCCESS:
        return {
          ...state,
          data: action.data,
          success :true
        };
        break;
      case DEL_VIDEO_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case DEL_VIDEO_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };
  