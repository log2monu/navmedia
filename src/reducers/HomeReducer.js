import { FETCH_HOME, FETCH_HOME_SUCCESS, FETCH_HOME_ERROR, FETCH_HOME_LOADING } from '../actions/ActionTypes';
  
  const INIT_STATE = {
    homeData: [],
    error: null,
    loading: false,
    success: null
  };
  
  export const home = (state = INIT_STATE, action) => {
    // console.log("bvhs :", action)
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_HOME:
        return state;
        break;
      case FETCH_HOME_SUCCESS:
        return {
          ...state,
          homeData: action.data,
          success: true
        };
        break;
      case FETCH_HOME_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
        break;
      case FETCH_HOME_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };
  