import { FETCH_CHANNEL_DETAILS, FETCH_CHANNEL_DETAILS_SUCCESS, FETCH_CHANNEL_DETAILS_LOADING, FETCH_CHANNEL_DETAILS_ERROR, FETCH_CHANNELS, FETCH_CHANNELS_SUCCESS, FETCH_CHANNELS_ERROR, FETCH_CHANNELS_LOADING, FETCH_MY_CHANNELS, FETCH_MY_CHANNELS_SUCCESS, FETCH_MY_CHANNELS_ERROR, FETCH_MY_CHANNELS_LOADING, FETCH_CHANNEL_POPULAR, FETCH_CHANNEL_POPULAR_SUCCESS, FETCH_CHANNEL_POPULAR_ERROR, FETCH_CHANNEL_POPULAR_LOADING, PUT_SUBSCRIBE, PUT_SUBSCRIBE_SUCCESS, PUT_SUBSCRIBE_ERROR, PUT_SUBSCRIBE_LOADING, DEL_CHANNEL, DEL_CHANNEL_SUCCESS, DEL_CHANNEL_ERROR, DEL_CHANNEL_LOADING, PUT_CHANNEL, PUT_CHANNEL_SUCCESS, PUT_CHANNEL_ERROR, PUT_CHANNEL_LOADING } from '../actions/ActionTypes';
  
  const INIT_STATE = {
    channelsList: [],
    error: null,
    loading: false,
  };
  
  export const channels = (state = INIT_STATE, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_CHANNELS:
        return state;
        break;
      case FETCH_CHANNELS_SUCCESS:
        return {
          ...state,
          channelsList: action.data,
        };
        break;
      case FETCH_CHANNELS_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_CHANNELS_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  
  import { FETCH_CHANNEL_CONTENT, FETCH_CHANNEL_CONTENT_SUCCESS, FETCH_CHANNEL_CONTENT_ERROR, FETCH_CHANNEL_CONTENT_LOADING } from '../actions/ActionTypes';
  
  const INIT_STATE_CONTENT = {
    data: [],
    error: null,
    loading: false,
  };
  
  export const channel_content = (state = INIT_STATE_CONTENT, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_CHANNEL_CONTENT:
        return state;
        break;
      case FETCH_CHANNEL_CONTENT_SUCCESS:
        return {
          ...state,
          data: action.data,
        };
        break;
      case FETCH_CHANNEL_CONTENT_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_CHANNEL_CONTENT_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_DETAILS = {
    data: [],
    error: null,
    loading: false,
  };

  export const channel_details = (state = INIT_STATE_DETAILS, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_CHANNEL_DETAILS:
        return state;
        break;
      case FETCH_CHANNEL_DETAILS_SUCCESS:
        return {
          ...state,
          data: action.data,
        };
        break;
      case FETCH_CHANNEL_DETAILS_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_CHANNEL_DETAILS_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_POPULAR = {
    data: [],
    error: null,
    loading: false,
  };

  export const channel_popular = (state = INIT_STATE_POPULAR, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_CHANNEL_POPULAR:
        return state;
        break;
      case FETCH_CHANNEL_POPULAR_SUCCESS:
        return {
          ...state,
          data: action.data,
        };
        break;
      case FETCH_CHANNEL_POPULAR_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_CHANNEL_POPULAR_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_LIST = {
    myChannels: [],
    error: null,
    loading: false,
  };
  
  export const my_channels = (state = INIT_STATE_LIST, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_MY_CHANNELS:
        return state;
        break;
      case FETCH_MY_CHANNELS_SUCCESS:
        return {
          ...state,
          myChannels: action.data,
        };
        break;
      case FETCH_MY_CHANNELS_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_MY_CHANNELS_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_SUB = {
    data: [],
    error: null,
    loading: false,
    success: null
  };
  
  export const subscribe = (state = INIT_STATE_SUB, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case PUT_SUBSCRIBE:
        return state;
        break;
      case PUT_SUBSCRIBE_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
        break;
      case PUT_SUBSCRIBE_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case PUT_SUBSCRIBE_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };


  const INIT_STATE_DEL = {
    data: [],
    error: null,
    loading: false,
    success :null
  };
  
  export const del_channel = (state = INIT_STATE_DEL, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case DEL_CHANNEL:
        return state;
        break;
      case DEL_CHANNEL_SUCCESS:
        return {
          ...state,
          data: action.data,
          success :true
        };
        break;
      case DEL_CHANNEL_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case DEL_CHANNEL_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };
  

  const INIT_STATE_UPDT = {
    data: [],
    error: null,
    loading: false,
    success :null
  };
  
  export const update_channel = (state = INIT_STATE_UPDT, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case PUT_CHANNEL:
        return state;
        break;
      case PUT_CHANNEL_SUCCESS:
        return {
          ...state,
          data: action.data,
          success :true
        };
        break;
      case PUT_CHANNEL_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case PUT_CHANNEL_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };