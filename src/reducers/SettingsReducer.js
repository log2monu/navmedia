import { POST_FEEDBACK, POST_FEEDBACK_SUCCESS, POST_FEEDBACK_ERROR, POST_FEEDBACK_LOADING } from '../actions/ActionTypes';
  
  const INIT_STATE = {
    data: [],
    error: null,
    loading: false,
    success: null
  };
  
  export const feedback = (state = INIT_STATE, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case POST_FEEDBACK:
        return state;
        break;
      case POST_FEEDBACK_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
        break;
      case POST_FEEDBACK_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case POST_FEEDBACK_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };
  