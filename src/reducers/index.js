import { persistCombineReducers } from "redux-persist";
import AsyncStorage from '@react-native-community/async-storage';
import login from './LoginReducer';
import { user, user_info } from './UserReducer';
import {token, refresh_token} from './TokenReducer';
import { notifications } from './NotificationReducer';
import { profile, create_channel, update_profile, delete_user } from './ProfileReducer';
import { banner_upload, ad_upload, ad_banner_1, ad_banner_2, ad_banner_3 } from './AdReducer';
import { trending, featured } from './TrendingReducer';
import { channels, my_channels, channel_content, channel_details, channel_popular, subscribe, del_channel, update_channel } from './ChannelsReducer';
import { category, cat_sub_category, cat_subcat_content, ser_sub_category, service_category, blog } from './CategoryReducer';
import { feedback } from './SettingsReducer';
import { video, video_upload, del_video } from './VideoReducer';
import { like, dislike, comments, post_comment } from "./VideoControlsReducer";
import { home } from './HomeReducer';
import { connect_image, connect_video } from "./ConnectReducer";
import { history, del_history, del_single_history} from "./HistoryReducer";
import { search_video } from "./SearchReducer";
import { premium } from "./PremiumReducer";
import logout from './LogoutReducer';

const config = {
    key: "primary",
    storage: AsyncStorage,
    blacklist: [
        "login",
        "notifications",
        "profile",
        "home",
        "trending",
        "featured",
        "channels",
        "my_channels",
        "channels_content",
        "channel_details",
        "channel_popular",
        "category",
        "cat_sub_category",
        "cat_subcat_content",
        "ser_sub_category",
        "service_category",
        "blog",
        "feedback",
        "video",
        "like",
        "dislike",
        "comments",
        "post_comment",
        "video_upload",
        "banner_upload",
        "ad_upload",
        "ad_banner_1",
        "ad_banner_2",
        "ad_banner_3",
        "create_channel",
        "del_channel", 
        "update_channel",
        "del_video",
        "subscribe",
        "connect_image",
        "connect_video",
        "search_video",
        "history",
        "del_history",
        "del_single_history",
        "delete_user",
        "logout",
    ]
};    

const AppReducer = persistCombineReducers(config, {
    login,
    notifications,
    token,
    premium,
    refresh_token,
    user,
    user_info,
    home,
    profile,
    update_profile,
    trending,
    featured,
    channels,
    my_channels,
    channel_content,
    channel_details,
    channel_popular,
    category,
    cat_sub_category,
    cat_subcat_content,
    ser_sub_category,
    service_category,
    blog,
    feedback,
    video,
    like,
    dislike,
    comments,
    post_comment,
    video_upload,
    banner_upload,
    ad_upload,
    ad_banner_1,
    ad_banner_2,
    ad_banner_3,
    create_channel,
    del_channel, 
    update_channel,
    del_video,
    subscribe,
    connect_image,
    connect_video,
    search_video,
    history,
    del_history,
    del_single_history,
    delete_user,
    logout
});    

export default AppReducer;