import {
    POST_BANNER,
    POST_BANNER_ERROR,
    POST_BANNER_LOADING,
    POST_BANNER_SUCCESS,
    POST_AD_VIDEO,
    POST_AD_VIDEO_ERROR,
    POST_AD_VIDEO_LOADING,
    POST_AD_VIDEO_SUCCESS,
    FETCH_AD_BANNER,
    FETCH_AD_BANNER_ERROR,
    FETCH_AD_BANNER_LOADING,
    FETCH_AD_BANNER_SUCCESS,
    FETCH_AD_BANNER_1,
    FETCH_AD_BANNER_1_ERROR,
    FETCH_AD_BANNER_1_LOADING,
    FETCH_AD_BANNER_1_SUCCESS,
    FETCH_AD_BANNER_3,
    FETCH_AD_BANNER_3_LOADING,
    FETCH_AD_BANNER_3_SUCCESS,
    FETCH_AD_BANNER_3_ERROR,
    FETCH_AD_BANNER_2,
    FETCH_AD_BANNER_2_LOADING,
    FETCH_AD_BANNER_2_SUCCESS,
    FETCH_AD_BANNER_2_ERROR,
  } from "../actions/ActionTypes";

const INIT_STATE_1 = {
    success: null,
    error: null,
    loading: false,
    data: null
  };
  export const banner_upload = (state = INIT_STATE_1, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case POST_BANNER:
        return state;
      case POST_BANNER_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
      case POST_BANNER_LOADING:
        return {
          ...state,
          loading: action.state
        };
      case POST_BANNER_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
      default:
        return state;
    }
  };
  


  const INIT_STATE_2 = {
    success: null,
    error: null,
    loading: false,
    data: null
  };
  export const ad_upload = (state = INIT_STATE_2, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case POST_AD_VIDEO:
        return state;
      case POST_AD_VIDEO_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
      case POST_AD_VIDEO_LOADING:
        return {
          ...state,
          loading: action.state
        };
      case POST_AD_VIDEO_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
      default:
        return state;
    }
  };


  const INIT_STATE_3 = {
    success: null,
    error: null,
    loading: false,
    data: null
  };
  export const ad_banner_1 = (state = INIT_STATE_3, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case FETCH_AD_BANNER_1:
        return state;
      case FETCH_AD_BANNER_1_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
      case FETCH_AD_BANNER_1_LOADING:
        return {
          ...state,
          loading: action.state
        };
      case FETCH_AD_BANNER_1_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
      default:
        return state;
    }
  };

  const INIT_STATE_4 = {
    success: null,
    error: null,
    loading: false,
    data: null
  };
  export const ad_banner_2 = (state = INIT_STATE_4, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case FETCH_AD_BANNER_2:
        return state;
      case FETCH_AD_BANNER_2_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
      case FETCH_AD_BANNER_2_LOADING:
        return {
          ...state,
          loading: action.state
        };
      case FETCH_AD_BANNER_2_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
      default:
        return state;
    }
  };

  const INIT_STATE_5 = {
    success: null,
    error: null,
    loading: false,
    data: null
  };
  export const ad_banner_3 = (state = INIT_STATE_5, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case FETCH_AD_BANNER_3:
        return state;
      case FETCH_AD_BANNER_3_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
      case FETCH_AD_BANNER_3_LOADING:
        return {
          ...state,
          loading: action.state
        };
      case FETCH_AD_BANNER_3_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
      default:
        return state;
    }
  };