import { PREMIUM_STATUS } from '../actions/ActionTypes';

const INIT_STATE = false;
export const premium = (state = INIT_STATE, action) => {
  if (!action) {
    return state;
  }
  switch (action.type) {
    case PREMIUM_STATUS:
      return action.data;

    default:
      return state;
  }
};