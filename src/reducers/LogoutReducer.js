import {
    POST_LOGOUT,
    POST_LOGOUT_ERROR,
    POST_LOGOUT_LOADING,
    POST_LOGOUT_SUCCESS
  } from "../actions/ActionTypes";
  
  const INIT_STATE = {
    logout: null,
    loading: false,
    success: null,
    error: null
  };
  export default (state = INIT_STATE, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case POST_LOGOUT:
        return state;
  
      case POST_LOGOUT_ERROR:
        return {
          ...state,
          error: action.state,
        };
  
      case POST_LOGOUT_LOADING:
        return {
          ...state,
          loading: action.state
        };
  
      case POST_LOGOUT_SUCCESS:
        return {
          ...state,
          logout: action.data,
          success: true
        };
  
      default:
        return state;
    }
  };
  