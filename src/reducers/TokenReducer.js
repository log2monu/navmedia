import { TOKEN_SUCCESS, TOKEN_EMPTY, REFRESH_TOKEN_SUCCESS, REFRESH_TOKEN_EMPTY } from "../actions/ActionTypes";

const INIT_STATE_TOKEN = null;
export const token = (state = INIT_STATE_TOKEN, action) => {
  if (!action) {
    return state;
  }
  switch (action.type) {
    case TOKEN_SUCCESS:
      return action.data;

    case TOKEN_EMPTY:
      return state;

    default:
      return state;
  }
};

const INIT_STATE_REFRESH_TOKEN = null;

export const refresh_token = (state = INIT_STATE_REFRESH_TOKEN, action) => {
  if (!action) {
    return state;
  }
  switch (action.type) {
    case REFRESH_TOKEN_SUCCESS:
      return action.data;

    case REFRESH_TOKEN_EMPTY:
      return null;

    default:
      return state;
  }
};