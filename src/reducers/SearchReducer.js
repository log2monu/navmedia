import { POST_SEARCH, POST_SEARCH_SUCCESS, POST_SEARCH_ERROR, POST_SEARCH_LOADING } from '../actions/ActionTypes';

const INIT_STATE = {
  searchResult: [],
  error: null,
  loading: false,
  success: null
};

export const search_video = (state = INIT_STATE, action) => {
  if (!action) {
    return state;
  }

  switch (action.type) {
    case POST_SEARCH:
      return state;
      break;
    case POST_SEARCH_SUCCESS:
      return {
        ...state,
        searchResult: action.data,
        success: true
      };
      break;
    case POST_SEARCH_ERROR:
      return {
        ...state,
        error: action.state,
      };
      break;
    case POST_SEARCH_LOADING:
      return {
        ...state,
        loading: action.state,
      };
      break;
    default:
      return state;
  }
};
