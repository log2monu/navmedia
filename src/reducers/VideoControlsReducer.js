import { PUT_LIKE, PUT_LIKE_SUCCESS, PUT_LIKE_ERROR, PUT_LIKE_LOADING, PUT_DISLIKE, PUT_DISLIKE_SUCCESS, PUT_DISLIKE_ERROR, PUT_DISLIKE_LOADING, FETCH_COMMENTS, FETCH_COMMENTS_SUCCESS, FETCH_COMMENTS_ERROR, FETCH_COMMENTS_LOADING, POST_COMMENT, POST_COMMENT_SUCCESS, POST_COMMENT_ERROR, POST_COMMENT_LOADING } from '../actions/ActionTypes';
  
  const INIT_STATE = {
    data: null,
    error: null,
    loading: false,
    success: null
  };
  
  export const like = (state = INIT_STATE, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case PUT_LIKE:
        return state;
        break;
      case PUT_LIKE_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
        break;
      case PUT_LIKE_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
        break;
      case PUT_LIKE_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };
  
    
  const INIT_STATE_1 = {
    data: [],
    error: null,
    loading: false,
    success: null
  };
  
  export const dislike = (state = INIT_STATE_1, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case PUT_DISLIKE:
        return state;
        break;
      case PUT_DISLIKE_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
        break;
      case PUT_DISLIKE_ERROR:
        return {
          ...state,
          error: false,
        };
        break;
      case PUT_DISLIKE_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_2 = {
    data: [],
    error: null,
    loading: false,
    success: null,
  };
  
  export const comments = (state = INIT_STATE_2, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_COMMENTS:
        return state;
        break;
      case FETCH_COMMENTS_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
        break;
      case FETCH_COMMENTS_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_COMMENTS_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_3 = {
    data: [],
    error: null,
    success: null,
    loading: false,
  };
  
  export const post_comment = (state = INIT_STATE_3, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case POST_COMMENT:
        return state;
        break;
      case POST_COMMENT_SUCCESS:
        return {
          ...state,
          data: action.data,
          success: true
        };
        break;
      case POST_COMMENT_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case POST_COMMENT_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };
  