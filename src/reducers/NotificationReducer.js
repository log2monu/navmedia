import {
  FETCH_NOTIFICATIONS,
  FETCH_NOTIFICATIONS_ERROR,
  FETCH_NOTIFICATIONS_LOADING,
  FETCH_NOTIFICATIONS_SUCCESS,
} from '../actions/ActionTypes';

const INIT_STATE = {
  notificationList: [],
  error: null,
  loading: false,
};

export const notifications = (state = INIT_STATE, action) => {
  if (!action) {
    return state;
  }

  switch (action.type) {
    case FETCH_NOTIFICATIONS:
      return state;
      break;
    case FETCH_NOTIFICATIONS_SUCCESS:
      return {
        ...state,
        notificationList: action.data,
      };
      break;
    case FETCH_NOTIFICATIONS_ERROR:
      return {
        ...state,
        error: action.state,
      };
      break;
    case FETCH_NOTIFICATIONS_LOADING:
      return {
        ...state,
        loading: action.state,
      };
      break;
    default:
      return state;
  }
};
