import { CONNECT_IMAGE, CONNECT_IMAGE_ERROR, CONNECT_IMAGE_LOADING, CONNECT_IMAGE_SUCCESS, CONNECT_VIDEO, CONNECT_VIDEO_ERROR, CONNECT_VIDEO_LOADING, CONNECT_VIDEO_SUCCESS } from "../actions/ActionTypes";

const INIT_STATE_1 = {
  success: null,
  error: null,
  loading: false,
  data: null
};
export const connect_image = (state = INIT_STATE_1, action) => {
  if (!action) {
    return state;
  }
  switch (action.type) {
    case CONNECT_IMAGE:
      return state;
    case CONNECT_IMAGE_ERROR:
      return {
        ...state,
        error: action.state,
        success: false
      };
    case CONNECT_IMAGE_LOADING:
      return {
        ...state,
        loading: action.state
      };
    case CONNECT_IMAGE_SUCCESS:
      return {
        ...state,
        data: action.data,
        success: true
      };
    default:
      return state;
  }
};



const INIT_STATE_2 = {
  success: null,
  error: null,
  loading: false,
  data: null
};
export const connect_video = (state = INIT_STATE_2, action) => {
  if (!action) {
    return state;
  }
  switch (action.type) {
    case CONNECT_VIDEO:
      return state;
    case CONNECT_VIDEO_ERROR:
      return {
        ...state,
        error: action.state,
        success: false
      };
    case CONNECT_VIDEO_LOADING:
      return {
        ...state,
        loading: action.state
      };
    case CONNECT_VIDEO_SUCCESS:
      return {
        ...state,
        data: action.data,
        success: true
      };
    default:
      return state;
  }
};