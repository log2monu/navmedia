import { FETCH_CATEGORY, FETCH_CATEGORY_SUCCESS, FETCH_CATEGORY_ERROR, FETCH_CATEGORY_LOADING, FETCH_SERVICE_CATEGORY, FETCH_SERVICE_CATEGORY_SUCCESS, FETCH_SERVICE_CATEGORY_ERROR, FETCH_SERVICE_CATEGORY_LOADING, FETCH_SUB_CATEGORY, FETCH_SUB_CATEGORY_SUCCESS, FETCH_SUB_CATEGORY_ERROR, FETCH_SUB_CATEGORY_LOADING, FETCH_BLOG, FETCH_BLOG_SUCCESS, FETCH_BLOG_ERROR, FETCH_BLOG_LOADING, FETCH_CAT_SUB_CATEGORY, FETCH_CAT_SUB_CATEGORY_SUCCESS, FETCH_CAT_SUB_CATEGORY_ERROR, FETCH_CAT_SUB_CATEGORY_LOADING, FETCH_SER_SUB_CATEGORY, FETCH_SER_SUB_CATEGORY_SUCCESS, FETCH_SER_SUB_CATEGORY_ERROR, FETCH_SER_SUB_CATEGORY_LOADING, FETCH_SUB_CAT_CONTENT, FETCH_SUB_CAT_CONTENT_SUCCESS, FETCH_SUB_CAT_CONTENT_ERROR, FETCH_SUB_CAT_CONTENT_LOADING } from '../actions/ActionTypes';
  
  const INIT_STATE = {
    categoryList: [],
    error: null,
    loading: false,
  };
  
  export const category = (state = INIT_STATE, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_CATEGORY:
        return state;
        break;
      case FETCH_CATEGORY_SUCCESS:
        return {
          ...state,
          categoryList: action.data,
        };
        break;
      case FETCH_CATEGORY_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_CATEGORY_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_1 = {
    subCategoryList: [],
    error: null,
    loading: false,
  };
  
  export const cat_sub_category = (state = INIT_STATE_1, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_CAT_SUB_CATEGORY:
        return state;
        break;
      case FETCH_CAT_SUB_CATEGORY_SUCCESS:
        return {
          ...state,
          subCategoryList: action.data,
        };
        break;
      case FETCH_CAT_SUB_CATEGORY_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_CAT_SUB_CATEGORY_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_SER = {
    subCategoryList: [],
    error: null,
    loading: false,
  };
  
  export const ser_sub_category = (state = INIT_STATE_SER, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_SER_SUB_CATEGORY:
        return state;
        break;
      case FETCH_SER_SUB_CATEGORY_SUCCESS:
        return {
          ...state,
          subCategoryList: action.data,
        };
        break;
      case FETCH_SER_SUB_CATEGORY_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_SER_SUB_CATEGORY_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };
  

  const INIT_STATE_2 = {
    serviceCategoryList: [],
    error: null,
    loading: false,
  };

  export const service_category = (state = INIT_STATE_2, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_SERVICE_CATEGORY:
        return state;
        break;
      case FETCH_SERVICE_CATEGORY_SUCCESS:
        return {
          ...state,
          serviceCategoryList: action.data,
        };
        break;
      case FETCH_SERVICE_CATEGORY_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_SERVICE_CATEGORY_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_3 = {
    blogData: [],
    error: null,
    loading: false,
  };

  export const blog = (state = INIT_STATE_3, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_BLOG:
        return state;
        break;
      case FETCH_BLOG_SUCCESS:
        return {
          ...state,
          blogData: action.data,
        };
        break;
      case FETCH_BLOG_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_BLOG_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_4 = {
    data: [],
    error: null,
    loading: false,
  };

  export const cat_subcat_content = (state = INIT_STATE_4, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_SUB_CAT_CONTENT:
        return state;
        break;
      case FETCH_SUB_CAT_CONTENT_SUCCESS:
        return {
          ...state,
          data: action.data,
        };
        break;
      case FETCH_SUB_CAT_CONTENT_ERROR:
        return {
          ...state,
          error: action.state,
        };
        break;
      case FETCH_SUB_CAT_CONTENT_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };