import { FETCH_USER, CLEAR_USER, FETCH_USER_SUCCESS, FETCH_USER_ERROR, FETCH_USER_LOADING, USER_SUCCESS, USER_EMPTY } from "../actions/ActionTypes";

const INIT_STATE = {
    userData: [],
    loading: false,
    error: null,
    success: null,
  };
  
  export const user = (state = INIT_STATE, action) => {
    if (!action) {
      return state;
    }
  
    switch (action.type) {
      case FETCH_USER:
        return state;
      case CLEAR_USER:
        return INIT_STATE;
      case FETCH_USER_SUCCESS:
        return {
          ...state,
          UserData: action.data,
          success: true
        };
        break;
      case FETCH_USER_ERROR:
        return {
          ...state,
          error: action.state,
          success: false
        };
        break;
      case FETCH_USER_LOADING:
        return {
          ...state,
          loading: action.state,
        };
        break;
      default:
        return state;
    }
  };

  const INIT_STATE_USER_INFO = null;
  export const user_info = (state = INIT_STATE_USER_INFO, action) => {
    if (!action) {
      return state;
    }
    switch (action.type) {
      case USER_SUCCESS:
        return action.data;

      case USER_EMPTY:
        return null;

      default:
        return state;
    }
  };
  