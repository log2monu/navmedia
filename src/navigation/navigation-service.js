import { NavigationActions } from 'react-navigation'

let _navigator;

function setNavigator(ref) {
    _navigator = ref;
}

function navigate(routeName, params) {
    _navigator.dispatch(
        NavigationActions.navigate({
            routeName,
            params,
        })
    );
}

export default {
    navigate,
    setNavigator,
};