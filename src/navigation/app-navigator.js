
import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, SafeAreaView } from 'react-native';
import { createAppContainer } from 'react-navigation'
import Icon1 from 'react-native-vector-icons/Feather'
import Icon2 from 'react-native-vector-icons/Fontisto'
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';
import HomeScreen from "../screens/HomeScreen";
import TrendingScreen from "../screens/TrendingScreen";
import ChannelScreen from "../screens/ChannelScreen";
import CategoriesScreen from "../screens/CategoriesScreen";
import ProfileScreen from "../screens/ProfileScreen";
import SplashScreen from "../screens/SplashScreen";
import LoginScreen from "../screens/LoginScreen";
import SettingsScreen from "../screens/SettingsScreen";
import NotificationScreen from "../screens/NotificationScreen";
import VideoScreen from "../screens/VideoScreen";
import CategoryContent from "../screens/CategoryContent";
import ChannelContent from "../screens/ChannelContent";
import BlogScreen from "../screens/BlogScreen";
import HistoryScreen from '../screens/HistoryScreen';
import CreateChannelScreen from '../screens/CreateChannelScreen';
import UploadVideoScreen from '../screens/UploadVideoScreen';
import { TermsScreen } from '../screens/TermsScreen';
import MyChannelScreen from '../screens/MyChannelScreen';
import CheckScreen from '../screens/CheckScreen';
import LiveVideoScreen from '../screens/LiveVideoScreen';
import SearchScreen from '../screens/SearchScreen';
import MyChannelContent from '../screens/MyChannelContent';
import ServiceSubCategoryScreen from '../screens/ServiceSubCategoryScreen';
import CatSubCategoryScreen from '../screens/CatSubCategoryScreen';


Icon1.loadFont();
Icon2.loadFont();

const HomeStack = createStackNavigator({
  HomeScreen: { screen: HomeScreen },
  ChannelContent: { screen: ChannelContent},
  CheckScreen: { screen: CheckScreen },

},{
  headerMode: 'none',
  initialRouteName: 'HomeScreen'
});
// HomeStack.navigationOptions = ({ navigation }) => {
//   let tabBarVisible = true;
//   if (navigation.state.index >= 1) {
//     tabBarVisible = false;
//   }
//   return {
//     tabBarVisible,
//   };
// };

const TrendingStack = createStackNavigator({
  TrendingScreen: { screen: TrendingScreen },
},{
  headerMode: 'none',
  initialRouteName: 'TrendingScreen'
});
// TrendingStack.navigationOptions = ({ navigation }) => {
//   let tabBarVisible = true;
//   if (navigation.state.index >= 1) {
//     tabBarVisible = false;
//   }
//   return {
//     tabBarVisible,
//   };
// };

const ChannelStack = createStackNavigator({
  ChannelScreen: { screen: ChannelScreen },
  ChannelContent: { screen: ChannelContent},
  MyChannelContent: { screen: MyChannelContent },

},{
  headerMode: 'none',
  initialRouteName: 'ChannelScreen',
});
// ChannelStack.navigationOptions = ({ navigation }) => {
//   let tabBarVisible = true;
//   if (navigation.state.index > 0) {
//     tabBarVisible = false;
//   }
//   return {
//     tabBarVisible,
//   };
// };

const CategoriesStack = createStackNavigator({
  CategoriesScreen: { screen: CategoriesScreen },
  CategoryContent: { screen: CategoryContent},
  ServiceSubCategoryScreen: { screen: ServiceSubCategoryScreen},
  CatSubCategoryScreen: { screen: CatSubCategoryScreen },
  BlogScreen: { screen: BlogScreen }

},{
  headerMode: 'none',
  initialRouteName: 'CategoriesScreen'
});
// CategoriesStack.navigationOptions = ({ navigation }) => {
//   let tabBarVisible = true;
//   if (navigation.state.index > 0) {
//     tabBarVisible = false;
//   }
//   return {
//     tabBarVisible,
//   };
// };

const ProfileStack = createStackNavigator({
  ProfileScreen: { screen: ProfileScreen },
  HistoryScreen: { screen: HistoryScreen },
  CreateChannel: { screen: CreateChannelScreen },
  Settings : { screen: SettingsScreen },
  Notification: { screen: NotificationScreen },
  MyChannel: { screen: MyChannelScreen},
  MyChannelContent: { screen: MyChannelContent },
},{
  headerMode: 'none',
  initialRouteName: 'ProfileScreen'
});
// ProfileStack.navigationOptions = ({ navigation }) => {
//   let tabBarVisible = true;
//   if (navigation.state.index > 0) {
//     tabBarVisible = false;
//   }
//   return {
//     tabBarVisible,
//   };
// };


const BottomTab = createBottomTabNavigator({
  Home: {
    screen: HomeStack,
    navigationOptions: {
      // tabBarLabel: 'HOME',
      tabBarIcon: ({ tintColor }) => (
        <Icon1 name="home" color={tintColor} size={22} />
      )
    }
  },
  Trending: {
    screen: TrendingStack,
    navigationOptions: {
      // tabBarLabel: 'TRENDING',
      tabBarIcon: ({ tintColor }) => (
        <Icon2 name="fire" color={tintColor} size={22} />
      )
    }
  },
  Channel: {
    screen: ChannelStack,
    navigationOptions: {
      // tabBarLabel: 'Channel',
      tabBarIcon: ({ tintColor }) => (
        <Icon1 name="grid" color={tintColor} size={22} />
      )
    }
  },
  Categories: {
    screen: CategoriesStack,
    navigationOptions: {
      // tabBarLabel: 'Categories',
      tabBarIcon: ({ tintColor }) => (
        <Icon1 name="video" color={tintColor} size={22} />
      )
    }
  },
  Profile: {
    screen: ProfileStack,
    navigationOptions: {
      // tabBarLabel: 'PROFILE',
      tabBarIcon: ({ tintColor }) => (
        <Icon1 name="user" color={tintColor} size={22} />
      )
    }
  }
}, {
  initialRouteName: 'Home',
  headerMode: 'none',
    tabBarOptions: {
      showLabel: false,
      animationEnabled:true,
      swipeEnabled:true, 
      activeTintColor: '#0089D0',
      inactiveTintColor: '#a9a9a9',
      style: {
        backgroundColor: 'white',
        borderTopWidth: 0,
        shadowOffset: { width: 5, height: 3 },
        shadowColor: 'black',
        shadowOpacity: 0.5,
        elevation: 5
      }
    }
  }
);

const AppStack  = createStackNavigator({
  SplashScreen: { screen: SplashScreen },
  Login: { screen: LoginScreen },
  Settings : { screen: SettingsScreen },
  Notification: { screen: NotificationScreen},
  BlogScreen: { screen: BlogScreen },
  BottomTab: { screen: BottomTab },
  VideoScreen: { screen: VideoScreen },
  LiveVideo: { screen: LiveVideoScreen},
  TermsScreen: { screen : TermsScreen },
  SearchScreen: { screen: SearchScreen },
  UploadVideo: { screen: UploadVideoScreen },
  CatSubCategory: { screen: CatSubCategoryScreen },
},
{
  initialRouteName:  'SplashScreen',
  headerMode: 'none'
});

export default createAppContainer(AppStack);