
import HomeScreen from "../screens/home-screen/HomeScreen"
import ProfileScreen from "../screens/profile-screen/ProfileScreen";
import NoticeBoardScreen from "../screens/noticeboard-screens/NoticeBoardScreen";
import NoticeBoardDetails from "../screens/noticeboard-screens/NoticeBoardDetails";
import DiaryScreen from "../screens/dairy-screen/DiaryScreen";
import AlbumScreen from "../screens/album-screen/AlbumScreen";
import GalleryScreen from "../screens/gallery-screen/GalleryScreen";
import EventsScreen from "../screens/events-screen/EventsScreen";
import ChatsScreen from "../screens/chats-screen/ChatScreen";
import MyClassScreen from "../screens/my-class-screen/MyClassScreen";
import MessageScreen from "../screens/messages-screen/MessageScreen";
import BottomTab from "../screens/bottom-tabs/BottomTab";
import NavigationDrawer from "../screens/navigation-drawer/NavigationDrawer";
import PersonalInfoScreen from "../screens/personal-info-screen/PersonalInfoScreen";
import TimetableScreen from "../screens/timetable-screen/TimetableScreen";
import MySchoolScreen from "../screens/my-school-screen/MySchoolScreen";
import TransportationScreen from "../screens/transportation-screen/TransportationScreen";
import LeaveScreen from "../screens/leave-screen/LeaveScreen";
import MessageContentScreen from "../screens/message-content-screen/MessageContentScreen";
import ExamScreen from "../screens/exam-screen/ExamScreen";
import ExamTimeTableScreen from "../screens/exam-timetable-screen/ExamTimeTableScreen";
import LibraryScreen from "../screens/library-screen/LibraryScreen";
import AttendanceScreen from "../screens/attendance-screen/AttendanceScreen";
import SchoolCalendarScreen from "../screens/school-calendar-screen/SchoolCalendarScreen";
import HomeWorDetailsScreen from "../screens/home-work-details/HomeWorkDetailsScreen";
import HomeWorkScreen from "../screens/home-work/HomeWorkScreen";
import FeeScreen from "../screens/fee-screen/FeeScreen";
import FeeDetailsScreen from "../screens/fee-details-screen/FeeDetailsScreen";
import NotificationCenterScreen from "../screens/notification-center/NotificationCenterScreen";
import TipsOfTheDayScreen from "../screens/tips-of-the-day/TipsOfTheDayScreen";
 

export default (AppRoutes = {

  HomeScreen: {
    screen: HomeScreen,
    children: []
  },
  ProfileScreen: {
    screen: ProfileScreen,
    children: []
  },
  HomeWork: {
    screen: HomeWorkScreen,
    children: []
  },
  HomeWorkDetails: {
    screen: HomeWorDetailsScreen,
    children: []
  },
  NoticeBoard: {
    screen: NoticeBoardScreen,
    children: []
  },
  NoticeBoardDetails: {
    screen: NoticeBoardDetails,
    children: []
  },
  DiaryScreen: {
    screen: DiaryScreen,
    children: []
  },
  AlbumScreen: {
    screen: AlbumScreen,
    children: []
  },
  GalleryScreen: {
    screen: GalleryScreen,
    children: []
  },
  EventsScreen: {
    screen: EventsScreen,
    children: []
  },
  ChatsScreen: {
    screen: ChatsScreen,
    children: []
  },
  MyClassScreen: {
    screen: MyClassScreen,
    children: []
  },
  MessageScreen: {
    screen: MessageScreen,
    children: []
  },
  BottomTab: {
    screen: BottomTab,
    children: []
  },
  // Navigation: {
  //   screen: NavigationDrawer,
  //   children: []
  // },  
  PersonalInfoScreen: {
    screen: PersonalInfoScreen,
    children: []
  },
  TimetableScreen: {
    screen: TimetableScreen,
    children: []
  },
  MySchoolScreen: {
    screen: MySchoolScreen,
    children: []
  },
  TransportationScreen:{
    screen: TransportationScreen,
    children: []
  },
  LeaveScreen:{
    screen: LeaveScreen,
    children: []
  },
  MessageContent: {
    screen: MessageContentScreen,
    children: []
  },
  ExamScreen: {
    screen: ExamScreen,
    children: []
  },
  ExamTimeTableScreen: {
    screen: ExamTimeTableScreen,
    children: []
  },
  LibraryScreen: {
    screen: LibraryScreen,
    children: []
  },
  AttendanceScreen: {
    screen: AttendanceScreen,
    children: []  
  },
  SchoolCalendarScreen: {
    screen: SchoolCalendarScreen,
    children: []
  },
  FeeScreen: {
    screen: FeeScreen,
    children: []
  },
  FeeDetailsScreen: {
    screen : FeeDetailsScreen,
    children: []
  },
  NotificationCenter: {
    screen: NotificationCenterScreen,
    children: []  
  },
  TipsOfDay: {
    screen: TipsOfTheDayScreen,
    children: []
  }
  
});
