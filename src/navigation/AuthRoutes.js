

import LoginScreen from "../screens/login-screen/LoginScreen";
import OnboardingScreen from "../screens/onboarding-screen/OnboardingScreen";
import OtpScreen from "../screens/otp-screen/OtpScreen";
import SelectStudentScreen from "../screens/select-student/SelectStudentScreen";

export default (AuthRoutes = {


  LoginScreen: {
    screen: LoginScreen,
    children: []
  },
  OtpScreen: {
    screen: OtpScreen,
    children: []
  },
  OnboardingScreen: {
    screen: OnboardingScreen,
    children: []
  },
  SelectStudent: {
    screen: SelectStudentScreen,
    children: []
  }
  
});
