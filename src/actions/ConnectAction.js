import {  CONNECT_IMAGE, CONNECT_VIDEO } from './ActionTypes';

export function postConnectImage(payload) {
    return {
        type: CONNECT_IMAGE,
        payload
    };
}

export function postConnectVideo(payload) {
    return {
        type: CONNECT_VIDEO,
        payload
    };
}
