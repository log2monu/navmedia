import { FETCH_TRENDING, FETCH_VIDEO, DEL_VIDEO } from './ActionTypes';

export function getTrendingVideos() {
    return {
        type: FETCH_TRENDING
    };
}

export function getVideoData(videoId) {
    return {
        type: FETCH_VIDEO,
        videoId
    };
}

export function deleteVideo(videoId) {
    return {
        type: DEL_VIDEO,
        videoId
    };
}