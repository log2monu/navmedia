import { PUT_LIKE, PUT_DISLIKE, FETCH_COMMENTS, POST_COMMENT } from './ActionTypes';

export function putLikeVideo(videoId) {
    return {
        type: PUT_LIKE,
        videoId
    };
}

export function putDislikeVideo(videoId) {
    return {
        type: PUT_DISLIKE,
        videoId
    };
}

export function getComments(videoId) {
  return {
      type: FETCH_COMMENTS,
      videoId
  };
}

export function postComment(payload) {
  return {
      type: POST_COMMENT,
      payload
  };
}