import {  FETCH_CATEGORY, FETCH_SERVICE_CATEGORY, FETCH_CAT_SUB_CATEGORY, FETCH_SUB_CAT_CONTENT, FETCH_BLOG, FETCH_SER_SUB_CATEGORY } from './ActionTypes';

export function getCategories() {
    return {
        type: FETCH_CATEGORY
    };
}

export function getCatSubCategories(cat_id) {
    return {
        type: FETCH_CAT_SUB_CATEGORY,
        cat_id
    };
}

export function getCatSubCategoriesVideo(cat_id) {
    return {
        type: FETCH_SUB_CATEGORY,
        cat_id
    };
}

export function fetchSubCatContent(sub_cat_id) {
    return {
        type: FETCH_SUB_CAT_CONTENT,
        sub_cat_id
    };
}

export function getServiceCategories() {
    return {
        type: FETCH_SERVICE_CATEGORY
    };
}

export function getServiceSubCategories(ser_cat_id) {
    return {
        type: FETCH_SER_SUB_CATEGORY,
        ser_cat_id
    };
}

export function getBlog(service_sub_cat_id) {
    return {
        type: FETCH_BLOG,
        service_sub_cat_id
    };
}