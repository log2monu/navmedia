import { POST_VIDEO } from './ActionTypes';

export function postVideoUpload(payload) {
    return {
        type: POST_VIDEO,
        payload
    };
}