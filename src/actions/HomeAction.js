import { FETCH_HOME } from './ActionTypes';

export function getHomeData() {
    return {
        type: FETCH_HOME
    };
}