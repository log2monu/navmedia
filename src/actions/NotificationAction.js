import { FETCH_TRENDING } from './ActionTypes';

export function getTrendingVideos() {
    return {
        type: FETCH_TRENDING
    };
}