import { POST_SEARCH } from "./ActionTypes";

export function postSearchVideo(payload) {
  return {
      type: POST_SEARCH,
      payload
  }
}