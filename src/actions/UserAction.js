import { FETCH_USER } from "./ActionTypes";

export function getUser(user_id) {
    return {
        type: FETCH_USER,
        user_id
    };
}