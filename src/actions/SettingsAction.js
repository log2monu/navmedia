import { POST_FEEDBACK } from './ActionTypes';

export function postFeedback(payload) {
  return {
      type: POST_FEEDBACK,
      payload
  };
}