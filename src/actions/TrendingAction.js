import { FETCH_TRENDING, FETCH_FEATURED } from './ActionTypes';

export function getTrendingVideos(payload) {
    return {
        type: FETCH_TRENDING,
        payload
    };
}

export function getFeaturedVideos(payload) {
    return {
        type: FETCH_FEATURED,
        payload
    };
}