import { FETCH_LOGIN } from "./ActionTypes";

export const postLogin = user => {
    return {
        type: FETCH_LOGIN,
        user
    }
}