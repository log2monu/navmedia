import { FETCH_HISTORY, DELETE_SINGLE_HISTORY, DELETE_HISTORY  } from "./ActionTypes";

export function getHistory() {
    return {
        type: FETCH_HISTORY,
    };
}

export function deleteSingleHistory(videoId) {
  return {
      type: DELETE_SINGLE_HISTORY,
      videoId
  };
}

export function deleteHistory() {
  return {
      type: DELETE_HISTORY,
  };
}