import { POST_BANNER, POST_AD_VIDEO, FETCH_AD_BANNER_1, FETCH_AD_BANNER_2, FETCH_AD_BANNER_3 } from "./ActionTypes";

export const postBannerAd = formBody => {
    return {
        type: POST_BANNER,
        formBody
    }
}

export const postVideoAd = formBody => {
    return {
        type: POST_AD_VIDEO,
        formBody
    }
}

export const getAdBanner1 = () => {
    return {
        type: FETCH_AD_BANNER_1,
    }
}

export const getAdBanner2 = () => {
    return {
        type: FETCH_AD_BANNER_2,
    }
}

export const getAdBanner3 = () => {
    return {
        type: FETCH_AD_BANNER_3,
    }
}