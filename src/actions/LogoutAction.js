import { POST_LOGOUT } from "./ActionTypes";

export function postLogOut(payload) {
  return {
      type: POST_LOGOUT,
      payload
  }
}