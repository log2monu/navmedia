import { FETCH_CHANNELS, POST_CREATE_CHANNEL, FETCH_CHANNEL_CONTENT, FETCH_MY_CHANNELS, FETCH_CHANNEL_DETAILS, FETCH_CHANNEL_POPULAR, PUT_SUBSCRIBE, PUT_CHANNEL, DEL_CHANNEL } from './ActionTypes';

export function getChannels() {
    return {
        type: FETCH_CHANNELS
    };
}

export function postCreateChannel(payload) {
    return {
        type: POST_CREATE_CHANNEL,
        payload
    };
}

export function getMyChannels() {
    return {
        type: FETCH_MY_CHANNELS
    }
}

export function getMyChannelDetails(channelId) {
    return {
        type: FETCH_CHANNEL_CONTENT,
        channelId
    }
}

export function getChannelAbout(channelId) {
    return {
        type: FETCH_CHANNEL_DETAILS,
        channelId
    }
}

export function getChannelPopular(channelId) {
    return {
        type: FETCH_CHANNEL_POPULAR,
        channelId
    }
}

export function putSubscribe(channelId) {
    return {
        type: PUT_SUBSCRIBE,
        channelId
    }
}

export function putChannel(payload) {
    return {
        type: PUT_CHANNEL,
        payload
    }
}

export function deleteChannel(channelId) {
    return {
        type: DEL_CHANNEL,
        channelId
    }
}