import {  FETCH_PROFILE, POST_UPDATE_PROFILE, DELETE_USER } from './ActionTypes';

export function getProfile() {
    return {
        type: FETCH_PROFILE
    };
}

export function putUpdateProfile(payload) {
    return {
        type: POST_UPDATE_PROFILE,
        payload
    };
}

export function deleteProfile() {
    return {
        type: DELETE_USER
    }
}
