import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { FETCH_TRENDING, FETCH_TRENDING_LOADING, FETCH_TRENDING_ERROR, FETCH_TRENDING_SUCCESS, FETCH_FEATURED, FETCH_FEATURED_LOADING, FETCH_FEATURED_ERROR, FETCH_FEATURED_SUCCESS } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* fetchTrendingData({payload}) {
  const token = yield select(getToken);
  yield put({type: FETCH_TRENDING_LOADING, state: true});
  yield put({type: FETCH_TRENDING_ERROR, state: null});
  try {
    const result = yield call(fetchTrendingDetails, {payload,token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_TRENDING_SUCCESS, payload:{data,page:payload.page}});
    } else {
      yield put({
        type: FETCH_TRENDING_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_TRENDING_ERROR, state: error});
  }
  yield put({type: FETCH_TRENDING_LOADING, state: false});
}

function* fetchFeaturedData() {
  const token = yield select(getToken);
  yield put({type: FETCH_FEATURED_LOADING, state: true});
  yield put({type: FETCH_FEATURED_ERROR, state: null});
  try {
    const result = yield call(fetchFeaturedDetails, token);
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_FEATURED_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_FEATURED_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_FEATURED_ERROR, state: error});
  }
  yield put({type: FETCH_FEATURED_LOADING, state: false});
}

function* TrendingSaga() {
  yield takeEvery(FETCH_TRENDING, fetchTrendingData);
  yield takeEvery(FETCH_FEATURED, fetchFeaturedData);
}

const fetchFeaturedDetails = async token => {
  const result = await fetch(
    `${base_url}/api/featured/?page=1`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const fetchTrendingDetails = async ({payload,token}) => {
  const result = await fetch(
    `${base_url}/api/trending/?${payload.params}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

export default TrendingSaga;
