import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { FETCH_CATEGORY, FETCH_CATEGORY_LOADING, FETCH_CATEGORY_ERROR, FETCH_CATEGORY_SUCCESS, FETCH_SERVICE_CATEGORY, FETCH_SERVICE_CATEGORY_LOADING, FETCH_SERVICE_CATEGORY_ERROR, FETCH_SERVICE_CATEGORY_SUCCESS, FETCH_SUB_CATEGORY, FETCH_SUB_CATEGORY_LOADING, FETCH_SUB_CATEGORY_ERROR, FETCH_SUB_CATEGORY_SUCCESS, FETCH_BLOG, FETCH_BLOG_LOADING, FETCH_BLOG_ERROR, FETCH_BLOG_SUCCESS, FETCH_CAT_SUB_CATEGORY, FETCH_CAT_SUB_CATEGORY_LOADING, FETCH_CAT_SUB_CATEGORY_ERROR, FETCH_CAT_SUB_CATEGORY_SUCCESS, FETCH_SER_SUB_CATEGORY, FETCH_SER_SUB_CATEGORY_LOADING, FETCH_SER_SUB_CATEGORY_ERROR, FETCH_SER_SUB_CATEGORY_SUCCESS, FETCH_SUB_CAT_CONTENT, FETCH_SUB_CAT_CONTENT_LOADING, FETCH_SUB_CAT_CONTENT_ERROR, FETCH_SUB_CAT_CONTENT_SUCCESS } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* fetchCategoryData() {
  const token = yield select(getToken);
  yield put({type: FETCH_CATEGORY_LOADING, state: true});
  yield put({type: FETCH_CATEGORY_ERROR, state: null});
  try {
    const result = yield call(fetchCategoryDetails, token);
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_CATEGORY_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_CATEGORY_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_CATEGORY_ERROR, state: error});
  }
  yield put({type: FETCH_CATEGORY_LOADING, state: false});
}

function* fetchServiceCategoryData() {
  const token = yield select(getToken);
  yield put({type: FETCH_SERVICE_CATEGORY_LOADING, state: true});
  yield put({type: FETCH_SERVICE_CATEGORY_ERROR, state: null});
  try {
    const result = yield call(fetchServiceCategoryDetails, token);
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_SERVICE_CATEGORY_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_SERVICE_CATEGORY_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_SERVICE_CATEGORY_ERROR, state: error});
  }
  yield put({type: FETCH_SERVICE_CATEGORY_LOADING, state: false});
}

function* fetchCatSubCategoryData({cat_id}) {
  const token = yield select(getToken);
  yield put({type: FETCH_CAT_SUB_CATEGORY_LOADING, state: true});
  yield put({type: FETCH_CAT_SUB_CATEGORY_ERROR, state: null});
  try {
    const result = yield call(fetchCatSubCategoryDetails, {cat_id,token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_CAT_SUB_CATEGORY_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_CAT_SUB_CATEGORY_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_CAT_SUB_CATEGORY_ERROR, state: error});
  }
  yield put({type: FETCH_CAT_SUB_CATEGORY_LOADING, state: false});
}

function* fetchSerSubCategoryData({ser_cat_id}) {
  const token = yield select(getToken);
  yield put({type: FETCH_SER_SUB_CATEGORY_LOADING, state: true});
  yield put({type: FETCH_SER_SUB_CATEGORY_ERROR, state: null});
  try {
    const result = yield call(fetchSerSubCategoryDetails, {ser_cat_id,token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_SER_SUB_CATEGORY_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_SER_SUB_CATEGORY_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_SER_SUB_CATEGORY_ERROR, state: error});
  }
  yield put({type: FETCH_SER_SUB_CATEGORY_LOADING, state: false});
}

function* fetchBlogData({service_sub_cat_id}) {
  const token = yield select(getToken);
  yield put({type: FETCH_BLOG_LOADING, state: true});
  yield put({type: FETCH_BLOG_ERROR, state: null});
  try {
    const result = yield call(fetchBlogDetails, {service_sub_cat_id,token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_BLOG_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_BLOG_ERROR,
        state: JSON.stringify(result.data),
      });
    } 
  } catch (error) {
    yield put({type: FETCH_BLOG_ERROR, state: error});
  }
  yield put({type: FETCH_BLOG_LOADING, state: false});
}

function* fetchCatSubCatContentData({sub_cat_id}) {
  const token = yield select(getToken);
  yield put({type: FETCH_SUB_CAT_CONTENT_LOADING, state: true});
  yield put({type: FETCH_SUB_CAT_CONTENT_ERROR, state: null});
  try {
    const result = yield call(fetchCatSubCatContentDetails, {sub_cat_id,token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_SUB_CAT_CONTENT_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_SUB_CAT_CONTENT_ERROR,
        state: JSON.stringify(result.data),
      });
    } 
  } catch (error) {
    yield put({type: FETCH_SUB_CAT_CONTENT_ERROR, state: error});
  }
  yield put({type: FETCH_SUB_CAT_CONTENT_LOADING, state: false});
}

function* CategorySaga() {
  yield takeEvery(FETCH_CATEGORY, fetchCategoryData);
  yield takeEvery(FETCH_SERVICE_CATEGORY, fetchServiceCategoryData);
  yield takeEvery(FETCH_CAT_SUB_CATEGORY, fetchCatSubCategoryData);
  yield takeEvery(FETCH_SUB_CAT_CONTENT, fetchCatSubCatContentData);
  yield takeEvery(FETCH_SER_SUB_CATEGORY, fetchSerSubCategoryData);
  yield takeEvery(FETCH_BLOG, fetchBlogData);
}

const fetchCatSubCatContentDetails = async ({ sub_cat_id, token}) => {
  console.log("cvmkdbm : ", `${base_url}/api/${sub_cat_id}/SubcatVideosListView/` )
  const result = await fetch(
    `${base_url}/api/${sub_cat_id}/SubcatVideosListView/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
}

const fetchBlogDetails = async ({ service_sub_cat_id, token}) => {
  const result = await fetch(
    `${base_url}/api/${service_sub_cat_id}/SubcatServiceListView/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
}

const fetchCatSubCategoryDetails = async ({cat_id, token}) => {
  const result = await fetch(
    `${base_url}/api/${cat_id}/subcategories/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
}

const fetchSerSubCategoryDetails = async ({ser_cat_id, token}) => {
  const result = await fetch(
    `${base_url}/api/${ser_cat_id}/ServiceCatsubcategories/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
}

const fetchServiceCategoryDetails = async token => {
  const result = await fetch(
    `${base_url}/api/Servicecategories/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const fetchCategoryDetails = async token => {
  const result = await fetch(
    `${base_url}/api/categories/?page=1`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

export default CategorySaga;
