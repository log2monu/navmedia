import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { POST_FEEDBACK, POST_FEEDBACK_LOADING, POST_FEEDBACK_ERROR, POST_FEEDBACK_SUCCESS } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* postFeedbackData({ payload }) {
  const token = yield select(getToken);
  yield put({type: POST_FEEDBACK_LOADING, state: true});
  yield put({type: POST_FEEDBACK_ERROR, state: null});
  try {
    const result = yield call(postFeedbackDetails, {payload, token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: POST_FEEDBACK_SUCCESS, data});
    } else {
      yield put({
        type: POST_FEEDBACK_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: POST_FEEDBACK_ERROR, state: error});
  }
  yield put({type: POST_FEEDBACK_LOADING, state: false});
}

function* SettingsSaga() {
  yield takeEvery(POST_FEEDBACK, postFeedbackData);
}

const postFeedbackDetails = async ({payload, token}) => {
  const result = await fetch(
    `${base_url}/api/FeedbackCreate`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`
      },
      body: payload
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

export default SettingsSaga;
