
import { put, takeEvery, call } from "redux-saga/effects";
import { FETCH_LOGIN, LOGIN_LOADING, LOGIN_ERROR, LOGIN_SUCCESS, TOKEN_SUCCESS, REFRESH_TOKEN_SUCCESS, USER_SUCCESS } from "../actions/ActionTypes";
import { base_url, getToken } from "../constants/config";

function* LoginSaga() {
  yield takeEvery(FETCH_LOGIN, loginUser);
}

export const checkResult = result => {
    if (result.status) {
        return true;
    }
    return false;
};

function* loginUser( {user} ) {
    yield put({ type: LOGIN_LOADING, state: true });
    yield put({ type: LOGIN_ERROR, state: null });
    try {
        const result = yield call(loginUserPost, user.formBody);
        if (checkResult(result)) {
          const { data } = result;
          const { access_token, refresh_token } = data;

          yield put ({
            type: LOGIN_SUCCESS,
            data
          });
          yield put({
            type: TOKEN_SUCCESS,
            data: access_token
          });
          yield put({
            type: REFRESH_TOKEN_SUCCESS,
            data: refresh_token
          });
          yield put({
            type: USER_SUCCESS,
            data: user.userInfo
          });
        } 
        else {
          yield put({ 
            type: LOGIN_ERROR,
            state: JSON.stringify(result.data)
          });
        }
    }
    catch (error) {
      yield put({ type: LOGIN_ERROR, state: error });
    }
    yield put({ type: LOGIN_LOADING, state: false });
}

const loginUserPost = async user => {
  console.log("Daaatttttaaaa : ", user)
  try {
    const result = await fetch(`${base_url}/auth/convert-token`, {
      method: 'POST',
      headers: {
        Accept: "application/json",
        'Content-Type': 'multipart/form-data'
      },
      body: user
    });
    return result.json().then(data => ({
      data: data,
      status: result.ok
    }));
  }
  catch (err) {}
};

export default LoginSaga;
