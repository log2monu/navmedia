import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { FETCH_VIDEO, FETCH_VIDEO_LOADING, FETCH_VIDEO_ERROR, FETCH_VIDEO_SUCCESS, DEL_VIDEO, DEL_VIDEO_LOADING, DEL_VIDEO_ERROR, DEL_VIDEO_SUCCESS } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* fetchVideoData({videoId}) {
  const token = yield select(getToken);
  yield put({type: FETCH_VIDEO_LOADING, state: true});
  yield put({type: FETCH_VIDEO_ERROR, state: null});
  try {
    const result = yield call(fetchVideoDetails, {token, videoId});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_VIDEO_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_VIDEO_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_VIDEO_ERROR, state: error});
  }
  yield put({type: FETCH_VIDEO_LOADING, state: false});
}

function* delVideoData({videoId}) {
  const token = yield select(getToken);
  yield put({type: DEL_VIDEO_LOADING, state: true});
  yield put({type: DEL_VIDEO_ERROR, state: null});
  try {
    const result = yield call(delVideoDetails, {token, videoId});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: DEL_VIDEO_SUCCESS, data});
    } else {
      yield put({
        type: DEL_VIDEO_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: DEL_VIDEO_ERROR, state: error});
  }
  yield put({type: DEL_VIDEO_LOADING, state: false});
}

function* VideoSaga() {
  yield takeEvery(FETCH_VIDEO, fetchVideoData);
  yield takeEvery(DEL_VIDEO, delVideoData);
}

const delVideoDetails = async ({token, videoId}) => {
  // console.log("sghdjv :", videoId)
  const result = await fetch(
    `${base_url}/api/${videoId}/deleteVideo`,
    {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const fetchVideoDetails = async ({token, videoId}) => {
  // console.log("sghdjv :", videoId)
  const result = await fetch(
    `${base_url}/api/${videoId}/player`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

export default VideoSaga;
