import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { FETCH_PROFILE, FETCH_PROFILE_LOADING, FETCH_PROFILE_ERROR, FETCH_PROFILE_SUCCESS, POST_BANNER_LOADING, POST_BANNER_SUCCESS, POST_BANNER, POST_BANNER_ERROR, POST_AD_VIDEO, POST_AD_VIDEO_LOADING, POST_AD_VIDEO_ERROR, POST_AD_VIDEO_SUCCESS, POST_CREATE_CHANNEL, POST_CREATE_CHANNEL_LOADING, POST_CREATE_CHANNEL_ERROR, POST_CREATE_CHANNEL_SUCCESS, POST_UPDATE_PROFILE, POST_UPDATE_PROFILE_LOADING, POST_UPDATE_PROFILE_ERROR, POST_UPDATE_PROFILE_SUCCESS, USER_SUCCESS, DELETE_USER, DELETE_USER_LOADING, DELETE_USER_ERROR, DELETE_USER_SUCCESS, TOKEN_EMPTY, LOGIN_EMPTY, REFRESH_TOKEN_EMPTY, USER_EMPTY, PREMIUM_STATUS } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* fetchProfileData() {
  const token = yield select(getToken);
  yield put({type: FETCH_PROFILE_LOADING, state: true});
  yield put({type: FETCH_PROFILE_ERROR, state: null});
  try {
    const result = yield call(fetchProfileDetails, token);
    if (checkResult(result)) {
      const {data} = result;
      const { profile } = data;
      console.log("Dattattatttattattatat :", data.profile.is_premium)
      yield put({
        type: FETCH_PROFILE_SUCCESS, 
        data
      });
      yield put({
        type: PREMIUM_STATUS, 
        data: profile.is_premium
      });
    } 
    else {
      yield put({
        type: FETCH_PROFILE_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_PROFILE_ERROR, state: error});
  }
  yield put({type: FETCH_PROFILE_LOADING, state: false});
}

function* postCreateChannelData({ payload }) {
  const token = yield select(getToken);
  yield put({type: POST_CREATE_CHANNEL_LOADING, state: true});
  yield put({type: POST_CREATE_CHANNEL_ERROR, state: null});
  try {
    const result = yield call(postCreateChannelDetails, { payload, token} );
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: POST_CREATE_CHANNEL_SUCCESS, data});
    } else {
      yield put({
        type: POST_CREATE_CHANNEL_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: POST_CREATE_CHANNEL_ERROR, state: error});
  }
  yield put({type: POST_CREATE_CHANNEL_LOADING, state: false});
}


function* postUpdateData({ payload }) {
  const token = yield select(getToken);
  yield put({type: POST_UPDATE_PROFILE_LOADING, state: true});
  yield put({type: POST_UPDATE_PROFILE_ERROR, state: null});
  try {
    const result = yield call(postUpdateDetails, { payload, token} );
    if (checkResult(result)) {
      const {data} = result;
      // const { image, phone } = data;
      yield put({type: POST_UPDATE_PROFILE_SUCCESS, data});
      yield put({type:USER_SUCCESS, data})
    } else {
      yield put({
        type: POST_UPDATE_PROFILE_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: POST_UPDATE_PROFILE_ERROR, state: error});
  }
  yield put({type: POST_UPDATE_PROFILE_LOADING, state: false});
}

function* deleteUserData() {
  const token = yield select(getToken);
  yield put({type: DELETE_USER_LOADING, state: true});
  yield put({type: DELETE_USER_ERROR, state: null});
  try {
    const result = yield call(deleteUserDetails, { token} );
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: DELETE_USER_SUCCESS, data});
      yield put({ type: TOKEN_EMPTY });
      yield put({ type: REFRESH_TOKEN_EMPTY });
      yield put ({ type: USER_EMPTY });
      yield put({ type: LOGIN_EMPTY });
    } else {
      yield put({
        type: DELETE_USER_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: DELETE_USER_ERROR, state: error});
  }
  yield put({type: DELETE_USER_LOADING, state: false});
}

function* ProfileSaga() {
  yield takeEvery(FETCH_PROFILE, fetchProfileData);
  yield takeEvery(POST_CREATE_CHANNEL, postCreateChannelData);
  yield takeEvery(POST_UPDATE_PROFILE, postUpdateData);
  yield takeEvery(DELETE_USER, deleteUserData);
}

const deleteUserDetails = async ({token}) => {
  const result = await fetch(
    `${base_url}/api/deleteAccount`,
    {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const postUpdateDetails = async ({payload,token}) => {
  console.log("updatee channl.. : ", payload)
  const result = await fetch(
    `${base_url}/api/ProfileUpdate`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`
      },
      body: payload
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const postCreateChannelDetails = async ({payload,token}) => {
  console.log("createe channl.. : ", `${base_url}/api/channel-create`)
  const result = await fetch(
    `${base_url}/api/channel-create`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`
      },
      body: payload
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const fetchProfileDetails = async token => {
  const result = await fetch(
    `${base_url}/api/ConnectProfile/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

export default ProfileSaga;
