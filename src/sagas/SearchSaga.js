import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { POST_SEARCH, POST_SEARCH_LOADING, POST_SEARCH_ERROR, POST_SEARCH_SUCCESS } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* postSearchData({ payload }) {
  const token = yield select(getToken);
  yield put({type: POST_SEARCH_LOADING, state: true});
  yield put({type: POST_SEARCH_ERROR, state: null});
  try {
    const result = yield call(postSearchDetails, {payload, token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: POST_SEARCH_SUCCESS, data});
    } else {
      yield put({
        type: POST_SEARCH_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: POST_SEARCH_ERROR, state: error});
  }
  yield put({type: POST_SEARCH_LOADING, state: false});
}

function* SearchSaga() {
  yield takeEvery(POST_SEARCH, postSearchData);
}

const postSearchDetails = async ({payload, token}) => {
  console.log("Searrrch :", `${base_url}/api/search/?s=${payload}`)
  const result = await fetch(
    `${base_url}/api/search/?s=${payload}`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

export default SearchSaga;
