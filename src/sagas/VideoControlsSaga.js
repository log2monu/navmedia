import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { PUT_LIKE, PUT_LIKE_LOADING, PUT_LIKE_ERROR, PUT_LIKE_SUCCESS, PUT_DISLIKE_LOADING, PUT_DISLIKE_ERROR, PUT_DISLIKE_SUCCESS, FETCH_COMMENTS, POST_COMMENT, FETCH_COMMENTS_LOADING, FETCH_COMMENTS_ERROR, FETCH_COMMENTS_SUCCESS, POST_COMMENT_LOADING, POST_COMMENT_ERROR, POST_COMMENT_SUCCESS, PUT_DISLIKE } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* putLikeData({videoId}) {
  const token = yield select(getToken);
  yield put({type: PUT_LIKE_LOADING, state: true});
  yield put({type: PUT_LIKE_ERROR, state: null});
  try {
    const result = yield call(putLikeDetails, {token, videoId});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: PUT_LIKE_SUCCESS, data});
    } else {
      yield put({
        type: PUT_LIKE_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: PUT_LIKE_ERROR, state: error});
  }
  yield put({type: PUT_LIKE_LOADING, state: false});
}

function* putDislikeData({videoId}) {
  const token = yield select(getToken);
  yield put({type: PUT_DISLIKE_LOADING, state: true});
  yield put({type: PUT_DISLIKE_ERROR, state: null});
  try {
    const result = yield call(putDislikeDetails, {token, videoId});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: PUT_DISLIKE_SUCCESS, data});
    } else {
      yield put({
        type: PUT_DISLIKE_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: PUT_DISLIKE_ERROR, state: error});
  }
  yield put({type: PUT_DISLIKE_LOADING, state: false});
}

function* fetchCommentsData({videoId}) {
  const token = yield select(getToken);
  yield put({type: FETCH_COMMENTS_LOADING, state: true});
  yield put({type: FETCH_COMMENTS_ERROR, state: null});
  try {
    const result = yield call(fetchCommentsDetails, {token, videoId});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_COMMENTS_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_COMMENTS_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_COMMENTS_ERROR, state: error});
  }
  yield put({type: FETCH_COMMENTS_LOADING, state: false});
}

function* postCommentData({payload}) {
  const token = yield select(getToken);
  yield put({type: POST_COMMENT_LOADING, state: true});
  yield put({type: POST_COMMENT_ERROR, state: null});
  try {
    const result = yield call(postCommentDetails, {token, payload});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: POST_COMMENT_SUCCESS, data});
    } else {
      yield put({
        type: POST_COMMENT_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: POST_COMMENT_ERROR, state: error});
  }
  yield put({type: POST_COMMENT_LOADING, state: false});
}

function* VideoControlsSaga() {
  yield takeEvery(PUT_LIKE, putLikeData);
  yield takeEvery(PUT_DISLIKE, putDislikeData);
  yield takeEvery(FETCH_COMMENTS, fetchCommentsData);
  yield takeEvery(POST_COMMENT, postCommentData);
}

const postCommentDetails = async ({token, payload}) => {
  const result = await fetch(
    `${base_url}/api/post-comments`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      },
      body: payload
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
}

const fetchCommentsDetails = async ({token, videoId}) => {
  const result = await fetch(
    `${base_url}/api/${videoId}/comments`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
}

const putLikeDetails = async ({token, videoId}) => {
  const result = await fetch(
    `${base_url}/api/${videoId}/likeVideo`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const putDislikeDetails = async ({token, videoId}) => {
  const result = await fetch(
    `${base_url}/api/${videoId}/dislikeVideo`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

export default VideoControlsSaga;
