import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { CONNECT_IMAGE, CONNECT_VIDEO, CONNECT_IMAGE_LOADING, CONNECT_IMAGE_ERROR, CONNECT_IMAGE_SUCCESS, CONNECT_VIDEO_LOADING, CONNECT_VIDEO_ERROR, CONNECT_VIDEO_SUCCESS } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* postConnectImageData({ payload }) {
  const token = yield select(getToken);
  yield put({type: CONNECT_IMAGE_LOADING, state: true});
  yield put({type: CONNECT_IMAGE_ERROR, state: null});
  try {
    const result = yield call(postConnectImageDetails, { payload, token});
    if (checkResult(result)) {
      const {data} = result;
      // console.log("Dattatta :", data)
      yield put({type: CONNECT_IMAGE_SUCCESS, data});
    } else {
      yield put({
        type: CONNECT_IMAGE_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: CONNECT_IMAGE_ERROR, state: error});
  }
  yield put({type: CONNECT_IMAGE_LOADING, state: false});
}

function* postConnectVideoData({ payload }) {
  const token = yield select(getToken);
  yield put({type: CONNECT_VIDEO_LOADING, state: true});
  yield put({type: CONNECT_VIDEO_ERROR, state: null});
  try {
    const result = yield call(postConnectVideoDetails, { payload, token} );
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: CONNECT_VIDEO_SUCCESS, data});
    } else {
      yield put({
        type: CONNECT_VIDEO_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: CONNECT_VIDEO_ERROR, state: error});
  }
  yield put({type: CONNECT_VIDEO_LOADING, state: false});
}


function* ConnectSaga() {
  yield takeEvery(CONNECT_IMAGE, postConnectImageData);
  yield takeEvery(CONNECT_VIDEO, postConnectVideoData);
}


const postConnectImageDetails = async ({payload,token}) => {
  console.log("FORMBODY image :", payload)
  const result = await fetch(
    `${base_url}/api/user-connect-image`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`
      },
      body: payload
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const postConnectVideoDetails = async ({ payload, token }) => {
  console.log("FORMBODY Video :", payload)
  const result = await fetch(
    `${base_url}/api/user-connect-video`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`
      },
      body: payload
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

export default ConnectSaga;
