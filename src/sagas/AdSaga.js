import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import {POST_BANNER_LOADING, POST_BANNER_SUCCESS, POST_BANNER, POST_BANNER_ERROR, POST_AD_VIDEO, POST_AD_VIDEO_LOADING, POST_AD_VIDEO_ERROR, POST_AD_VIDEO_SUCCESS, FETCH_AD_BANNER_1, FETCH_AD_BANNER_1_LOADING, FETCH_AD_BANNER_1_ERROR, FETCH_AD_BANNER_1_SUCCESS, FETCH_AD_BANNER_2, FETCH_AD_BANNER_3, FETCH_AD_BANNER_2_LOADING, FETCH_AD_BANNER_2_ERROR, FETCH_AD_BANNER_2_SUCCESS, FETCH_AD_BANNER_3_LOADING, FETCH_AD_BANNER_3_ERROR, FETCH_AD_BANNER_3_SUCCESS,   } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
    if (result.status) {
      return true;
    }
    return false;
  };

  function* postBannerData({ formBody }) {
    const token = yield select(getToken);
    yield put({type: POST_BANNER_LOADING, state: true});
    yield put({type: POST_BANNER_ERROR, state: null});
    try {
      const result = yield call(postBannerDetails, { formBody, token} );
      if (checkResult(result)) {
        const {data} = result;
        yield put({type: POST_BANNER_SUCCESS, data});
      } else {
        yield put({
          type: POST_BANNER_ERROR,
          state: JSON.stringify(result.data),
        });
      }
    } catch (error) {
      yield put({type: POST_BANNER_ERROR, state: error});
    }
    yield put({type: POST_BANNER_LOADING, state: false});
  }
  
  function* postAdData({ formBody }) {
    const token = yield select(getToken);
    yield put({type: POST_AD_VIDEO_LOADING, state: true});
    yield put({type: POST_AD_VIDEO_ERROR, state: null});
    try {
      const result = yield call(postAdDetails, { formBody, token} );
      if (checkResult(result)) {
        const {data} = result;
        yield put({type: POST_AD_VIDEO_SUCCESS, data});
      } else {
        yield put({
          type: POST_AD_VIDEO_ERROR,
          state: JSON.stringify(result.data),
        });
      }
    } catch (error) {
      yield put({type: POST_AD_VIDEO_ERROR, state: error});
    }
    yield put({type: POST_AD_VIDEO_LOADING, state: false});
  }  

  function* getBanner1Data() {
    const token = yield select(getToken);
    yield put({type: FETCH_AD_BANNER_1_LOADING, state: true});
    yield put({type: FETCH_AD_BANNER_1_ERROR, state: null});
    try {
      const result = yield call(getBanner1Details, token);
      if (checkResult(result)) {
        const {data} = result;
        yield put({type: FETCH_AD_BANNER_1_SUCCESS, data});
      } else {
        yield put({
          type: FETCH_AD_BANNER_1_ERROR,
          state: JSON.stringify(result.data),
        });
      }
    } catch (error) {
      yield put({type: FETCH_AD_BANNER_1_ERROR, state: error});
    }
    yield put({type: FETCH_AD_BANNER_1_LOADING, state: false});
  }

  function* getBanner2Data() {
    const token = yield select(getToken);
    yield put({type: FETCH_AD_BANNER_2_LOADING, state: true});
    yield put({type: FETCH_AD_BANNER_2_ERROR, state: null});
    try {
      const result = yield call(getBanner2Details, token);
      if (checkResult(result)) {
        const {data} = result;
        yield put({type: FETCH_AD_BANNER_2_SUCCESS, data});
      } else {
        yield put({
          type: FETCH_AD_BANNER_2_ERROR,
          state: JSON.stringify(result.data),
        });
      }
    } catch (error) {
      yield put({type: FETCH_AD_BANNER_2_ERROR, state: error});
    }
    yield put({type: FETCH_AD_BANNER_2_LOADING, state: false});
  }

  function* getBanner3Data() {
    const token = yield select(getToken);
    yield put({type: FETCH_AD_BANNER_3_LOADING, state: true});
    yield put({type: FETCH_AD_BANNER_3_ERROR, state: null});
    try {
      const result = yield call(getBanner3Details, token);
      if (checkResult(result)) {
        const {data} = result;
        yield put({type: FETCH_AD_BANNER_3_SUCCESS, data});
      } else {
        yield put({
          type: FETCH_AD_BANNER_3_ERROR,
          state: JSON.stringify(result.data),
        });
      }
    } catch (error) {
      yield put({type: FETCH_AD_BANNER_3_ERROR, state: error});
    }
    yield put({type: FETCH_AD_BANNER_3_LOADING, state: false});
  }

  function* AdSaga() {
    yield takeEvery(POST_BANNER, postBannerData);
    yield takeEvery(POST_AD_VIDEO, postAdData);
    yield takeEvery(FETCH_AD_BANNER_1, getBanner1Data);
    yield takeEvery(FETCH_AD_BANNER_2, getBanner2Data);
    yield takeEvery(FETCH_AD_BANNER_3, getBanner3Data);
}

const getBanner3Details = async token => {
  const result = await fetch(
    `${base_url}/api/Banner3View`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const getBanner2Details = async token => {
  const result = await fetch(
    `${base_url}/api/Banner2View`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const getBanner1Details = async token => {
    const result = await fetch(
      `${base_url}/api/Banner1View`,
      {
        method: "GET",
        headers: {
          Accept: "application/json",
          Authorization: `Bearer ${token}`
        }
      }
    );
    return result.json().then(data => ({
      data: data,
      status: result.ok
    }));
  };


const postAdDetails = async ({formBody,token}) => {
    console.log("ad.. : ", formBody)
    const result = await fetch(
      `${base_url}/api/user-ad`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${token}`
        },
        body: formBody
      }
    );
    return result.json().then(data => ({
      data: data,
      status: result.ok
    }));
  };
  
  const postBannerDetails = async ({formBody,token}) => {
    console.log("edcj : ", formBody)
    const result = await fetch(
      `${base_url}/api/user-banner`,
      {
        method: "POST",
        headers: {
          Accept: "application/json",
          'Content-Type': 'multipart/form-data',
          Authorization: `Bearer ${token}`
        },
        body: formBody
      }
    );
    return result.json().then(data => ({
      data: data,
      status: result.ok
    }));
  };

export default AdSaga;