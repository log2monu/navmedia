import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { base_url, getToken } from "../constants/config";
import { FETCH_HISTORY, DELETE_HISTORY, DELETE_SINGLE_HISTORY, FETCH_HISTORY_LOADING, FETCH_HISTORY_ERROR, FETCH_HISTORY_SUCCESS, DELETE_SINGLE_HISTORY_LOADING, DELETE_SINGLE_HISTORY_ERROR, DELETE_SINGLE_HISTORY_SUCCESS, DELETE_HISTORY_LOADING, DELETE_HISTORY_ERROR, DELETE_HISTORY_SUCCESS } from '../actions/ActionTypes';

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};


function* fetchHistoryData() {
  const token = yield select(getToken);
  yield put({type: FETCH_HISTORY_LOADING, state: true});
  yield put({type: FETCH_HISTORY_ERROR, state: null});
  try {
    const result = yield call(fetchHistoryDetails, token );
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_HISTORY_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_HISTORY_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_HISTORY_ERROR, state: error});
  }
  yield put({type: FETCH_HISTORY_LOADING, state: false});
}

function* delSingleHistoryData({ videoId }) {
  const token = yield select(getToken);
  yield put({type: DELETE_SINGLE_HISTORY_LOADING, state: true});
  yield put({type: DELETE_SINGLE_HISTORY_ERROR, state: null});
  try {
    const result = yield call(delSingleHistoryDetails, { videoId, token} );
    if (checkResult(result)) {
      const {data} = result;
      yield put({type:DELETE_SINGLE_HISTORY_SUCCESS, data})
    } else {
      yield put({
        type: DELETE_SINGLE_HISTORY_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: DELETE_SINGLE_HISTORY_ERROR, state: error});
  }
  yield put({type: DELETE_SINGLE_HISTORY_LOADING, state: false});
}

function* delHistoryData() {
  const token = yield select(getToken);
  yield put({type: DELETE_HISTORY_LOADING, state: true});
  yield put({type: DELETE_HISTORY_ERROR, state: null});
  try {
    const result = yield call(delHistoryDetails, token );
    if (checkResult(result)) {
      const {data} = result;
      yield put({type:DELETE_HISTORY_SUCCESS, data})
    } else {
      yield put({
        type: DELETE_HISTORY_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: DELETE_HISTORY_ERROR, state: error});
  }
  yield put({type: DELETE_HISTORY_LOADING, state: false});
}


function* HistorySaga() {
  yield takeEvery(FETCH_HISTORY, fetchHistoryData);
  yield takeEvery(DELETE_SINGLE_HISTORY, delSingleHistoryData);
  yield takeEvery(DELETE_HISTORY, delHistoryData);
}

const delHistoryDetails = async token => {
  const result = await fetch(
    `${base_url}/api/deleteAllHistory`,
    {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const delSingleHistoryDetails = async ({videoId,token}) => {
  console.log("payloaddd.. : ", `${base_url}/api/${videoId}/deleteHistory`)
  const result = await fetch(
    `${base_url}/api/${videoId}/deleteHistory`,
    {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const fetchHistoryDetails = async token => {
  const result = await fetch(
    `${base_url}/api/history`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};


export default HistorySaga;
