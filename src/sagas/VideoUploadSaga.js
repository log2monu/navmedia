import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { POST_VIDEO, POST_VIDEO_LOADING, POST_VIDEO_ERROR, POST_VIDEO_SUCCESS } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* fetchVideoUploadData({ payload }) {
  const token = yield select(getToken);
  yield put({type: POST_VIDEO_LOADING, state: true});
  yield put({type: POST_VIDEO_ERROR, state: null});
  try {
    const result = yield call(fetchVideoUploadDetails, {payload, token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: POST_VIDEO_SUCCESS, data});
    } else {
      yield put({
        type: POST_VIDEO_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: POST_VIDEO_ERROR, state: error});
  }
  yield put({type: POST_VIDEO_LOADING, state: false});
}

function* VideoUploadSaga() {
  yield takeEvery(POST_VIDEO, fetchVideoUploadData);
}

const fetchVideoUploadDetails = async ({payload, token}) => {
  console.log("params :", payload)
  const result = await fetch(
    `${base_url}/api/vid-create`,
    {
      method: "POST",
      headers: {
        Accept: "application/json",
        'Content-Type': 'multipart/form-data',
        Authorization: `Bearer ${token}`
      },
      body: payload
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

export default VideoUploadSaga;
