import { all } from "redux-saga/effects";
import LoginSaga from "./LoginSaga";
import LogoutSaga from "./LogoutSaga";
import HomeSaga from "./HomeSaga";
import CategorySaga from "./CategorySaga";
import ChannelsSaga from "./ChannelsSaga";
import ProfileSaga from "./ProfileSaga";
import SettingsSaga from "./SettingsSaga";
import TrendingSaga from "./TrendingSaga";
import VideoSaga from "./VideoSaga";
import VideoUploadSaga from "./VideoUploadSaga";
import AdSaga from "./AdSaga";
import VideoControlsSaga from "./VideoControlsSaga";
import ConnectSaga from "./ConnectSaga";
import SearchSaga from "./SearchSaga";
import HistorySaga from "./HistorySaga";

function* rootSaga() {
    yield all([
        LoginSaga(),
        HomeSaga(),
        HomeSaga(),
        CategorySaga(),
        ChannelsSaga(),
        ProfileSaga(),
        AdSaga(),
        SettingsSaga(),
        TrendingSaga(),
        VideoSaga(),
        VideoControlsSaga(),
        VideoUploadSaga(),
        ConnectSaga(),
        SearchSaga(),
        HistorySaga(),
        LogoutSaga()
    ]);
}

export default rootSaga;