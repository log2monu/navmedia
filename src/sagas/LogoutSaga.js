import {
  POST_LOGOUT,
  POST_LOGOUT_ERROR,
  POST_LOGOUT_LOADING,
  POST_LOGOUT_SUCCESS,
  TOKEN_EMPTY,
  LOGIN_EMPTY,
  REFRESH_TOKEN_EMPTY,
} from '../actions/ActionTypes';

import {base_url, getToken} from '../constants/config';
import {checkResultCommon} from './checkResult';
import {put, takeEvery, call, select} from 'redux-saga/effects';

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* postData({payload}) {
  const token = yield select(getToken);
  yield put({type: POST_LOGOUT_LOADING, state: true});
  yield put({type: POST_LOGOUT_ERROR, state: null});
  try {
    const result = yield call(postLogout, {token,payload});
    // const checkResult = yield call(checkResultCommon, result);
    if (checkResult(result)) {
      // if (checkResult(result)) {
      const {data} = result;
      yield put({type: POST_LOGOUT_SUCCESS, data});
      
      // yield put({type: LOGIN_EMPTY});
    } else {
      yield put({
        type: POST_LOGOUT_ERROR,
        state: JSON.stringify(result.data),
      });
      yield put({type: TOKEN_EMPTY});
      yield put({type: REFRESH_TOKEN_EMPTY});
    }
  } catch (error) {
    yield put({type: POST_LOGOUT_ERROR, state: error});
  }
  yield put({type: POST_LOGOUT_LOADING, state: false});
}

function* LogoutSaga() {
  yield takeEvery(POST_LOGOUT, postData);
}

const postLogout = async ({token, payload}) => {
  const result = await fetch(`${base_url}/auth/invalidate-sessions`, 
  {
    method: 'POST',
    headers: {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data',
      Authorization: `Bearer ${token}`,
    },
    body: payload
  });
  return result.json().then(data => ({
    data: data,
    status: result.ok,
    responseCode: result.status,
  }));
};

export default LogoutSaga;
