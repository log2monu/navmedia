import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import { FETCH_HOME, FETCH_HOME_LOADING, FETCH_HOME_ERROR, FETCH_HOME_SUCCESS } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* fetchHomeData() {
  const token = yield select(getToken);
  yield put({type: FETCH_HOME_LOADING, state: true});
  yield put({type: FETCH_HOME_ERROR, state: null});
  try {
    const result = yield call(fetchHomeDetails, token);
    if (checkResult(result)) {
      const {data} = result;
      console.log("hf :", result)
      yield put({type: FETCH_HOME_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_HOME_ERROR,
        state: result.data,
      });
    }
  } catch (error) {
    yield put({type: FETCH_HOME_ERROR, state: error});
  }
  yield put({type: FETCH_HOME_LOADING, state: false});
}

function* HomeSaga() {
  yield takeEvery(FETCH_HOME, fetchHomeData);
}

const fetchHomeDetails = async token => {
  // console.log("token saga:", token) 
  const result = await fetch(
    `${base_url}/api/home/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );

  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

export default HomeSaga;
