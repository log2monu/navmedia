import {put, takeEvery, call, select, takeLatest} from 'redux-saga/effects';
import {  FETCH_CHANNEL_DETAILS, FETCH_CHANNEL_DETAILS_SUCCESS, FETCH_CHANNEL_DETAILS_LOADING, FETCH_CHANNEL_DETAILS_ERROR,FETCH_CHANNELS, FETCH_CHANNELS_LOADING, FETCH_CHANNELS_ERROR, FETCH_CHANNELS_SUCCESS, FETCH_MY_CHANNELS, FETCH_CHANNEL_CONTENT, FETCH_MY_CHANNELS_LOADING, FETCH_MY_CHANNELS_ERROR, FETCH_MY_CHANNELS_SUCCESS, FETCH_CHANNEL_CONTENT_ERROR, FETCH_CHANNEL_CONTENT_LOADING, FETCH_CHANNEL_CONTENT_SUCCESS, FETCH_CHANNEL_POPULAR, FETCH_CHANNEL_POPULAR_LOADING, FETCH_CHANNEL_POPULAR_ERROR, FETCH_CHANNEL_POPULAR_SUCCESS, PUT_SUBSCRIBE, PUT_SUBSCRIBE_LOADING, PUT_SUBSCRIBE_ERROR, PUT_SUBSCRIBE_SUCCESS, PUT_CHANNEL, DEL_CHANNEL, DEL_CHANNEL_LOADING, DEL_CHANNEL_ERROR, DEL_CHANNEL_SUCCESS, PUT_CHANNEL_LOADING, PUT_CHANNEL_ERROR, PUT_CHANNEL_SUCCESS } from '../actions/ActionTypes';
import { base_url, getToken } from "../constants/config";

export const checkResult = result => {
  if (result.status) {
    return true;
  }
  return false;
};

function* fetchChannelsData() {
  const token = yield select(getToken);
  yield put({type: FETCH_CHANNELS_LOADING, state: true});
  yield put({type: FETCH_CHANNELS_ERROR, state: null});
  try {
    const result = yield call(fetchChannelsDetails, token);
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_CHANNELS_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_CHANNELS_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_CHANNELS_ERROR, state: error});
  }
  yield put({type: FETCH_CHANNELS_LOADING, state: false});
}

function* fetchMyChannelsData() {
  const token = yield select(getToken);
  yield put({type: FETCH_MY_CHANNELS_LOADING, state: true});
  yield put({type: FETCH_MY_CHANNELS_ERROR, state: null});
  try {
    const result = yield call(fetchMyChannelsDetails, token);
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_MY_CHANNELS_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_MY_CHANNELS_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_MY_CHANNELS_ERROR, state: error});
  }
  yield put({type: FETCH_MY_CHANNELS_LOADING, state: false});
}

function* fetchChannelContentData({ channelId }) {
  const token = yield select(getToken);
  yield put({type: FETCH_CHANNEL_CONTENT_LOADING, state: true});
  yield put({type: FETCH_CHANNEL_CONTENT_ERROR, state: null});
  try {
    const result = yield call(fetchChannelContentDetails, {channelId, token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_CHANNEL_CONTENT_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_CHANNEL_CONTENT_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_CHANNEL_CONTENT_ERROR, state: error});
  }
  yield put({type: FETCH_CHANNEL_CONTENT_LOADING, state: false});
}

function* fetchChannelData({ channelId }) {
  const token = yield select(getToken);
  yield put({type: FETCH_CHANNEL_DETAILS_LOADING, state: true});
  yield put({type: FETCH_CHANNEL_DETAILS_ERROR, state: null});
  try {
    const result = yield call(fetchChannelDetails, {channelId, token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_CHANNEL_DETAILS_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_CHANNEL_DETAILS_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_CHANNEL_DETAILS_ERROR, state: error});
  }
  yield put({type: FETCH_CHANNEL_DETAILS_LOADING, state: false});
}


function* fetchChannelPopData({ channelId }) {
  const token = yield select(getToken);
  yield put({type: FETCH_CHANNEL_POPULAR_LOADING, state: true});
  yield put({type: FETCH_CHANNEL_POPULAR_ERROR, state: null});
  try {
    const result = yield call(fetchChannelPopDetails, {channelId, token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: FETCH_CHANNEL_POPULAR_SUCCESS, data});
    } else {
      yield put({
        type: FETCH_CHANNEL_DETAILS_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: FETCH_CHANNEL_POPULAR_ERROR, state: error});
  }
  yield put({type: FETCH_CHANNEL_POPULAR_LOADING, state: false});
}

function* putSubscribeData({ channelId }) {
  const token = yield select(getToken);
  yield put({type: PUT_SUBSCRIBE_LOADING, state: true});
  yield put({type: PUT_SUBSCRIBE_ERROR, state: null});
  try {
    const result = yield call(putSubscribeDetails, {channelId, token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: PUT_SUBSCRIBE_SUCCESS, data});
    } else {
      yield put({
        type: PUT_SUBSCRIBE_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: PUT_SUBSCRIBE_ERROR, state: error});
  }
  yield put({type: PUT_SUBSCRIBE_LOADING, state: false});
}

function* putChannelData({ payload }) {
  const token = yield select(getToken);
  yield put({type: PUT_CHANNEL_LOADING, state: true});
  yield put({type: PUT_CHANNEL_ERROR, state: null});
  try {
    const result = yield call(putChannelDetails, {payload, token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: PUT_CHANNEL_SUCCESS, data});
    } else {
      yield put({
        type: PUT_CHANNEL_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: PUT_CHANNEL_ERROR, state: error});
  }
  yield put({type: PUT_CHANNEL_LOADING, state: false});
}

function* delChannelData({ channelId }) {
  const token = yield select(getToken);
  yield put({type: DEL_CHANNEL_LOADING, state: true});
  yield put({type: DEL_CHANNEL_ERROR, state: null});
  try {
    const result = yield call(delChannelDetails, {channelId, token});
    if (checkResult(result)) {
      const {data} = result;
      yield put({type: DEL_CHANNEL_SUCCESS, data});
    } else {
      yield put({
        type: DEL_CHANNEL_ERROR,
        state: JSON.stringify(result.data),
      });
    }
  } catch (error) {
    yield put({type: DEL_CHANNEL_ERROR, state: error});
  }
  yield put({type: DEL_CHANNEL_LOADING, state: false});
}

function* ChannelsSaga() {
  yield takeEvery(FETCH_CHANNELS, fetchChannelsData);
  yield takeEvery(FETCH_MY_CHANNELS, fetchMyChannelsData);
  yield takeEvery(FETCH_CHANNEL_CONTENT, fetchChannelContentData);
  yield takeEvery(FETCH_CHANNEL_DETAILS, fetchChannelData);
  yield takeEvery(FETCH_CHANNEL_POPULAR, fetchChannelPopData);
  yield takeEvery(PUT_SUBSCRIBE, putSubscribeData);
  yield takeEvery(PUT_CHANNEL, putChannelData);
  yield takeEvery(DEL_CHANNEL, delChannelData);
}

const delChannelDetails = async ({ channelId, token }) => {
  const result = await fetch(
    `${base_url}/api/${channelId}/deletechannel`,
    {
      method: "DELETE",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      },
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const putChannelDetails = async ({ payload, token }) => {
  console.log("scfvjzd :" , payload)
  const result = await fetch(
    `${base_url}/api/${payload.channelId}/ChannelUpdate`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      },
      body: payload.formBody
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const putSubscribeDetails = async ({ channelId, token }) => {
  const result = await fetch(
    `${base_url}/api/${channelId}/subscribeChannel`,
    {
      method: "PUT",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      },
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const fetchChannelsDetails = async token => {
  const result = await fetch(
    `${base_url}/api/channels/?page=1`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const fetchMyChannelsDetails = async token => {
  const result = await fetch(
    `${base_url}/api/MyChannelListView/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const fetchChannelContentDetails = async ({channelId, token}) => {
  const result = await fetch(
    `${base_url}/api/${channelId}/ChannelVideosListView/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const fetchChannelDetails = async ({channelId, token}) => {
  const result = await fetch(
    `${base_url}/api/${channelId}/ChannelDetailView/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
};

const fetchChannelPopDetails = async ({ channelId, token}) => {
  const result = await fetch(
    `${base_url}/api/${channelId}/ChannelPopListView/`,
    {
      method: "GET",
      headers: {
        Accept: "application/json",
        Authorization: `Bearer ${token}`
      }
    }
  );
  return result.json().then(data => ({
    data: data,
    status: result.ok
  }));
}

export default ChannelsSaga;
