import { put, takeEvery, call, select } from "redux-saga/effects";
import {Alert} from "react-native"
import {
  POST_LOGOUT,
  POST_LOGOUT_ERROR,
  POST_LOGOUT_LOADING,
  POST_LOGOUT_SUCCESS,
  TOKEN_EMPTY,
  LOGIN_EMPTY,
  CLEAR_USER
} from "../actions/ActionTypes";
import { NavigationActions } from 'react-navigation'
import Navigationservice from "../navigation/navigation-service";

export function* checkResultCommon(result) {
  if(result.responseCode==401){
    const data={"data":{"success":true,"data":{},"errors":{"details":null}},"status":true};
    yield put({ type: POST_LOGOUT_SUCCESS, data });
    yield put({ type: TOKEN_EMPTY });
    yield put({ type: LOGIN_EMPTY });
    yield put({ type: USER_EMPTY });
    yield put({ type: CLEAR_USER });
    Alert.alert("Unauthorized Access","Sorry, your request could not be processed. Please re-login",
    [
      {text: 'OK', onPress: ()=>{Navigationservice.navigate("LoginScreen")}},
    ],
    { cancelable: false })
    
    return false;
  }else{
    if (result.status) {
      return true;
    }
    return false;
  }
}