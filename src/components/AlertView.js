import React, {Component} from 'react';
import {View, Text, StyleSheet} from 'react-native';
import { scale } from '../constants/Scale';
import { family } from '../constants/Fonts';

export default class AlertView extends Component {
  constructor(props){
    super(props)
    this.state={ 
      visible: true
    };
  }

  componentDidMount() {
    setTimeout(this.hideView, 4000);
  }

  hideView = () => {
    this.setState({ visible: false });
  };

  render() {
    const { type, message } = this.props;
    const backgroundColor = type === "success" ? "#007F00" : "#E22D39";
    const text = 
      type === "success"
        ? message
          ? message
          : "Successfully updated !"
        : message
        ? message
        : "Oops! Something went wrong!";
    return this.state.visible ? (
      <View style={[styles.bottomView, { backgroundColor}]}>
        <Text style={styles.textStyle}>{text}.</Text>
      </View>
    ) : null;
  }
}

const styles = StyleSheet.create({
  bottomView: {
    width: '100%',
    height: scale(30),
    backgroundColor: '#FF9800',
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    bottom: 0
  },
  textStyle: {
    fontSize: scale(10),
    color: 'white',
    fontFamily: family.semi_bold,
  }
})