import { Component } from "react";
import { StatusBar } from 'react-native';
export default class Header extends Component {
    render() {
        return (
        <View>
            <StatusBar backgroundColor="#0089D0" barStyle="dark-content" />
        </View>
    )}
}
