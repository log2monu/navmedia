import React, { Component } from "react";
import { View, Image, Text, StyleSheet, TouchableOpacity,TextInput } from 'react-native';
import {
    Menu,
    MenuProvider,
    MenuOptions,
    MenuOption,
    MenuTrigger,
  } from 'react-native-popup-menu';
import Icon1 from 'react-native-vector-icons/Feather';
import Icon2 from 'react-native-vector-icons/FontAwesome';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import Icon4 from 'react-native-vector-icons/Foundation';
import Icon5 from 'react-native-vector-icons/Octicons'
import Icon6 from 'react-native-vector-icons/AntDesign'
import Icon7 from 'react-native-vector-icons/MaterialIcons';
import Icon8 from 'react-native-vector-icons/FontAwesome5';
import { scale } from "../constants/Scale";
import { GoogleSignin } from '@react-native-community/google-signin';
import { family } from "../constants/Fonts";
import {connect} from "react-redux";
import { postBannerAd, postVideoAd } from '../actions/AdActions';
import Loader from '../components/Loader';
import { NavigationEvents } from "react-navigation";
import FloatingLabel from "react-native-floating-labels";
import ImagePicker from 'react-native-image-crop-picker';
import MultiSelect from 'react-native-multiple-select';
import Modal from "react-native-modal";
import { putUpdateProfile, getProfile } from "../actions/ProfileAction";
import { postConnectImage, postConnectVideo } from "../actions/ConnectAction";
import { connect_video } from "../reducers/ConnectReducer";
import { postLogOut } from "../actions/LogoutAction";
import Toast from "react-native-simple-toast";

class HeaderComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isAdModalVisible: false,
      isBannerAdModalVisible: false,
      isConnectModalVisible: false,
      connectImage: null,
      connectVideo: null,
      channel_name: "",
      channel_about: "",
      logoImage: null,
      channelBanner: null,
      videoAdTitle: "",
      videoAdDesc: "",
      videoAdCompanyName: "",
      videoAdPhoneNumber:"",
      video: null,
      videoImage: null,
      videoExtention: null,
      bannerTitle: "",
      bannerDesc: "",
      bannerPhoneNumber: "",
      bannerCompanyName: "",
      bannerImage: null,
      selectedAdCategories: [],
      adCategory: [
        {category: "Sports", id: "1"},
        {category: "Health", id: "2"},
        {category: "News", id: "3"},
        {category: "Climate", id: "4"},
        {category: "Entertainmet", id: "5"},
        {category: "Music", id: "6"},
      ],
      isMenuPressed: false,
      isUpdateProfileModalVisible: false,
      isPremium: this.props.premium,
      profileName: "",
      mobileNumber: "",
      profileImage: null
    };
  };

  componentDidMount() {
    this.props.fetchProfile();
  }

  // signOut = async () => {
  //   try {
  //     await GoogleSignin.revokeAccess();
  //     await GoogleSignin.signOut();
  //     this.props.navigation.navigate("LoginScreen");
  //   } catch (error) {
  //     console.error(error);
  //   }
  // };

  // signOut() {
  //   this.props.postLogOut();
  // }

  componentDidUpdate(prevProps) {
    if (this.props.banner_upload) {
      if (this.props.banner_upload.success &&
          !this.props.banner_upload.loading &&
          !this.props.banner_upload.error) {
            if(prevProps.banner_upload != this.props.banner_upload){
              this.toggleBannerAdsModal();
              Toast.showWithGravity('Banner Ad. uploaded!', Toast.LONG, Toast.CENTER)
            }     
      }
    }
    if (this.props.ad_upload) {
      if (this.props.ad_upload.success &&
          !this.props.ad_upload.loading &&
          !this.props.ad_upload.error) {
            if(prevProps.ad_upload != this.props.ad_upload){
              this.toggleAdsModal();
              Toast.showWithGravity('Video Ad. uploaded!', Toast.LONG, Toast.CENTER)
            }     
      }
    }
    if (this.props.connect_video) {
      if (this.props.connect_video.success &&
          !this.props.connect_video.loading &&
          !this.props.connect_video.error) {
            if(prevProps.connect_video != this.props.connect_video){
              this.toggleConnectModal();
              Toast.showWithGravity('Connect video uploaded!', Toast.LONG, Toast.CENTER)
            }     
      }
    }
    if (this.props.connect_image) {
      if (this.props.connect_image.success &&
          !this.props.connect_image.loading &&
          !this.props.connect_image.error) {
            if(prevProps.connect_image != this.props.connect_image){
              this.toggleConnectModal();
              Toast.showWithGravity('Connect image uploaded!', Toast.LONG, Toast.CENTER)
            }     
      }
    }
    if (this.props.logout) {
      if (this.props.logout.success &&
          !this.props.logout.loading &&
          !this.props.logout.error) {
            if(prevProps.logout != this.props.logout){
              if (this.props.token == null) {
                this.props.navigation.navigate("LoginScreen")
              }
            }     
      }
    }
    // if (this.props.update_profile) {
    //   if (this.props.update_profile.success &&
    //       !this.props.update_profile.loading &&
    //       !this.props.update_profile.error) {
            
    //         if(prevProps.update_profile != this.props.update_profile){
    //           this.setState({ isUpdateProfileModalVisible: false});
    //           Toast.showWithGravity('Profile updated!', Toast.LONG, Toast.CENTER)
    //           if (this.props.update_profile.userData.is_premium){
    //             this.setState({isPremium: true})
    //           } else {
    //             this.setState({isPremium: false})
    //           }
    //         }     
    //   }
    // }
  }

  pickLogoImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        logoImage: image.path
      });
      // console.log("cdbjskv", image)
    }).catch(e => alert(e));
  }

  pickBannerImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        channelBanner: image.path
      });
    }).catch(e => alert(e));
  }

  pickAdVideoImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        videoImage: image.path
      });
    }).catch(e => alert(e));
  }
  pickAdBannerImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        bannerImage: image.path
      });
    }).catch(e => alert(e));
  }

  pickConnectImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        connectImage: image.path
      });
    }).catch(e => alert(e));
  }

  pickVideo() {
    ImagePicker.openPicker({
      mediaType: "video",
    }).then((video) => {
      // this.setState({ video : video.path, videoExtention: fileType(video.path)})
      this.setState({ video : video.path,})

    });
  }

  pickConnectVideo() {
    ImagePicker.openPicker({
      mediaType: "video",
    }).then((video) => {
      // this.setState({ video : video.path, videoExtention: fileType(video.path)})
      this.setState({ connectVideo : video.path,})

    });
  }
  onSelectedCategoriesChange = selectedItems => {
    this.setState({ selectedItems });
  };

  render() {
    const { ad_upload, banner_upload, logout, update_profile, connect_image, connect_video, profile } = this.props;
    let userImageFile = this.props.user_info && this.props.user_info.user ? this.props.user_info.user.photo : null;
    let userImage = this.state.profileImage ? this.state.profileImage : userImageFile;
    let isLoading = ad_upload.loading || banner_upload.loading || update_profile.loading || connect_image.loading || connect_video.loading || logout.loading ? true : false;
    const userImageData = profile && profile.userData && profile.userData.profile && profile.userData.profile.image ? profile.userData.profile.image : null;
    return (
        <View style={styles.navBar}>
            <Loader loading={isLoading} /> 
            <TouchableOpacity style={{alignItems: 'center', justifyContent: 'center'}} onPress={()=> this.props.navigation.navigate("Home")}>
              <Image
              source={require('../assets/logo.png')}
              style={{width: 98, height: 35}}
              />
            </TouchableOpacity>
            <View style={styles.rightNav}>
                

            <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}}>

              <Menu>
                <MenuTrigger  >
                    <View style={{alignItems: 'center', justifyContent: 'center'}}>
                      <Icon8 name={"adversal"} color={'grey'} size={20} style={{padding: 15}}/>
                      {/* <Text style={{color: 'grey', fontFamily: family.bold, paddingHorizontal: 10, fontSize: 15, opacity: 0.75}}>Ad.</Text> */}
                    </View>
                </MenuTrigger>
                <MenuOptions>
                    <MenuOption onSelect={() => this.setState({isAdModalVisible: !this.state.isAdModalVisible})} >
                        <View style={{flexDirection: 'row', marginHorizontal: 10, alignItems: 'center', paddingVertical: 10}}>
                            <Icon1  name="video" size={18} color={'grey'} />
                            <Text style={{color: 'grey', marginHorizontal: 15}}>Upload Advert</Text>
                        </View>
                    </MenuOption>
                    <MenuOption onSelect={() => this.setState({isBannerAdModalVisible: !this.state.isBannerAdModalVisible})} >
                        <View style={{flexDirection: 'row', marginHorizontal: 10, alignItems: 'center', paddingVertical: 10}}>
                          <Icon1  name="image" size={18} color={'grey'} />
                            <Text style={{color: 'grey', marginHorizontal: 15}}>Upload Banner</Text>
                        </View>
                    </MenuOption>
                </MenuOptions>
              </Menu>

              <TouchableOpacity style={{}} onPress={()=> this.toggleConnectModal()}>
                    <Icon1 name={'wifi'} color={'grey'} size={23}/>
              </TouchableOpacity>

                <TouchableOpacity style={{marginHorizontal: scale(15)}} onPress={()=> this.props.navigation.navigate('SearchScreen')}>
                    <Icon5 name={'search'} color={'grey'} size={18}/>
                </TouchableOpacity>
            </View>
            <Menu>
                <MenuTrigger >
                    {(this.props.user_info && this.props.user_info.image) || userImageData ? 
                      <Image
                        style={{borderRadius: 100, width: 32, height: 32,}}
                        source={{uri: userImageData ? userImageData : this.props.user_info.image}}
                    />:
                    <Image
                        style={{borderRadius: 100, width: 32, height: 32,}}
                        source={require("../assets/user.png")}
                    />}
                    
                </MenuTrigger>
                <MenuOptions>
                    <MenuOption onSelect={() => this.props.navigation.navigate("Profile")} >
                        <View style={{flexDirection: 'row', marginHorizontal: 10, alignItems: 'center', paddingVertical: 5}}>
                            <Icon2  name="user-circle" size={15} color={'grey'} />
                            <Text style={{color: 'grey', marginHorizontal: 8}}>My Account</Text>
                        </View>
                    </MenuOption>
                    <MenuOption onSelect={() => this.toggleModal()} >
                        <View style={{flexDirection: 'row', marginHorizontal: 8, alignItems: 'center', paddingVertical: 5}}>
                            <Icon3  name="update" size={18} color={'grey'} />
                            <Text style={{color: 'grey', marginHorizontal: 6}}>Update Profile</Text>
                        </View>
                    </MenuOption>
                    <MenuOption onSelect={() => this.props.navigation.navigate("MyChannel")} >
                        <View style={{flexDirection: 'row', marginHorizontal: 10, alignItems: 'center', paddingVertical: 5}}>
                            <Icon1  name="video" size={15} color={'grey'} />
                            <Text style={{color: 'grey', marginHorizontal: 8}}>Subscriptions</Text>
                        </View>
                    </MenuOption>
                    <MenuOption onSelect={() => this.props.navigation.navigate('Settings')} >
                        <View style={{flexDirection: 'row', marginHorizontal: 10, alignItems: 'center', paddingVertical: 5}}>
                            <Icon1  name="settings" size={15} color={'grey'} />
                            <Text style={{color: 'grey', marginHorizontal: 8}}>Settings</Text>
                        </View>
                    </MenuOption>
                    <MenuOption onSelect={() => this.signOut()} >
                        <View style={{flexDirection: 'row', marginHorizontal: 10, alignItems: 'center', paddingVertical: 10, }}>
                            <Icon3  name="logout" size={15} color={'grey'} />
                            <Text style={{color: 'grey', marginHorizontal: 8, fontWeight: '500'}}>Logout</Text>
                        </View>
                    </MenuOption>
                </MenuOptions>
            </Menu>

            
            </View>
            {this.renderModal()}
            {this.renderAdsModal()}
            {this.renderBannerAdsModal()}
            {this.renderConnectModal()}
        </View>
    );
  }

  renderAdsModal() {
    const { update_profile } = this.props;
    let isPremium = update_profile && update_profile.userData &&  update_profile.userData.is_premium ? update_profile.userData.is_premium : false;

    return (
      <Modal
        visible={this.state.isAdModalVisible}
        onBackButtonPress={()=>this.toggleAdsModal()}
        onBackdropPress={()=> this.toggleAdsModal()}
        backdropColor="black"
        backdropOpacity={0.4}
        // style={{flex: 1, backgroundColor: 'grey'}}
        style={{justifyContent: 'center',margin: scale(0), backgroundColor: 'rgba(0,0,0,0.7)',}}
      >
        <View style={styles.modalContent}>
        <Text style={{fontFamily: family.bold, fontSize: scale(14), marginVertical: scale(15), color: "#0089D0"}}>Post Video Ad.</Text>
          {this.state.isPremium ? 
          <View >
            <Icon1 name={"upload-cloud"} color={"#0089D0"} size={35} style={{alignSelf: 'center'}}/>
            <View style={{paddingHorizontal: 45, paddingTop: 15}}>
              <Text style={{color: 'grey', fontFamily: family.regular}}>Upload your bussiness ads to share through our application and website</Text>
            </View>
          </View>
           : null}
          
          {this.state.isPremium ?
          (
          <View style={{width: '80%'}}>  
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon3 name={"format-title"} color={'grey'} size={20} />
              <TextInput
                placeholder={'Title'}
                placeholderTextColor={'grey'}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
                onChangeText={(value)=> this.setState({ videoAdTitle: value})}
              />  

            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon1 name={"file-text"} color={'grey'} size={20} />
              <TextInput
                placeholder={'Description'}
                placeholderTextColor={'grey'}
                onChangeText={(value)=> this.setState({ videoAdDesc: value})}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
              />  

            </View>

          <View style={{  marginTop: 15, width : '100%', justifyContent: 'space-between', paddingHorizontal:  35}}>
            <TouchableOpacity 
              onPress={() => this.pickVideo()}
              style={{ backgroundColor: 'white', elevation: 1 , width: '100%', paddingVertical: 4, borderRadius: 5 }}
            >
              {!this.state.video ? 
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center',  paddingHorizontal: 20, paddingVertical: 4}}>
                <Text style={{alignSelf: 'center', marginRight: 5, color: 'grey'}}>Select video</Text>
                <Icon1 name={'video'} size={16} color={'grey'}/>
              </View>:
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center',  paddingHorizontal: 20, paddingVertical: 4}}>
                <Text style={{alignSelf: 'center', marginRight: 5, color: 'grey'}}>Video selected</Text>
                <Icon6 name={'like2'} size={16} color={'#0089D0'}/>
              </View>}
            </TouchableOpacity>  

          </View>
            

            <View style={{}}>
              <TouchableOpacity 
                onPress={() => this.pickAdVideoImage()}
                style={{  flexDirection : 'row', borderRadius: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', width: '78%', paddingVertical: 8, marginVertical: 10, elevation: 1, alignSelf: 'center' }}
              >
                <Icon1 name={"image"} color={'grey'} size={16} />
                <Text style={{  color: 'grey', fontFamily: family.regular, marginLeft: 6}}>Select image</Text>
              </TouchableOpacity> 
            
            </View>
            {this.state.videoImage && 
                <Image  
                  style={{width: '90%', height: 100, borderRadius: 4, alignSelf: 'center'}}
                  source={{uri: this.state.videoImage}}
                /> } 

            <TouchableOpacity
              onPress={() => this.uploadVideoAd()}
              style={{
                borderRadius: 5,
                width: '80%',
                marginVertical: scale(15),
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                paddingVertical: 4,
                flexDirection: 'row',
                elevation: 1
              }}>
              <Icon1 name={"upload"} color={'#0089D0'} size={16} />
              <Text style={{color: '#0089D0', fontSize: scale(12), paddingHorizontal: scale(10), paddingVertical: scale(6), fontFamily: family.bold }}>Upload</Text>
            </TouchableOpacity> 
          </View>   ) :
          (
            <View style={{}}>
              <Text style={{
                fontFamily: family.regular,                  
                  paddingHorizontal: scale(15),
                  paddingVertical: scale(6),
              }}>Only Premium members can post ads.</Text>
              <TouchableOpacity
              style={{
                borderRadius: 3,
                backgroundColor: 'white',
                borderColor: 'grey',
                elevation: 2,
                marginVertical: 20,
                alignItems: 'center'
              }}
              onPress={() => {
                this.toggleAdsModal()
                this.props.navigation.navigate("Settings")             
              }}>
              <Text
                style={{
                  fontFamily: family.condensed_bold,                  
                  paddingHorizontal: scale(15),
                  paddingVertical: scale(6),
                }}>
                Go Premium
              </Text>
            </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    )
  }

  renderBannerAdsModal() {
    const { update_profile } = this.props;
    return (
      <Modal
        visible={this.state.isBannerAdModalVisible}
        onBackButtonPress={()=>this.toggleBannerAdsModal()}
        onBackdropPress={()=> this.toggleBannerAdsModal()}
        backdropColor="black"
        backdropOpacity={0.4}
        // style={{flex: 1, backgroundColor: 'grey'}}
        style={{justifyContent: 'center',margin: scale(0), backgroundColor: 'rgba(0,0,0,0.7)',}}
      >
        <View style={styles.modalContent}>
          <Text style={{fontFamily: family.bold, fontSize: scale(14), marginVertical: scale(15), color: "#0089D0"}}>Post Banner Ad.</Text>
          {this.state.isPremium ? 
            <View >
              <Icon1 name={"upload-cloud"} color={"#0089D0"} size={35} style={{alignSelf: 'center'}}/>
              <View style={{paddingHorizontal: 45, paddingTop: 15}}>
                <Text style={{color: 'grey', fontFamily: family.regular}}>Upload your bussiness ads to share through our application and website</Text>
              </View>
            </View>
           : null}
          {this.state.isPremium ?
          (
          <View style={{width: '80%', }}>  
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon3 name={"format-title"} color={'grey'} size={20} />
              <TextInput
                placeholder={'Title'}
                placeholderTextColor={'grey'}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
                onChangeText={(value)=> this.setState({ bannerTitle: value})}
              />  

            </View>

            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon1 name={"file-text"} color={'grey'} size={20} />
              <TextInput
                placeholder={'Description'}
                placeholderTextColor={'grey'}
                onChangeText={(value)=> this.setState({ bannerDesc: value})}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
              />  

            </View>

            <View style={{flexDirection: 'row',  marginTop: 8, width : '100%', justifyContent: 'space-between', paddingHorizontal:  35, alignItems: 'center'}}>
              <TouchableOpacity 
                onPress={() => this.pickAdBannerImage()}
                style={{  flexDirection : 'row', borderRadius: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', width: '100%', paddingVertical: 8, marginVertical: 10, elevation: 1 }}
              >
                <Icon1 name={"image"} color={'grey'} size={16} />
                <Text style={{  color: 'grey', fontFamily: family.regular, marginLeft: 6}}>Select image</Text>
              </TouchableOpacity> 
            
            </View>
            {this.state.bannerImage && 
                <Image  
                  style={{width: '90%', height: 100, borderRadius: 4, alignSelf: 'center'}}
                  source={{uri: this.state.bannerImage}}
                /> } 

            <TouchableOpacity
              onPress={() => this.uploadBanner()}
              style={{
                borderRadius: 5,
                width: '80%',
                marginVertical: scale(15),
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                paddingVertical: 4,
                flexDirection: 'row',
                elevation: 1
              }}>
              <Icon1 name={"upload"} color={'#0089D0'} size={16} />
              <Text style={{color: '#0089D0', fontSize: scale(12), paddingHorizontal: scale(10), paddingVertical: scale(6), fontFamily: family.bold }}>Upload</Text>
            </TouchableOpacity> 
          </View>   ) :
          (
            <View style={{}}>
              <Text style={{
                fontFamily: family.regular,                  
                  paddingHorizontal: scale(15),
                  paddingVertical: scale(6),
              }}>Only Premium members can post ads.</Text>
              <TouchableOpacity
              style={{
                borderRadius: 3,
                backgroundColor: 'white',
                borderColor: 'grey',
                elevation: 2,
                marginVertical: 20,
                alignItems: 'center'
              }}
              onPress={() => {
                this.toggleBannerAdsModal()
                this.props.navigation.navigate("Settings")             
              }}>
              <Text
                style={{
                  fontFamily: family.condensed_bold,                  
                  paddingHorizontal: scale(15),
                  paddingVertical: scale(6),
                }}>
                Go Premium
              </Text>
            </TouchableOpacity>
            </View>
          )}
        </View>
      </Modal>
    )
  }

  renderConnectModal() {
    const { selectedItems, adCategory } = this.state;

    return (
      <Modal
        visible={this.state.isConnectModalVisible}
        onBackButtonPress={()=>this.toggleConnectModal()}
        onBackdropPress={()=> this.toggleConnectModal()}
        backdropColor="black"
        backdropOpacity={0.4}
        // style={{flex: 1, backgroundColor: 'grey'}}
        style={{justifyContent: 'center',margin: scale(0), backgroundColor: 'rgba(0,0,0,0.7)',}}
      >
        <View style={styles.modalContent}>
        <Text style={{fontFamily: family.bold, fontSize: scale(14), marginVertical: scale(15), color: "#0089D0"}}>Connect</Text>
          <Icon1 name={"wifi"} color={"#0089D0"} size={35} style={{alignSelf: 'center'}}/>
          <View style={{paddingHorizontal: 45, paddingTop: 15}}>
            <Text style={{color: 'grey', fontFamily: family.regular}}>Commincation facility for users to reach nav media to share us your local news</Text>
          </View>
          <View style={{width: '80%'}}>  

          <View style={{  marginTop: 15, width : '100%', justifyContent: 'space-between', paddingHorizontal:  35}}>
            <Text style={{fontFamily: family.bold, fontSize: 14, color: 'grey', marginVertical: 8, textAlign: 'center'}}>Connect video upload</Text>
            <TouchableOpacity 
              onPress={() => this.pickConnectVideo()}
              style={{ backgroundColor: 'white', elevation: 1 , width: '100%', paddingVertical: 4, borderRadius: 5 }}
            >
              {!this.state.connectVideo ? 
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center',  paddingHorizontal: 20, paddingVertical: 4}}>
                <Text style={{alignSelf: 'center', marginRight: 5, color: 'grey'}}>Select video</Text>
                <Icon1 name={'video'} size={16} color={'grey'}/>
              </View>:
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center',  paddingHorizontal: 20, paddingVertical: 4}}>
                <Text style={{alignSelf: 'center', marginRight: 5, color: 'grey'}}>Video selected</Text>
                <Icon6 name={'like2'} size={16} color={'#0089D0'}/>
              </View>}
            </TouchableOpacity>  
            <TouchableOpacity
              onPress={() => this.connectVideoUpload()}
              style={{
                borderRadius: 5,
                width: '100%',
                marginVertical: scale(15),
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                paddingVertical: 4,
                flexDirection: 'row',
                elevation: 1
              }}>
              <Icon1 name={"upload"} color={'#0089D0'} size={16} />
              <Text style={{color: '#0089D0', fontSize: scale(12), paddingHorizontal: scale(10), paddingVertical: scale(6), fontFamily: family.bold }}>Upload</Text>
            </TouchableOpacity> 

          </View>
            
            <View style={{}}>
            <Text style={{fontFamily: family.bold, fontSize: 14, color: 'grey', marginVertical: 8, textAlign: 'center', marginTop: 10}}>Connect image upload</Text>

              <TouchableOpacity 
                onPress={() => this.pickConnectImage()}
                style={{  flexDirection : 'row', borderRadius: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', width: '78%', paddingVertical: 8, marginVertical: 10, elevation: 1, alignSelf: 'center' }}
              >
                <Icon1 name={"image"} color={'grey'} size={16} />
                <Text style={{  color: 'grey', fontFamily: family.regular, marginLeft: 6}}>Select image</Text>
              </TouchableOpacity> 
            
            </View>
            {this.state.connectImage && 
                <Image  
                  style={{width: '90%', height: 100, borderRadius: 4, alignSelf: 'center'}}
                  source={{uri: this.state.connectImage}}
                /> } 

            <TouchableOpacity
              onPress={() => this.connectImageUpload()}
              style={{
                borderRadius: 5,
                width: '80%',
                marginVertical: scale(15),
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                paddingVertical: 4,
                flexDirection: 'row',
                elevation: 1
              }}>
              <Icon1 name={"upload"} color={'#0089D0'} size={16} />
              <Text style={{color: '#0089D0', fontSize: scale(12), paddingHorizontal: scale(10), paddingVertical: scale(6), fontFamily: family.bold }}>Upload</Text>
            </TouchableOpacity> 
          </View>   
        </View>
      </Modal>
    )
  }

  renderModal =() => {
    return (
      <Modal
        visible={this.state.isUpdateProfileModalVisible}
        onBackButtonPress={()=>this.toggleModal()}
        onBackdropPress={()=> this.toggleModal()}
        backdropColor="black"
        backdropOpacity={0.4}
        // style={{flex: 1, backgroundColor: 'grey'}}
        style={{justifyContent: 'center',margin: scale(0), backgroundColor: 'rgba(0,0,0,0.7)',}}
      >
        <View style={styles.modalContent}>
          <Text style={{fontFamily: family.bold, fontSize: scale(14), marginVertical: scale(15), color: "#0089D0"}}>Update Profile</Text>
          <View >
            {/* <Icon3 name={"user-edit"} color={"#0089D0"} size={35} style={{alignSelf: 'center'}}/> */}
            <View style={{paddingHorizontal: 45, paddingBottom: 15}}>
              <Text style={{color: 'grey', fontFamily: family.regular}}>Update your profile details before proceeding.</Text>
            </View>
          </View>

         
          {this.state.profileImage ?
            <Image  
              style={{width: 120, height: 120, borderRadius: 100, alignSelf: 'center'}}
              source={{uri: this.state.profileImage}}
            /> :
            <Image  
              style={{width: '35%', height: '24%', borderRadius: 100, alignSelf: 'center'}}
              source={this.props.user_info && this.props.user_info.image ? {uri: this.props.user_info.image} : require("../assets/user.png")}
            />
          } 

          <View style={{flexDirection: 'row',  marginTop: 8, width : '100%', justifyContent: 'space-between', paddingHorizontal:  35, alignItems: 'center'}}>
            <TouchableOpacity 
              onPress={() => this.pickProfileImage()}
              style={{  flexDirection : 'row', borderRadius: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', width: '100%', paddingVertical: 5, marginVertical: 10, elevation: 1 }}
            >
              <Icon3 name={"update"} color={'grey'} size={16} />
              <Text style={{  color: 'grey', fontFamily: family.regular, marginLeft: 6}}>Update image</Text>
            </TouchableOpacity>      
          </View>

          <View style={{width: '80%', }}>  
          {/* <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon1 name={"user"} color={'grey'} size={20} />
              <TextInput
                value={this.props.user_info && this.props.user_info.user ? this.props.user_info.user.name: ""}
                placeholderTextColor={'grey'}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
                onChangeText={(value)=> this.setState({ profileName: value})}
              />  
          </View> */}

          <View style={{width: '80%', }}>  
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon6 name={"mobile1"} color={'grey'} size={20} />
              <TextInput
                placeholder={"Mobile Number"}
                placeholderTextColor={'grey'}
                keyboardType={'number-pad'}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
                onChangeText={(value)=> this.setState({ mobileNumber: value})}
              />  
          </View>
          </View>
            

            <TouchableOpacity
              onPress={() => this.updateProfile()}
              style={{
                borderRadius: 5,
                width: '80%',
                marginVertical: scale(15),
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                paddingVertical: 4,
                flexDirection: 'row',
                elevation: 1
              }}>
              <Icon1 name={"upload"} color={'#0089D0'} size={16} />
              <Text style={{color: '#0089D0', fontSize: scale(12), paddingHorizontal: scale(10), paddingVertical: scale(6), fontFamily: family.bold }}>Update</Text>
            </TouchableOpacity> 
          </View>  
        </View>
      </Modal>
    )
  }

  toggleModal=()=> {
    this.setState({
      isUpdateProfileModalVisible: !this.state.isUpdateProfileModalVisible
    })
  }

  pickProfileImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        profileImage: image.path
      });
    }).catch(e => alert(e));
  }

  updateProfile =() => {
    const {profileImage, mobileNumber } = this.state;
    const imageData = {uri: profileImage, name: "banner.jpeg", type: 'image/jpeg'};
    const data ={
      image: imageData,
      phone: mobileNumber,
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }
    
    this.props.putProfile(formBody)

  }

  connectImageUpload = () => {
    const { connectImage } = this.state;
    console.log("imgURI :", connectImage)
    const imageData = {uri: connectImage, name:"connect.jpeg", type: 'image/jpeg'};
    const data = {
      image: imageData
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }
    
    this.props.postConnectImage(formBody)

  }

  connectVideoUpload = () => {
    const { connectVideo } = this.state;
    console.log("vidURI :", connectVideo)
    const videoData = {uri: connectVideo, name: "connect_video.mp4", type: "video/mp4"}
    const data = {
      vid: videoData
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }
    
    this.props.postConnectVideo(formBody)

  }

  uploadBanner = () => {
    const {bannerImage, bannerTitle, bannerDesc} = this.state;
    const imageData = {uri: bannerImage, name: "banner.jpeg", type: 'image/jpeg'};
    const data ={
      image: imageData,
      title: bannerTitle,
      Targetview: 1000,
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }
    
    this.props.postBannerAd(formBody)

  }

  uploadVideoAd = () => {
    const {videoAdTitle, videoAdDesc, videoImage, video} = this.state;
    const imageData = {uri: videoImage, name: "banner.jpeg", type: 'image/jpeg'};
    const videoData = {uri: video, name: "adVideo.mp4", type: "video/mp4"}
    const data ={
      image: imageData,
      name: videoAdTitle,
      desc: videoAdDesc,
      vid: videoData,
      targetview: 1000
      };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }
    
    this.props.postVideoAd(formBody)

  }

  signOut = () => {
    const data ={
      client_id: "xsBYPRPiqUAH6JT0jPeOAwjoMDAl1IR74P9XSmTz",
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }
    this.props.postLogOut(formBody)
  } 

  // signOut = () => {

  //   const data ={
  //     client_id: "xsBYPRPiqUAH6JT0jPeOAwjoMDAl1IR74P9XSmTz",
  //     client_secret: "g8Ymi2bfqMu57MqedipQWiUFzNb6YAS0qnLFrZ5nlvfavFYIlWcNHQ43yxXqI8GBaWJcT5MQnP7cgLBQabihj3weR3z5KMILcSB48FD7X1q7HD8zJiqKjj1cX2jfgwtX",
  //     token: this.props.token
  //   };
  //   let formBody = new FormData();
  //   for (let key in data) {
  //     formBody.append(key, data[key]);
  //   }
    
  //   this.props.postLogOut(formBody)

  // }

  toggleAdsModal = () => {
    this.setState({ isAdModalVisible: !this.state.isAdModalVisible });
  };

  toggleBannerAdsModal = () => {
    this.setState({ isBannerAdModalVisible: !this.state.isBannerAdModalVisible });
  };

  toggleConnectModal = () => {
    this.setState({ isConnectModalVisible: !this.state.isConnectModalVisible });
  };

}

const styles = StyleSheet.create({
    navBar: {
        paddingVertical: scale(5),
        backgroundColor: 'white',
        elevation: 3,
        paddingHorizontal: 15,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
      },
      rightNav: {
        flexDirection: 'row',
        width: '46%',
        justifyContent: 'space-between',
        alignItems: 'center',
      },
      navItem: {
        marginLeft: 25,
      },
      modalContent:{
        backgroundColor: 'white',
        // padding: scale(10),
        // justifyContent: 'center',
        // alignItems: 'center',
        marginHorizontal: 20,
        borderRadius: scale(8),
        borderColor: 'rgba(0, 0, 0, 0.3)',
        borderWidth: 0.6,
        alignItems: 'center'
      },
})

const mapStateToProps = ({ banner_upload, ad_upload, user_info, update_profile, connect_image, connect_video, profile, logout, token, premium }) => {
  return {
    banner_upload,
    ad_upload,
    user_info,
    update_profile,
    connect_image,
    connect_video,
    profile,
    logout,
    token,
    premium
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postBannerAd: (postData) => {
      dispatch(postBannerAd(postData));
    },
    postVideoAd: (postData) => {
      dispatch(postVideoAd(postData));
    },
    putProfile: (postData) => {
      dispatch(putUpdateProfile(postData));
    },
    postConnectImage: (postData) => {
      dispatch(postConnectImage(postData));
    },
    postConnectVideo: (postData) => {
      dispatch(postConnectVideo(postData));
    },
    postLogOut: (params) => {
      dispatch(postLogOut(params));
    },
    fetchProfile: (postData) => {
      dispatch(getProfile(postData));
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderComponent);