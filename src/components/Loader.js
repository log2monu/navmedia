import React, {Component} from 'react';
import {StyleSheet, View, Modal, ActivityIndicator} from 'react-native';
import { scale } from '../constants/Scale';
import Colors from '../constants/Colors';
// import AnimatedLoader from "react-native-animated-loader";

const Loader = props => {
  const {loading} = props;

  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={loading}
      onRequestClose={() => {}}
    >
      <View style={styles.modalBackground}>
        <View style={styles.activityIndicatorWrapper}>
          <ActivityIndicator animating={loading} color={'#0089D0'} size={'small'}/>
          {/* <AnimatedLoader
              visible={loading}
              overlayColor="rgba(255,255,255,0.75)"
              source={require('../../constants/animated-loader/preloader.json')}
              animationStyle={styles.lottie}
              speed={1}
            /> */}
        </View>
      </View>
    </Modal> 
  );
};

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: "center",
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#FFFFFF',
    height: scale(50),
    width: scale(50),
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-around',
    borderRadius: 50
  },
  lottie: {
    width: 100,
    height: 100
  }
});

export default Loader;