import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  StyleSheet,
  StatusBar,
  Dimensions,
  BackHandler,
  Animated,
  Image,
  Alert,
  View, TouchableOpacity,ScrollView
} from "react-native";
// import VideoPlayer from "";
import KeepAwake from "react-native-keep-awake";
import Orientation from "react-native-orientation";
import Icons from "react-native-vector-icons/MaterialIcons";
import { VimeoControls } from "./";
import { checkSource } from "./utils";
import Modal from 'react-native-modal';
import {family,size} from "../../constants/Fonts";
import { WebView } from 'react-native-webview';
import { scale } from "../../constants/Scale";
import { ToggleIcon, Time, Scrubber } from "./";
const Win = Dimensions.get("window");
const backgroundColor = "#000";

const styles = StyleSheet.create({
  background: {
    backgroundColor,
    // justifyContent: "center",
    // alignItems: "center",
    zIndex: 2
  },
  fullScreen: {
    ...StyleSheet.absoluteFillObject
  },
  image: {
    ...StyleSheet.absoluteFillObject,
    width: undefined,
    height: undefined,
    zIndex: 99
  }
});

const defaultTheme = {
  title: "#FFF",
  more: "#FFF",
  center: "#FFF",
  fullscreen: "#FFF",
  volume: "#FFF",
  scrubberThumb: "#FFF",
  scrubberBar: "#FFF",
  seconds: "#FFF",
  duration: "#FFF",
  progress: "#FFF",
  loading: "#FFF"
};

class Vimeo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inlineHeight: Win.width * 0.5625,
      backgroundColor:'#FFF',
      fullScreen: false,
      webViewOpacity:0
    };
    this.animInline = new Animated.Value(Win.width * 0.5625);
    this.animFullscreen = new Animated.Value(Win.width * 0.5625);
    this.BackHandler = this.BackHandler.bind(this);
    this.onRotated = this.onRotated.bind(this);
  }

  componentDidMount() {
    Dimensions.addEventListener("change", this.onRotated);
    BackHandler.addEventListener("hardwareBackPress", this.BackHandler);
  }

  componentWillUnmount() {
    Dimensions.removeEventListener("change", this.onRotated);
    BackHandler.removeEventListener("hardwareBackPress", this.BackHandler);
  }

  toggleFS() {
    this.setState({ fullScreen: !this.state.fullScreen }, () => {
      Orientation.getOrientation((e, orientation) => {
        if (this.state.fullScreen) {
          const initialOrient = Orientation.getInitialOrientation();
          const height = orientation !== initialOrient ? Win.width : Win.height;
          this.props.onFullScreen(this.state.fullScreen);
          if (this.props.rotateToFullScreen) Orientation.lockToLandscape();
          this.animToFullscreen(height);
        } else {
          if (this.props.fullScreenOnly) {
            this.setState({ paused: true }, () =>
              this.props.onPlay(!this.state.paused)
            );
          }
          this.props.onFullScreen(this.state.fullScreen);
          if (this.props.rotateToFullScreen) Orientation.lockToPortrait();
          this.animToInline();
          setTimeout(() => {
            if (!this.props.lockPortraitOnFsExit)
              Orientation.unlockAllOrientations();
          }, 1500);
        }
      });
    });
  }

  onRotated({ window: { width, height } }) {
    // Add this condition incase if inline and fullscreen options are turned on
    if (this.props.inlineOnly) return;
    const orientation = width > height ? "LANDSCAPE" : "PORTRAIT";
    if (this.props.rotateToFullScreen) {
      if (orientation === "LANDSCAPE") {
        this.setState({ fullScreen: true }, () => {
          this.animToFullscreen(height);
          this.props.onFullScreen(this.state.fullScreen);
        });
        return;
      }
      if (orientation === "PORTRAIT") {
        this.setState(
          {
            fullScreen: false,
            paused: this.props.fullScreenOnly || this.state.paused
          },
          () => {
            this.animToInline();
            if (this.props.fullScreenOnly)
              this.props.onPlay(!this.state.paused);
            this.props.onFullScreen(this.state.fullScreen);
          }
        );
        return;
      }
    } else {
      this.animToInline();
    }
    if (this.state.fullScreen) this.animToFullscreen(height);
  }

  BackHandler() {
    if (this.state.fullScreen) {
      this.setState({ fullScreen: false }, () => {
        this.animToInline();
        this.props.onFullScreen(this.state.fullScreen);
        if (this.props.fullScreenOnly && !this.state.paused) this.togglePlay();
        if (this.props.rotateToFullScreen) Orientation.lockToPortrait();
        setTimeout(() => {
          if (!this.props.lockPortraitOnFsExit)
            Orientation.unlockAllOrientations();
        }, 1500);
      });
      return true;
    }
    return false;
  }

  animToFullscreen(height) {
    Animated.parallel([
      Animated.timing(this.animFullscreen, { toValue: height, duration: 200 }),
      Animated.timing(this.animInline, { toValue: height, duration: 200 })
    ]).start();
  }

  animToInline(height) {
    const newHeight = height || this.state.inlineHeight;
    Animated.parallel([
      Animated.timing(this.animFullscreen, {
        toValue: newHeight,
        duration: 100
      }),
      Animated.timing(this.animInline, {
        toValue: this.state.inlineHeight,
        duration: 100
      })
    ]).start();
  }

  getVimeoID(url) {
    let vimeo_Reg = /https?:\/\/(?:www\.)?vimeo.com\/(?:channels\/(?:\w+\/)?|groups\/([^\/]*)\/videos\/|album\/(\d+)\/video\/|)(\d+)(?:$|\/|\?)/;
    var match = url.match(vimeo_Reg);
    if (match){
      return match[3];
    }else{
      return "";
    }
  }
  
  render() {
    const {
      fullScreen,
      inlineHeight,
    } = this.state;
    const inline = {
      height: inlineHeight,
      alignSelf: "stretch"
    };
    const {
      url,
      loop,
      title,
      logo,
      rate,
      style,
      volume,
      placeholder,
      theme,
      onTimedMetadata,
      resizeMode,
      onMorePress,
      inlineOnly,
      playInBackground,
      playWhenInactive
    } = this.props;
    const setTheme = {
      ...defaultTheme,
      ...theme
    };
    var videoId=this.getVimeoID(url)
    // console.warn(videoId)
    const errorPage = () => {
      return (<Text>{'page not found'}</Text>)
    }
    return (
      <Animated.View style={[
        styles.background,
        fullScreen
          ? (styles.fullScreen, { height: this.animFullscreen })
          : { height: this.animInline },
        fullScreen ? null : style
      ]}>
        <StatusBar hidden={fullScreen} />
        <WebView
          scalesPageToFit={true}
          bounces={false}
          scrollEnabled={false}
          allowsInlineMediaPlayback={true}
          mediaPlaybackRequiresUserAction={false}
          onLoadStart={() => this.setState({ webViewOpacity: 0,backgroundColor:'#FFF' })}
          onLoadEnd={() => this.setState({ webViewOpacity: 1,backgroundColor:'#000' })}
          javaScriptEnabled={true}
          renderError={errorPage}
          onError={errorPage}
          originWhitelist={["*"]}
          // source={{ html:`<iframe src="https://cdn.plyr.io/3.5.6/plyr.js" width="100%" height="100%" frameborder="0" allow="autoplay;" allowfullscreen></iframe>` }}
          source={{ html:`<iframe src="https://player.vimeo.com/video/${videoId}?autoplay=1#t" width="100%" height="100%" frameborder="0" allow="autoplay;" allowfullscreen></iframe>` }}
          style={[fullScreen ? styles.fullScreen : inline,{ flex:0,opacity: this.state.webViewOpacity,backgroundColor:this.state.backgroundColor }]}
          // containerStyle={[fullScreen ? styles.fullScreen : inline,{ flex:0,opacity: this.state.webViewOpacity,backgroundColor:this.state.backgroundColor }]}
        />
        {/* <VimeoControls
          ref={ref => {
            this.controls = ref;
          }}
          toggleFS={() => this.toggleFS()}
          fullscreen={fullScreen}
          logo={logo}
          title={title}
          theme={setTheme}
          inlineOnly={inlineOnly}
        /> */}
        <View style={{position:'absolute',alignItems:'center',justifyContent:'center',
          top:scale(10),right:scale(15),height:scale(26),width:scale(26),borderRadius:scale(4),backgroundColor:'rgba(23,35,34,0.8)'}}>
          <TouchableOpacity style={{alignItems:'center',justifyContent:'center'}}
            onPress={() => this.toggleFS()}
          >
            {fullScreen ?
              <Image source={require('../../assets/exit-fullscreen.png')}
                style={{height:scale(20),width:scale(20)}} resizeMode="contain"/>:
              <Image source={require('../../assets/fullscreen.png')}
                style={{height:scale(20),width:scale(20)}} resizeMode="contain"/>
            }
          </TouchableOpacity>
        </View>
      </Animated.View>
    )
  }
}



Vimeo.propTypes = {
  url: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  placeholder: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  style: PropTypes.oneOfType([PropTypes.object, PropTypes.number]),
  error: PropTypes.oneOfType([PropTypes.bool, PropTypes.object]),
  loop: PropTypes.bool,
  autoPlay: PropTypes.bool,
  inlineOnly: PropTypes.bool,
  fullScreenOnly: PropTypes.bool,
  playInBackground: PropTypes.bool,
  playWhenInactive: PropTypes.bool,
  rotateToFullScreen: PropTypes.bool,
  lockPortraitOnFsExit: PropTypes.bool,
  onEnd: PropTypes.func,
  onLoad: PropTypes.func,
  onPlay: PropTypes.func,
  onError: PropTypes.func,
  onProgress: PropTypes.func,
  onMorePress: PropTypes.func,
  onFullScreen: PropTypes.func,
  onTimedMetadata: PropTypes.func,
  rate: PropTypes.number,
  volume: PropTypes.number,
  lockRatio: PropTypes.number,
  logo: PropTypes.string,
  title: PropTypes.string,
  theme: PropTypes.object,
  resizeMode: PropTypes.string
};

Vimeo.defaultProps = {
  placeholder: undefined,
  style: {},
  error: true,
  loop: false,
  autoPlay: false,
  inlineOnly: false,
  fullScreenOnly: false,
  playInBackground: false,
  playWhenInactive: false,
  rotateToFullScreen: false,
  lockPortraitOnFsExit: false,
  onEnd: () => {},
  onLoad: () => {},
  onPlay: () => {},
  onError: () => {},
  onProgress: () => {},
  onMorePress: undefined,
  onFullScreen: () => {},
  onTimedMetadata: () => {},
  rate: 1,
  volume: 1,
  lockRatio: undefined,
  logo: undefined,
  title: "",
  theme: defaultTheme,
  resizeMode: "contain"
};

export default Vimeo;
