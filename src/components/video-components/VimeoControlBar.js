import React from "react";
import PropTypes from "prop-types";
import { StyleSheet, View,TouchableOpacity } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import { ToggleIcon, Time, Scrubber } from ".";
import Icons from 'react-native-vector-icons/MaterialIcons'
import { scale } from "../../constants/Scale";
const styles = StyleSheet.create({
  container: {
    flexDirection: "row",
    height: 35,
    
  }
});

const VimeoControlBar = props => {
  const {
    onSeek,
    onSeekRelease,
    progress,
    currentTime,
    duration,
    muted,
    fullscreen,
    theme,
    inlineOnly
  } = props;

  return (
    <View
      style={{position:'absolute',alignItems:'center',justifyContent:'center',top:scale(8),right:scale(14),height:scale(23),width:scale(25),backgroundColor:'rgba(23,35,34,0.75)'}}
    >
      {/* <Time time={currentTime} theme={theme.seconds} />
      <Scrubber
        onSeek={pos => onSeek(pos)}
        onSeekRelease={pos => onSeekRelease(pos)}
        progress={progress}
        theme={{
          scrubberThumb: theme.scrubberThumb,
          scrubberBar: theme.scrubberBar
        }}
      />
      <ToggleIcon
        paddingLeft
        theme={theme.volume}
        onPress={() => props.toggleMute()}
        isOn={muted}
        iconOff="volume-up"
        iconOn="volume-mute"
        size={20}
      />
      <Time time={duration} theme={theme.duration} />
      <View style={{alignItems: 'center',backgroundColor:'transparent',justifyContent: 'center'}}>
        <TouchableOpacity
          onPress={() => props.toggleSettings()}
        >
          <Icons
            style={{paddingLeft:4,paddingRight:8}}
            name="settings"
            color="#fff"
            size={20}
          />
        </TouchableOpacity>
      </View> */}
      {!inlineOnly && (
        <ToggleIcon
          paddingRight
          onPress={() => props.toggleFS()}
          iconOff="fullscreen"
          iconOn="fullscreen-exit"
          isOn={fullscreen}
          theme={theme.fullscreen}
          size={scale(26)}
        />
      )}
    </View>
  );
};

VimeoControlBar.propTypes = {
  // toggleSettings: PropTypes.func.isRequired,
  toggleFS: PropTypes.func.isRequired,
  // toggleMute: PropTypes.func.isRequired,
  // onSeek: PropTypes.func.isRequired,
  // onSeekRelease: PropTypes.func.isRequired,
  fullscreen: PropTypes.bool.isRequired,
  // muted: PropTypes.bool.isRequired,
  inlineOnly: PropTypes.bool.isRequired,
  // progress: PropTypes.number.isRequired,
  // currentTime: PropTypes.number.isRequired,
  // duration: PropTypes.number.isRequired,
  theme: PropTypes.object.isRequired
};

export { VimeoControlBar };
