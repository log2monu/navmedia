// import React, { Component } from "react";
// import { StyleSheet, View, TouchableOpacity, Text } from "react-native";
// import VideoPlayerr from "react-native-af-video-player";
// import Icon from "react-native-vector-icons/MaterialCommunityIcons";

// let seek_time = 5;
// export default class VideoCard extends Component {
//     state = {
//         seek_val: 0,
//         paused: true,
//         key: 0,
//         currentTime: 0,
//         duration: 0
//     };
//     componentDidUnMount() {
//         this.video.pause();
//     }

//     onVideoEnd() {
//         this.video.seek(0);
//         this.setState({ key: new Date(), currentTime: 0, paused: true });
//     }

//     onVideoLoad(e) {
//         this.setState({ currentTime: e.currentTime, duration: e.duration });
//     }

//     onProgress(e) {
//         this.setState({ currentTime: e.currentTime });
//     }

//     playOrPauseVideo() {
//         this.setState({ paused: !this.state.paused });
//         if (this.state.paused) {
//             this.video.pause();
//         } else {
//             this.video.play();
//         }
//     }

//     onBackward() {
//         let newTime = Math.max(this.state.currentTime - seek_time, 0);
//         this.video.seekTo(newTime);
//         this.setState({ currentTime: newTime });
//     }

//     onForward() {
//         if (this.state.currentTime + seek_time > this.state.duration) {
//             this.video.seekTo(this.state.duration);
//             this.onVideoEnd();
//         } else {
//             let newTime = this.state.currentTime + seek_time;
//             this.video.seekTo(newTime);
//             this.setState({ currentTime: newTime });
//         }
//     }

//     render() {
//         const theme = {
//             title: "#fff",
//             more: "#fff",
//             center: "#fff",
//             fullscreen: "#fff",
//             volume: "#fff",
//             scrubberThumb: "#e52418",
//             scrubberBar: "#e52418",
//             seconds: "#fff",
//             duration: "#fff",
//             progress: "#e52418",
//             loading: "#fff"
//         };
//         return (
//             <View>
//                 <VideoPlayerr
//                     onEnd={this.onVideoEnd.bind(this)}
//                     onLoad={this.onVideoLoad.bind(this)}
//                     onProgress={this.onProgress.bind(this)}
//                     rotateToFullScreen={true}
//                     ref={ref => {
//                         this.video = ref;
//                     }}
//                     theme={theme}
//                     resizeMode="contain"
//                     playInBackground={false}
//                     url={this.props.url}
//                 />
//                 {/* <TouchableOpacity onPress={() => this.onBackward()}>
//                     <Icon name="skip-backward" color="#000" size={20} />
//                 </TouchableOpacity>
//                 <TouchableOpacity onPress={() => this.onForward()}>
//                     <Icon name="skip-forward" color="#000" size={20} />
//                 </TouchableOpacity> */}
//             </View>
//         );
//     }
// }

// const styles = StyleSheet.create({});
