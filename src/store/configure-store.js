import { createStore, applyMiddleware } from "redux";
import reducers from "../reducers";
import AsyncStorage from '@react-native-community/async-storage';
import createSagaMiddleware from "redux-saga";
import rootSaga from "../sagas";
import { createLogger } from "redux-logger";
import { persistStore } from "redux-persist";

const sagaMiddleware = createSagaMiddleware();
let middleware = applyMiddleware(sagaMiddleware);

if (__DEV__) {
  const logger = createLogger({
    level: "info",
    collapsed: true
  });
  middleware = applyMiddleware(sagaMiddleware, logger);
}

const rootReducer = (state, action) => {
  if (action.type === "RESET_APP") {
    AsyncStorage.removeItem("persist:user");
    AsyncStorage.removeItem("persist:token");
    state = undefined;
  }

  return reducers(state, action);
};

export default () => {
  const store = createStore(reducers, middleware);
  sagaMiddleware.run(rootSaga);
  const persistor = persistStore(store);
  return { store, persistor };
};
