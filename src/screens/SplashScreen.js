import React, { Component } from "react";
import { View, Image, ImageBackground } from 'react-native';
import { connect } from "react-redux";

class SplashScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  componentDidMount() {
    setTimeout(() => {
      requestAnimationFrame(() => {
          if(this.props.token) {
            this.props.navigation.navigate('BottomTab');
          } else {
            this.props.navigation.navigate('Login');
          }
      });
    }, 2500);
  }

  render() {
    console.log("Token : ", this.props.token)
    console.log("Refresh token : ", this.props.refresh_token)

    return (
      <ImageBackground 
        source={require("../assets/splash.png")} 
        resizeMode={'cover'}
        style={{flex: 1,}}>
      </ImageBackground>
    );
  }
}


const mapStateToProps = ({ token, refresh_token }) => {
  return {
    token,
    refresh_token
  };
};

const mapDispatchToProps = {
  
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SplashScreen);