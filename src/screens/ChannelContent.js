import React, { Component } from "react";
import { SafeAreaView, Image, Dimensions, ImageBackground, Text, StyleSheet, View, ScrollView, TouchableOpacity, FlatList } from 'react-native';
import HeaderComponent from "../components/HeaderComponent";
import { scale } from "../constants/Scale";
import Icon from 'react-native-vector-icons/Feather';
import { family } from "../constants/Fonts";
import { NavigationEvents } from "react-navigation";
import { getMyChannelDetails, getChannelAbout, getChannelPopular } from "../actions/ChannelsAction";
import {connect} from "react-redux";
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';

const {height, width} = Dimensions.get('window');

const lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

class ChannelContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      channelId: this.props.navigation.getParam("channel_id"),
      tabData: [
        {
          name: "Videos",
          id: 1
        },
        {
          name: "Popular Videos",
          id: 2
        },
        {
          name: "About",
          id: 3
        }
      ],
      selectedId: 1,
      isSelected: true,

    };
  }

  UNSAFE_componentWillMount() {
    this.setState({channelId: this.props.navigation.getParam("channel_id")}) ;
  }

  callApiFunctions() {
    this.props.fetchChannelContent(this.state.channelId)
    this.props.getChannelPopularVideos(this.state.channelId);
    this.props.getChannelAbout(this.state.channelId)
  }

  render() {
    const {channel_popular, channel_details, channel_content} = this.props;
    let videoData = channel_content && channel_content.data && channel_content.data.results && channel_content.data.results.length > 0 ? channel_content.data.results : [];
    let popData = channel_popular && channel_popular.data && channel_popular.data.results && channel_popular.data.results.length > 0 ? channel_popular.data.results : [];
    let aboutData = channel_details && channel_details.data && channel_details.data.category  ? channel_details.data.category : [];

    let isLoading = channel_popular.loading || channel_details.loading || channel_content.loading ? true : false;
    let isError = channel_popular.error || channel_details.error || channel_content.error ? true : false;

    console.log("Channel about :", aboutData)
    console.log("Channel pop :", popData)
    console.log("Channel vide :", videoData)

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: "#fafafa"}}>
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
        <Loader loading={isLoading} />   
        <HeaderComponent navigation={this.props.navigation} />
        <ScrollView showsVerticalScrollIndicator={false}>
        <ImageBackground style={{height: scale(120)}} source={{uri : aboutData.banner}}/>
          <TouchableOpacity
            style={{height: 100, alignItems: 'center', justifyContent: 'center', marginTop: -40}}
            onPress={()=> {}}
          >
            <Image style={{width: 90, height: 90, borderRadius: 100, borderWidth: 6, borderColor:'white'}} source={{uri: aboutData.image}}/>
          </TouchableOpacity>   
          <View style={{alignItems : 'center', marginVertical: 10}}>
            <Text style={{fontFamily: family.bold, fontSize: 14,}}>{aboutData.name}</Text>

          </View>
        <View style={{justifyContent: 'center', alignItems: 'center'}}>
          <FlatList
            data={this.state.tabData}
            keyExtractor={(item, index)=> index.toString()}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{marginHorizontal: scale(20)}}
            renderItem={({item}) => 
              <View style={{marginHorizontal: scale(6)}}>
                {this.state.isSelected && this.state.selectedId == item.id? (
                <TouchableOpacity 
                  style={{
                    borderRadius: 3, 
                    backgroundColor: 'white', 
                    // borderColor: 'grey', 
                    // borderWidth: 2,
                    elevation: 2
                  }}
                  onPress={()=> this.setState({selectedId: item.id})}
                >
                  <Text style={{color: "#0089D0", paddingHorizontal: scale(15), paddingVertical: scale(6), fontFamily: family.condensed_bold, fontSize: 14}}>{item.name}</Text>
                </TouchableOpacity>
              ): (
                <TouchableOpacity 
                  style={{borderRadius: 2, backgroundColor: 'white'}}
                  onPress={()=> this.setState({selectedId: item.id})}
                >
                  <Text style={{opacity: 0.8, paddingHorizontal: scale(15), paddingVertical: scale(6), fontFamily: family.condensed_regular}}>{item.name}</Text>
                </TouchableOpacity>
              )}
              </View>
            }
          />  
          </View>
          {/* <View style={{height: scale(0.3), width: '100%', backgroundColor: 'grey'}}/> */}
          
          {this.state.selectedId === 1 && this.renderVideos(videoData)}
          {this.state.selectedId === 2 && this.renderPopularVideos(popData)}
          {this.state.selectedId === 3 && this.renderAbout(aboutData)}

        </ScrollView>
      </SafeAreaView>
    );
  }

  renderVideos(data) {
    return(
      <View style={{}}>
            <Text
              style={{
                color: '#262625',
                fontSize: 14,
                marginVertical: 5,
                marginHorizontal: 10,
                fontFamily: family.bold
              }}>
              Videos
            </Text>

            <FlatList
              data={data}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              style={{marginBottom: '24%'}}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.id})} style={styles.featuredBox}>
                  <ImageBackground
                    resizeMode={'cover'}
                    style={{
                      height: 220,
                      width: '100%'
                      }}
                    source={{uri: item.image}}>
                    <View style={{backgroundColor: 'black', borderWidth: 0.6, borderColor: '#fafafa', alignSelf: 'flex-end', margin: 3, opacity: 0.5}}>
                      <Text style={{color: '#fafafa', fontFamily: family.regular, paddingHorizontal: 5, paddingVertical: 3, fontSize: 12}}>{item.get_length}</Text>
                    </View>
                  </ImageBackground> 
                  <View style={{flexDirection: 'row', paddingHorizontal: scale(10)}}>
                    <View>
                      <Text
                        style={{
                          marginLeft: scale(10),
                          marginTop: scale(10),
                          fontFamily: family.condensed_regular
                        }}>
                        {item.name}
                      </Text>
  
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          marginBottom: 10,
                          marginTop: 3
                        }}>
                        <View style={{flexDirection: 'row'}}>
                          <Icon name={'eye'} color={'#949494'} size={15} />
                          <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                            {item.views} views
                          </Text>
                        </View>

                        <View style={{flexDirection: 'row', marginLeft: 8}}>
                          <Icon name={'calendar'} color={'#949494'} size={15} />
                          <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                            {item.get_date}{' '}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
    )
  }

  renderPopularVideos(data) {
    return(
      <View style={{}}>
            <Text
              style={{
                color: '#262625',
                fontSize: 14,
                marginVertical: 10,
                marginLeft: 10,
                fontFamily: family.bold
              }}>
              Popular videos
            </Text>

            <FlatList
              data={data}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              style={{marginBottom: '24%'}}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.id})} style={styles.featuredBox}>
                  <ImageBackground
                    resizeMode={'cover'}
                    style={{
                      height: 220,
                      width: '100%'
                      }}
                    source={{uri: item.image}}>
                    <View style={{backgroundColor: 'black', borderWidth: 0.6, borderColor: '#fafafa', alignSelf: 'flex-end', margin: 3, opacity: 0.5}}>
                      <Text style={{color: '#fafafa', fontFamily: family.regular, paddingHorizontal: 5, paddingVertical: 3, fontSize: 12}}>{item.get_length}</Text>
                    </View>
                  </ImageBackground> 
                  <View style={{flexDirection: 'row', paddingHorizontal: scale(10)}}>
                    <View>
                      <Text
                        style={{
                          marginLeft: scale(10),
                          marginTop: scale(10),
                          fontFamily: family.condensed_regular
                        }}>
                        {item.name}
                      </Text>
  
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          marginBottom: 10,
                          marginTop: 3
                        }}>
                        <View style={{flexDirection: 'row'}}>
                          <Icon name={'eye'} color={'#949494'} size={15} />
                          <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                            {item.views} views
                          </Text>
                        </View>

                        <View style={{flexDirection: 'row', marginLeft: 8}}>
                          <Icon name={'calendar'} color={'#949494'} size={15} />
                          <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                            {item.get_date}{' '}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
    )
  }

  renderAbout(data) {
    return (
      <View style={{
        borderRadius: 4,
        // borderWidth: 0.7,
        // borderColor: 'grey',
        backgroundColor: 'white',
        marginHorizontal: scale(20),
        marginTop: scale(25),
        marginBottom: scale(20),
        elevation: 1
      }}>
        <View style={{flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 15}}>
          <Image source={{uri: data.image}} style={{width: 85, height: 85, }} />
          <View style={{marginHorizontal: 15, justifyContent: 'center'}}>
            <Text style={{ fontSize: scale(14), fontFamily: family.bold}}>{data.name}</Text>
          </View>
        </View>

        <View style={{}}>
          <Text
            style={{
              color: '#262625',
              fontSize: 14,
              marginBottom: 10,
              marginHorizontal: 15,
              fontFamily: family.bold
            }}>
            About :
          </Text>
          <Text
            style={{
              color: '#262625',
              fontSize: 12,
              marginBottom: 20,
              marginHorizontal: 15,
              fontFamily: family.condensed_light
            }}>
            {data.about}
          </Text>

          {/* <Text
            style={{
              color: '#262625',
              fontSize: 14,
              marginBottom: 10,
              marginHorizontal: 15,
              fontFamily: family.bold
            }}>
            Sub Category :
          </Text>
          <Text
            style={{
              color: '#262625',
              fontSize: 12,
              marginBottom: 20,
              marginHorizontal: 15,
              fontFamily: family.condensed_light
            }}>
            Sports
          </Text> */}
        </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  featuredBox: {
    marginVertical: 5,
    // elevation:1,
    backgroundColor: 'white',
    // marginHorizontal: 20,
    overflow: 'hidden',
    // borderRadius: 10,
  },

});

const mapStateToProps = ({ channel_content, channel_popular, channel_details }) => {
  return { channel_content, channel_popular, channel_details };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchChannelContent: (params) => {
      dispatch(getMyChannelDetails(params));
    },
    getChannelAbout: (params) => {
      dispatch(getChannelAbout(params));
    },
    getChannelPopularVideos: (params) => {
      dispatch(getChannelPopular(params));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChannelContent);