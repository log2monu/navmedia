import React, { Component } from 'react';
import { View, Text } from 'react-native';
import {LivePlayer} from "react-native-live-stream";
// import {  NodePlayerView, NodeCameraView } from 'react-native-nodemediaclient';

export default class CreateChannelScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  render() {
    const url =  "rtmp://bangalore.restream.io/live/re_1535870_0190059267368801f150";
    return (
      <View style={{flex: 1}}>
        {/* <BroadcastView
          publish="rtmp://bangalore.restream.io/live/re_1535870_0190059267368801f150"
          cameraPosition="front"
        /> */}

        <LivePlayer source={{uri: url}}
          ref={(ref) => {
              this.player = ref
          }}
          style={{flex: 0.9}}
          paused={false}
          muted={false}
          bufferTime={300}
          maxBufferTime={1000}
          resizeMode={"contain"}
          onLoading={()=>{}}
          onLoad={()=>{}}
          onEnd={()=>{}}
        />

        {/* <NodePlayerView 
          style={{ height: 200 }}
          ref={(vp) => { this.vp = vp }}
          inputUrl={"rtmp://192.168.0.10/live/stream"}
          scaleMode={"ScaleAspectFit"}
          bufferTime={300}
          maxBufferTime={1000}
          autoplay={true}
        /> */}

        {/* <NodeCameraView 
  style={{ height: 400 }}
  ref={(vb) => { this.vb = vb }}
  outputUrl = {"rtmp://192.168.0.10/live/stream"}
  camera={{ cameraId: 1, cameraFrontMirror: true }}
  audio={{ bitrate: 32000, profile: 1, samplerate: 44100 }}
  video={{ preset: 12, bitrate: 400000, profile: 1, fps: 15, videoFrontMirror: false }}
  autopreview={true}
/>

<Button
  onPress={() => {
    if (this.state.isPublish) {
      this.setState({ publishBtnTitle: 'Start Publish', isPublish: false });
      this.vb.stop();
    } else {
      this.setState({ publishBtnTitle: 'Stop Publish', isPublish: true });
      this.vb.start();
    }
  }}
  title={this.state.publishBtnTitle}
  color="#841584"
/> */}

      </View>
    );
  }
}
