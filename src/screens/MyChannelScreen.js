import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  Dimensions,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {connect} from 'react-redux';
import {NavigationEvents, ScrollView} from 'react-navigation';
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import {base_url} from '../constants/config';
import HeaderComponent from '../components/HeaderComponent';
import { scale } from '../constants/Scale';
import { family } from '../constants/Fonts';
import { getTrendingVideos } from '../actions/TrendingAction';
import { getAdBanner } from "../actions/AdActions";
import { getChannels, getMyChannels } from '../actions/ChannelsAction';

const {height, width} = Dimensions.get('window');

class MyChannelScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      
    };
  }

  callApiFunctions() {
    // this.props.getChannels();
    this.props.getMyChannels();
  }

  render() {
    let adBanner = { image: require('../assets/restaurant.jpg'), title: "sample ad.", desc: "This ad is a samle ad.This ad is a sample ad."}  
    let myChannelData = this.props.my_channels && this.props.my_channels.myChannels && this.props.my_channels.myChannels.results ? this.props.my_channels.myChannels.results : [];
    let isLoading =  this.props.my_channels.loading;
    let isError = this.props.my_channels.error;
    return (
      <View style={styles.container}>
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
        <Loader loading={isLoading} />     
        <HeaderComponent navigation={this.props.navigation} />
          <ScrollView showsVerticalScrollIndicator={false} style={{}} >
          <Text
              style={{
                color: '#262625',
                fontSize: 14,
                fontFamily: family.bold,
                marginBottom: 3,
                margin: 10,
              }}>
              My Channels
            </Text>             

          <View style={{}}>
            {myChannelData && myChannelData.length > 0 ?
            <FlatList
              data={myChannelData}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              style={{marginHorizontal: 15}}
              numColumns={2}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => this.props.navigation.navigate('MyChannelContent', {"channel_id": item.id})} style={styles.featuredBox}>
                  <Image
                    resizeMode={'cover'}
                    style={{
                      height: 100,
                      width: 100,
                      borderWidth: 10,
                      borderColor: 'white',
                      borderRadius: 100,
                      alignSelf:'center'
                    }}
                    source={{uri :item.image}}
                  />
                  <Text style={{textAlign: 'center', paddingVertical: 15, fontSize: 12, fontFamily: family.condensed_regular}}>{item.name}</Text>
                </TouchableOpacity>
              )}
            /> : null }
          </View>
          </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  featuredBox: {
    marginVertical: 5,
    marginRight: 10,
    elevation:1,
    backgroundColor: 'white',
    width: '48%',
    paddingTop: 15,
    borderRadius: 6,
    // marginHorizontal: 20,
    overflow: 'hidden',
    alignSelf: 'center'
    // borderRadius: 10,
  },

});

const mapStateToProps = ({ channels, my_channels }) => {
  return { channels, my_channels };
};

const mapDispatchToProps = dispatch => {
  return {
    // getChannels: (params) => {
    //   dispatch(getChannels(params));
    // },
    getMyChannels: (params) => {
      dispatch(getMyChannels(params));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyChannelScreen);
