import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  Dimensions,
  SafeAreaView,
  FlatList,
  ImageBackground
} from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Icon from 'react-native-vector-icons/Feather';
import {connect} from 'react-redux';
import {NavigationEvents} from 'react-navigation';
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import {base_url} from '../constants/config';
import HeaderComponent from '../components/HeaderComponent';
import {getHomeData} from '../actions/HomeAction';
import {scale} from '../constants/Scale';
import { ScrollView } from 'react-native-gesture-handler';
import { family } from '../constants/Fonts';
import { getCategories, getSubCategories, getServiceCategories } from '../actions/CategoryAction';
import { getFeaturedVideos } from "../actions/TrendingAction";

const {height, width} = Dimensions.get('window');

class CategoriesScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    };
  }

  callApiFunctions() {
    this.props.getCategories();
    this.props.fetchServiceCategories();
    this.props.getFeaturedVideos();
  }

  render() {
    const categoryData = this.props.category.categoryList && this.props.category.categoryList.results ? this.props.category.categoryList.results : [];
    const featuredData = this.props.featured.featuredList && this.props.featured.featuredList.results ? this.props.featured.featuredList.results : [];
    const serviceCatData =this.props.service_category && this.props.service_category.serviceCategoryList && this.props.service_category.serviceCategoryList.results ? this.props.service_category.serviceCategoryList.results : [];

    let isLoading = this.props.category.loading || this.props.featured.loading;
    let isError = this.props.category.error || this.props.featured.error;

    return (
      <View style={styles.container}>
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
        <Loader loading={isLoading} />   
        <HeaderComponent navigation={this.props.navigation} />
        <ScrollView showsVerticalScrollIndicator={false}>
        <View >
        <View>
            <Text style={{padding: 5,color: '#262625', fontSize: 14, fontFamily: family.bold, marginVertical: 5}}>
              Categories
            </Text>
            </View>
            <SafeAreaView style={{flex:1}}>
            {categoryData && categoryData.length > 0 ?
              <SwiperFlatList
                autoplay
                autoplayDelay={2.5}
                index={0}
                autoplayLoop
                data={categoryData}
                renderItem={({item}) => (
                  <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("CatSubCategory", {"cat_id": item.id})}
                    activeOpacity={0.5}
                    style={styles.channelsBox}>
                      <Image source={{uri: item.image}} style={{height: 60, width: 60, borderRadius: 100, borderColor: 'white', borderWidth: 6, resizeMode: 'cover'}} />
                      <Text style={{ marginTop: 3, fontFamily: family.condensed_regular, fontSize: 12, }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              /> : null}
            </SafeAreaView>  
          </View>

          <View>
            <Text style={{padding: 5,color: '#262625', fontSize: 14, fontFamily: family.bold, marginVertical: 5, marginTop: 5}}>
              Service categories
            </Text>
            </View>
            <SafeAreaView style={{flex:1}}>
            {serviceCatData && serviceCatData.length > 0 ?
              <SwiperFlatList
                autoplay
                autoplayDelay={2.5}
                index={0}
                autoplayLoop
                data={serviceCatData}
                renderItem={({item}) => (
                  <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("ServiceSubCategoryScreen", {"ser_cat_id": item.id})}
                    activeOpacity={0.5}
                    style={styles.channelsBox}>
                      <Image source={{uri: item.image}} style={{height: 60, width: 60, borderRadius: 100, borderColor: 'white', borderWidth: 6, resizeMode: 'cover'}} />
                      <Text style={{ marginTop: 3, fontFamily: family.condensed_regular, fontSize: 12, }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              /> : null}
            </SafeAreaView>  

          
        
          <View >
            <Text style={{color: '#262625', fontSize: 14, fontFamily: family.bold , marginVertical: 10, marginLeft: 5}}>
              Featured Videos
            </Text>
            <SafeAreaView style={{}}>
            {featuredData && featuredData.length >0 ?
              <FlatList
                data={featuredData}
                keyExtractor={(item, index)=> index.toString()}
                showsVerticalScrollIndicator={false}
                style={{marginLeft: 0}}
                renderItem={({item})=> 
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.id})}
                    style={styles.featuredBox}>
                      <ImageBackground
                        resizeMode={'cover'}
                        style={{
                          height: 220,
                          width: '100%'
                          }}
                        source={{uri: item.image}}>
                        <View style={{backgroundColor: 'black', borderWidth: 0.6, borderColor: '#fafafa', alignSelf: 'flex-end', margin: 3, opacity: 0.5}}>
                          <Text style={{color: '#fafafa', fontFamily: family.regular, paddingHorizontal: 5, paddingVertical: 3, fontSize: 12}}>{item.get_length}</Text>
                        </View>
                      </ImageBackground>  
                      <View style={{flexDirection: 'row'}}>
                        <Image style={{width: 50, height: 50, borderRadius: 100, marginLeft: 10, alignSelf: 'center', borderWidth: 1, borderColor: 'grey'}} source={{uri: item.channel.image}}/>
                        <View>
                          <Text style={{marginLeft: 10, marginTop: 10, fontFamily: family.regular}}>{item.name}</Text>
                          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
                            <Text style={{opacity: 0.7, marginVertical: 5, fontFamily: family.condensed_regular}}>
                              {item.channel.name}
                            </Text>
                            <Icon
                              name={'check-circle'}
                              size={12}
                              color={'grey'}
                              style={{marginHorizontal: 6}}
                            />
                          </View>
                          <View style={{flexDirection: 'row', marginLeft: 10, marginBottom: 10}}>
                            <View style={{flexDirection: 'row'}}>
                              <Icon name={'eye'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.views} views
                              </Text>
                            </View>

                            <View style={{flexDirection: 'row', marginLeft: 8}}>
                              <Icon name={'calendar'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.get_date}{' '}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>  
                      
                    </TouchableOpacity>
                }
              /> : null}
            </SafeAreaView>  
          </View>  
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  channelsBox: {
    marginHorizontal: 4,
    borderRadius: 50,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  subscribeButton: {
    borderRadius: 1,
    borderColor: '#349beb',
    borderWidth: 1,
    marginVertical: scale(8),
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1
  },
});

const mapStateToProps = ({ category, featured, sub_category, service_category }) => {
  return { category, featured, sub_category, service_category };
};

const mapDispatchToProps = dispatch => {
  return {
    getCategories: (params) => {
      dispatch(getCategories(params));
    },
    fetchServiceCategories: (params) => {
      dispatch(getServiceCategories(params));
    },
    getSubCategories: (params) => {
      dispatch(getSubCategories(params));
    },
    fetchSubCatContent: (params) => {
      dispatch(fetchSubCatContent(params));
    },
    getFeaturedVideos: (params) => {
      dispatch(getFeaturedVideos(params));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CategoriesScreen);

