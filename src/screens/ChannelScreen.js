import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  Dimensions,
  FlatList,
  SafeAreaView,
  ImageBackground
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {connect} from 'react-redux';
import {NavigationEvents} from 'react-navigation';
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import {base_url} from '../constants/config';
import HeaderComponent from '../components/HeaderComponent';
import {getHomeData} from '../actions/HomeAction';
import {scale} from '../constants/Scale';
import { ScrollView } from 'react-native-gesture-handler';
import { family } from '../constants/Fonts';
import { getChannels, putSubscribe } from "../actions/ChannelsAction";
import { getFeaturedVideos } from "../actions/TrendingAction";

const {height, width} = Dimensions.get('window');

class ChannelScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  callApiFunctions() {
    this.props.getChannels();
    this.props.getFeaturedVideos();
  }

  onSubscribe = (channelId) => {
    this.props.putSubscribe(channelId)
    this.props.getChannels();
  }

  render() {
    const channelData = this.props.channels.channelsList && this.props.channels.channelsList.results ? this.props.channels.channelsList.results : [];
    const featuredData = this.props.featured.featuredList && this.props.featured.featuredList.results ? this.props.featured.featuredList.results : [];

    let isLoading = this.props.channels.loading || this.props.featured.loading;
    let isError = this.props.channels.error || this.props.featured.error;
    return (
      <View style={styles.container}>
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
        <Loader loading={isLoading} />     
        <HeaderComponent navigation={this.props.navigation} />
        <ScrollView showsVerticalScrollIndicator={false}>
        <View style={{}}>
            <View>
              <Text
                style={{
                  color: '#262625',
                  fontSize: scale(14),
                  fontFamily: family.bold,
                  marginVertical: scale(10),
                  marginHorizontal: scale(5),
                }}>
                Channels
              </Text>
            </View>
            {channelData && channelData.length > 0 ?
              <FlatList
                horizontal
                data={channelData}
                keyExtractor={(item, index) => index.toString()}
                showsHorizontalScrollIndicator={false}
                style={{paddingLeft: 10}}
                renderItem={({item}) => (
                  <View 
                  style={{
                    backgroundColor: 'white', 
                    borderRadius: 6, padding: 10, 
                    alignItems: 'center', 
                    justifyContent: 'space-between',
                    // flexDirection: 'row',
                    marginHorizontal: scale(5),
                    marginVertical: scale(3),
                    paddingVertical: scale(12),
                    elevation: 1
                    }}>
                  <TouchableOpacity onPress={()=> this.props.navigation.navigate("ChannelContent",{"channel_id": item.id})} style={{ alignItems: 'center'}}>
                    <Image
                      source={{uri: item.image}}
                      style={{height: 70, width: 70, borderRadius: 100, borderWidth: 5, borderColor: 'white'}}
                    />
                    <Text style={{ marginHorizontal: scale(8), fontFamily: family.condensed_regular, marginVertical: 6, width: 70, textAlign: 'center', fontSize: 12}}>{item.name}</Text>
                  </TouchableOpacity>  
                  { item.is_sub == true? 
                    (<TouchableOpacity
                      onPress={() => this.onSubscribe(item.id)}
                      style={{
                        borderRadius: 5,
                        borderWidth: 0.5,                   
                        marginVertical: scale(10),
                        alignItems: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        elevation: 1
                      }}>
                      <Text style={{opacity: 0.8, paddingVertical: scale(3), paddingHorizontal: scale(14),fontSize: scale(12), fontFamily: family.condensed_regular }}>Subscribed</Text>
                    </TouchableOpacity> 
                    ) :
                    (
                      <TouchableOpacity
                        onPress={() => this.onSubscribe(item.id)}
                        style={{
                          borderRadius: 4,
                          borderColor: 'grey',
                          borderWidth: 0.4,
                          marginVertical: scale(8),
                          alignItems: 'center',
                          justifyContent: 'center',
                          backgroundColor: '#0089D0',
                          elevation: 1
                        }}>
                        <Text style={{color: 'white', fontSize: scale(12), paddingHorizontal: scale(14), paddingVertical: scale(3), fontFamily: family.condensed_regular }}>Subscribe</Text>
                      </TouchableOpacity> 
                    )}            
                </View>
                )}
              />: null
            } 
          </View>
          <View >
            <Text style={{color: '#262625', fontSize: 14, fontFamily: family.bold , marginVertical: 15, marginLeft: 5}}>
              Featured Videos
            </Text>
            <SafeAreaView style={{}}>
            {featuredData && featuredData.length >0 ?
              <FlatList
                data={featuredData}
                keyExtractor={(item, index)=> index.toString()}
                showsVerticalScrollIndicator={false}
                style={{marginLeft: 0}}
                renderItem={({item})=> 
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.id})}
                    style={styles.featuredBox}>
                      <ImageBackground
                        resizeMode={'cover'}
                        style={{
                          height: 220,
                          width: '100%'
                          }}
                        source={{uri: item.image}}>
                        <View style={{backgroundColor: 'black', borderWidth: 0.6, borderColor: '#fafafa', alignSelf: 'flex-end', margin: 3, opacity: 0.5}}>
                          <Text style={{color: '#fafafa', fontFamily: family.regular, paddingHorizontal: 5, paddingVertical: 3, fontSize: 12}}>{item.get_length}</Text>
                        </View>
                      </ImageBackground>  
                      <View style={{flexDirection: 'row'}}>
                        <Image style={{width: 50, height: 50, borderRadius: 100, marginLeft: 10, alignSelf: 'center', borderWidth: 1, borderColor: 'grey'}} source={{uri: item.channel.image}}/>
                        <View>
                          <Text style={{marginLeft: 10, marginTop: 10, fontFamily: family.regular}}>{item.name}</Text>
                          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
                            <Text style={{opacity: 0.7, marginVertical: 5, fontFamily: family.condensed_regular}}>
                              {item.channel.name}
                            </Text>
                            <Icon
                              name={'check-circle'}
                              size={12}
                              color={'grey'}
                              style={{marginHorizontal: 6}}
                            />
                          </View>
                          <View style={{flexDirection: 'row', marginLeft: 10, marginBottom: 10}}>
                            <View style={{flexDirection: 'row'}}>
                              <Icon name={'eye'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.views} views
                              </Text>
                            </View>

                            <View style={{flexDirection: 'row', marginLeft: 8}}>
                              <Icon name={'calendar'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.get_date}{' '}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>  
                      
                    </TouchableOpacity>
                }
              /> : null}
            </SafeAreaView>  
          </View>
        </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  channelsBox: {
    marginHorizontal: 15,
    marginVertical: scale(3),
    elevation: 1,
    backgroundColor: 'white',
    borderRadius: 15,
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  // subscribeButton: {
  //   borderRadius: 1,
  //   borderColor: this.state.,
  //   borderWidth: 1,
  //   marginVertical: scale(8),
  //   alignItems: 'center',
  //   justifyContent: 'center',
  //   elevation: 1
  // },
});

const mapStateToProps = ({ channels, featured, subscribe }) => {
  return { channels, featured, subscribe };
};

const mapDispatchToProps = dispatch => {
  return {
    getChannels: (params) => {
      dispatch(getChannels(params));
    },
    getFeaturedVideos: (params) => {
      dispatch(getFeaturedVideos(params));
    },
    putSubscribe: (params) => {
      dispatch(putSubscribe(params));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ChannelScreen);
