import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  Dimensions,
  FlatList,
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import {connect} from 'react-redux';
import {NavigationEvents, ScrollView} from 'react-navigation';
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import {base_url} from '../constants/config';
import HeaderComponent from '../components/HeaderComponent';
import { scale } from '../constants/Scale';
import { family } from '../constants/Fonts';
import { getTrendingVideos } from '../actions/TrendingAction';
import { getAdBanner } from "../actions/AdActions";
import { getHistory, deleteSingleHistory, deleteHistory } from '../actions/HistoryAction';

const {height, width} = Dimensions.get('window');

class HistoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {

    };
  }

  callApiFunctions() {
    this.props.getHistory();
  }

  render() {
    const historyData = this.props.history && this.props.history.data && this.props.history.data.results ? this.props.history.data.results : [];
    console.log("History : ", historyData);

    let isLoading = this.props.history.loading || this.props.del_single_history.loading || this.props.del_history.loading;
    let isError = this.props.history.error || this.props.del_single_history.error || this.props.del_history.error;
    return (
      <View style={styles.container}>
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
        <Loader loading={isLoading} />     
        <HeaderComponent navigation={this.props.navigation} />
          <ScrollView showsVerticalScrollIndicator={false} style={{}} >
          <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
            <Text
              style={{
                color: '#262625',
                fontSize: 14,
                fontFamily: family.bold,
                marginBottom: 3,
                margin: 10,
              }}>
              Watch History
            </Text>

            {historyData && historyData.length > 0 ?
            <TouchableOpacity onPress={()=> {
              this.props.deleteHistory()
              this.props.getHistory()
            }}>
            <Text
              style={{
                color: '#0089D0',
                fontSize: 13,
                fontFamily: family.condensed_bold,
                marginBottom: 3,
                margin: 10,
              }}>
              Clear All
            </Text>
            </TouchableOpacity>: null}
          </View>
   

          <View style={{}}>
            {historyData && historyData.length > 0 ?
            <FlatList
              data={historyData}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              style={{marginTop: 8}}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.video.id})} style={styles.featuredBox}>
                  <Image
                    resizeMode={'cover'}
                    style={{
                      height: 220,
                      width: '100%'
                    }}
                    source={{uri: item.video.image}}
                  />
                  <View style={{flexDirection: 'row', paddingHorizontal: scale(0), justifyContent: 'space-between'}}>
                    <View style={{flexDirection: 'row'}}>
                      <Image style={{width: 50, height: 50, borderRadius: 100, marginLeft: 10, alignSelf: 'center', borderWidth: 1, borderColor: 'grey'}} source={{uri: item.video.channel.image}}/>
                      <View>
                        <Text
                          style={{
                            marginLeft: scale(10),
                            marginTop: scale(10),
                            fontFamily: family.condensed_regular
                          }}>
                          {item.video.name}
                        </Text>
                        <View
                          style={{
                            flexDirection: 'row',
                            alignItems: 'center',
                            marginLeft: scale(10),
                          }}>
                          <Text
                            style={{
                              opacity: 0.8,
                              marginVertical: 2,
                              fontFamily: family.condensed_regular
                            }}>
                            {item.video.channel.name}
                          </Text>
                          <Icon
                            name={'check-circle'}
                            size={12}
                            color={'grey'}
                            style={{marginHorizontal: 6}}
                          />
                        </View>
                        <View
                          style={{
                            flexDirection: 'row',
                            marginLeft: 10,
                            marginBottom: 10,
                            marginTop: 0
                          }}>
                          <View style={{flexDirection: 'row', }}>
                            <Icon name={'eye'} color={'#949494'} size={15} />
                            <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                              {item.video.views} views
                            </Text>
                          </View>

                          <View style={{flexDirection: 'row', marginLeft: 8}}>
                            <Icon name={'calendar'} color={'#949494'} size={15} />
                            <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                              {item.watch_time}{' '}
                            </Text>
                          </View>
                        </View>
                      </View>
                    </View>
                    
                    <TouchableOpacity style={{alignSelf: 'center'}} onPress={()=> {
                      this.props.deleteSingleHistory(item.id)
                      this.props.getHistory()
                    }}>
                      <Icon3 name={"delete"} color={"#0089D0"} size={20} style={{padding: 10}}/>
                    </TouchableOpacity>
                  </View>
                </TouchableOpacity>
              )}
            /> : null }
          </View>
          </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  featuredBox: {
    marginVertical: 0,
    // elevation:1,
    backgroundColor: 'white',
    // marginHorizontal: 20,
    overflow: 'hidden',
    // borderRadius: 10,
  },

});

const mapStateToProps = ({ history, del_single_history, del_history }) => {
  return { history, del_single_history, del_history };
};

const mapDispatchToProps = dispatch => {
  return {
    getHistory: (params) => {
      dispatch(getHistory(params));
    },
    deleteSingleHistory: (params) => {
      dispatch(deleteSingleHistory(params));
    },
    deleteHistory: (params) => {
      dispatch(deleteHistory(params));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HistoryScreen);
