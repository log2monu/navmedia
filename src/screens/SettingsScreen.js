import React, {Component} from 'react';
import {
  SafeAreaView,
  Text,
  View,
  FlatList,
  TouchableOpacity,
  ScrollView,
  Alert
} from 'react-native';
import HeaderComponent from '../components/HeaderComponent';
import {scale} from '../constants/Scale';
import Icon from 'react-native-vector-icons/Feather';
import Icon1 from 'react-native-vector-icons//MaterialCommunityIcons';
import Icon2 from 'react-native-vector-icons//MaterialIcons';
import { TextInput } from 'react-native-gesture-handler';
import { family } from '../constants/Fonts';
import { deleteProfile, putUpdateProfile } from '../actions/ProfileAction';
import {connect} from 'react-redux';
import {NavigationEvents} from 'react-navigation';
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import { postFeedback } from '../actions/SettingsAction';
import Toast from "react-native-simple-toast";

 class SettingsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabData: [
        {
          name: 'Premium',
          id: 1,
        },
        {
          name: 'Help',
          id: 2,
        },
        {
          name: 'Feedback',
          id: 3,
        },

      ],
      selectedId: 1,
      isSelected: true,
      isPremium: null,
      helpsData: [
        {
          question: "cytesce csj v svrhj ?",
          answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
        },
        {
          question: "cytesce csj v  ?",
          answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
        },
        {
          question: "cytesce v svrhj ?",
          answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
        },
        {
          question: "cytesce v svrhj ?",
          answer: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat."
        },
      ],
      isAnswerOpen: false,
      subject: "",
      description: ""
    };
    this.subjectText = React.createRef();
    this.aboutText = React.createRef();
  }

  UNSAFE_componentWillMount() {
    this.setState({ isPremium: this.props.premium })
  }

  componentWillUpdate(prevProps) {
    if (this.props.update_profile) {
      if (this.props.update_profile.success &&
          !this.props.update_profile.loading &&
          !this.props.update_profile.error) {
            
            if(prevProps.update_profile != this.props.update_profile){
              // this.setState({ isUpdateProfileModalVisible: false});
              // Toast.showWithGravity('Profile updated!', Toast.LONG, Toast.CENTER)
              if (this.props.update_profile.userData != null){
                this.setState({isPremium: this.props.update_profile.userData.is_premium})
              } 
            }     
      }
    }

    if (this.props.feedback) {
      if (this.props.feedback.success &&
          !this.props.feedback.loading &&
          !this.props.feedback.error) {    
          if(prevProps.feedback != this.props.feedback){
            this.subjectText.current.clear();
            this.aboutText.current.clear();
            Toast.showWithGravity('Feedback posted!', Toast.LONG, Toast.CENTER)    
          }     
      }
    }
  }

  render() {
    let isLoading = this.props.update_profile.loading || this.props.feedback.loading ? true : false;
    console.log("Pree :", this.state.isPremium)
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fafafa'}}>
        <Loader loading={isLoading} /> 
        <HeaderComponent navigation={this.props.navigation} />
        <Text
          style={{
            fontSize: 14,
            marginHorizontal: 20,
            marginTop: 25,
            marginBottom: 10,
            fontFamily: family.bold
          }}>
          Settings
        </Text>
        <View
          style={{
            justifyContent: 'center',
            alignItems: 'center',
            marginVertical: 10,
          }}>
          <FlatList
            data={this.state.tabData}
            keyExtractor={(item, index) => index.toString()}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{marginHorizontal: scale(16)}}
            renderItem={({item}) => (
              <View style={{}}>
                {this.state.isSelected && this.state.selectedId == item.id ? (
                  <TouchableOpacity
                    style={{
                      borderRadius: 3,
                      backgroundColor: 'white',
                      borderColor: 'grey',
                    }}
                    onPress={() => this.setState({selectedId: item.id})}>
                    <Text
                      style={{
                        color: '#0089D0',
                        paddingHorizontal: scale(15),
                        paddingVertical: scale(6),
                        fontFamily: family.condensed_bold
                      }}>
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                ) : (
                  <TouchableOpacity
                    style={{borderTopEndRadius: 2}}
                    onPress={() => this.setState({selectedId: item.id})}>
                    <Text
                      style={{
                        opacity: 0.8,
                        paddingHorizontal: scale(15),
                        paddingVertical: scale(6),
                        fontFamily: family.condensed_regular
                      }}>
                      {item.name}
                    </Text>
                  </TouchableOpacity>
                )}
              </View>
            )}
          />
        </View>
        {this.state.selectedId === 1 && this.renderPremium()}
        {this.state.selectedId === 2 && this.renderHelp()}
        {this.state.selectedId === 3 && this.renderFeedback()}
        {/* {this.state.selectedId === 4 && this.renderContact()} */}
      </SafeAreaView>
    );
  }

  renderPremium() {
    const { update_profile } = this.props;
    let isPremium = update_profile && update_profile.userData &&  update_profile.userData.is_premium ? update_profile.userData.is_premium : false;
    return (
      <ScrollView style={{marginTop: 100}} showsVerticalScrollIndicator={false}>
        <View style={{alignItems: 'center', marginVertical: 25}}>
          <Text style={{fontFamily: family.bold, fontSize: 17, marginBottom: 12}}>
            Nav Media Premium
          </Text>
          <Text style={{fontFamily: family.medium, fontSize: 12, color: 'grey'}}>
            Videos and musics ad-free
          </Text>
          {this.state.isPremium ? (
            <TouchableOpacity
              style={{
                borderRadius: 3,
                backgroundColor: 'white',
                borderColor: 'grey',
                borderWidth: 0,
                elevation: 2,
                marginVertical: 20
              }}
              onPress={() => this.updateProfile("False")}>
              <Text
                style={{
                  opacity: 0.8,
                  paddingHorizontal: scale(15),
                  paddingVertical: scale(6),
                  fontFamily: family.condensed_bold
                }}>
                End Membership
              </Text>
            </TouchableOpacity>
          ) : (
            <TouchableOpacity
              style={{
                borderRadius: 3,
                backgroundColor: 'white',
                borderColor: 'grey',
                elevation: 2,
                marginVertical: 20
              }}
              onPress={() => this.updateProfile("True")}>
              <Text
                style={{
                  fontFamily: family.condensed_bold,                  
                  paddingHorizontal: scale(15),
                  paddingVertical: scale(6),
                }}>
                Go Premium
              </Text>
            </TouchableOpacity>
          )}
          {this.state.isPremium ? (
            <View style={{alignItems: 'center'}}>
              <Text style={{color: 'grey', marginBottom: 2 , fontFamily: family.regular}}>You will no longer be able to enjoy</Text>
              <Text style={{color: 'grey', fontFamily: family.regular}}>Premium services.</Text>
            </View>
          ): null}

          <TouchableOpacity
            onPress={()=> this.props.navigation.navigate("TermsScreen")}
            style={{}}
          >
            <Text style={{opacity: 0.8, fontFamily: family.condensed_bold, fontSize: 12, color: '#0089D0'}}>Terms & Conditions.</Text>
          </TouchableOpacity>  
        </View>

        {/* <View style={{backgroundColor: 'white', elevation: 2,borderRadius: 4, marginHorizontal: 20, marginVertical: 20}}>
          <View style={{flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 15}}>
            <Icon name={'video'} size={60} color={'#0089D0'} />
            <Icon name={'music'} size={60} color={'#0089D0'} />
            <Icon1 name={'movie-roll'} size={60} color={'#0089D0'} />
            <Icon2 name={'local-movies'} size={60} color={'#0089D0'} />
          </View>
          <View style={{marginHorizontal: 20, marginVertical: 20}}>
            <Text style={{fontSize: 18, color: 'black', fontFamily: family.medium}}>Membership</Text>
            <Text style={{fontSize: 14, color: 'grey', marginTop: 5, fontFamily: family.regular}}>Premium</Text>
            <View style={{flexDirection: 'row', marginVertical: 15}}>
              <Text style={{fontSize: 15, color: 'black', fontWeight: 'bold', fontFamily: family.medium}}>Renewal  :  </Text>
              <Text style={{fontSize: 15, color: 'grey', fontFamily: family.regular}}> next renewal</Text>
            </View>
          </View>
        
        </View> */}

        <View 
          style={{
          borderRadius: 4,
          // borderWidth: 0.7,
          // borderColor: 'grey',
          backgroundColor: 'white',
          marginHorizontal: scale(20),
          marginTop: scale(10),
          marginBottom: scale(10),
          elevation: 1
        }}>
          <TouchableOpacity onPress ={()=> this.deleteAccount()}>
            <Text style={{textAlign: 'center', color: 'red', fontFamily: family.bold, paddingVertical: 8}}>Delete account</Text>
          </TouchableOpacity>
        </View>
      </ScrollView>
    );
  }

  renderHelp() {
    const lorem = "Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. 3 wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt laborum eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee nulla assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable."
    return (
      <ScrollView style={{}} showsVerticalScrollIndicator={false}>
        <View style={{}}>
          <Text
            style={{
              fontFamily: family.medium,
              fontSize: 16,
              marginHorizontal: 20,
              marginVertical: 10
            }}>
            Helps & FAQ
          </Text>
          <FlatList
            data={this.state.helpsData}
            keyExtractor={(item, index)=> index.toString()}
            showsVerticalScrollIndicator={false}
            renderItem={({item})=>
              <View style={{ marginVertical: 4, marginHorizontal: 20, }}>
                <TouchableOpacity 
                  onPress={()=> this.setState({isAnswerOpen: !this.state.isAnswerOpen})}
                  style={{flexDirection: 'row', backgroundColor: '#fafafa', justifyContent: 'space-between', paddingHorizontal: 8, paddingVertical: 10, elevation: 1}}>
                  <Text style={{color: '#0089D0', fontSize: 15, fontFamily: family.condensed_regular}}>{item.question}</Text>
                  <View >
                    <Icon name={this.state.isAnswerOpen ? 'chevron-up' : 'chevron-down'} color={"#0089D0"} size={25} />
                  </View> 
                </TouchableOpacity>
                {this.state.isAnswerOpen ? (
                  <View style={{paddingHorizontal: 25, backgroundColor: 'white', elevation: 1}}>
                    <View style={{height: 0.4, backgroundColor: '#grey', marginTop: 5}}/>
                    <Text style={{color: 'grey', paddingVertical: 15, marginBottom: 10, fontFamily: family.condensed_regular}}>{lorem}</Text>
                  </View>
                ): null}
              </View>  
            }
          />  
        </View>
      </ScrollView>  
    );
  }

  renderFeedback() {
    return (
      <View style={{}} >
        <View style={{marginHorizontal: 25, marginVertical: 10}}>
          <Text
            style={{
              fontFamily: family.medium,              
              fontSize: 16,
            }}>
              Subject
          </Text>
          <View style={{backgroundColor: '#fafafa', borderRadius: 2, borderColor: '#b0aeae', borderWidth: 0.8, marginTop: 10, marginBottom: 15}}>
            <TextInput
              ref={this.subjectText}
              placeholder={"Subject"}
              placeholderTextColor={"grey"}
              onChangeText={(value)=> this.setState({ subject: value })}
              style={{paddingHorizontal: 20, marginVertical: 0, fontFamily: family.regular}}
            />  
          </View>

          <Text
            style={{
              fontFamily: family.medium,
              fontSize: 16,
            }}>
              About
          </Text>
          <View style={{backgroundColor: '#fafafa', borderRadius: 2, borderColor: '#b0aeae', borderWidth: 0.8, marginTop: 10, marginBottom: 0, height: '35%'}}>
            <TextInput
              ref={this.aboutText}
              placeholder={"Description"}
              placeholderTextColor={"grey"}
              onChangeText={(value)=> this.setState({ description: value })}
              style={{paddingHorizontal: 20, marginVertical: 0, fontFamily: family.regular}}
            />  
          </View>

          <View style={{ justifyContent: 'space-between', marginVertical: 15}}>
            <TouchableOpacity 
              style={{backgroundColor: '#fafafa', borderRadius: 4, elevation: 2, width: 60, height: 40, justifyContent: 'center'}}
              onPress={()=> this.sendFeedBack()}
            >
              <Icon name={'upload'} color={"#0089D0"} size={20} style={{alignSelf: 'center'}}/>
            </TouchableOpacity>  

          </View>
        </View>
      </View> 
    )
  }

  updateProfile =(status) => {
    const { update_profile } = this.props;
    let isPremium = update_profile && update_profile.userData &&  update_profile.userData.is_premium ? update_profile.userData.is_premium : false;
    const data ={
      is_premium: status
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }
    
    this.props.putUpdateProfile(formBody)
    // if (this.props.update_profile) {
    //   if (this.props.update_profile.success) {
    //     this.setState({ isPremium: !this.state.isPremium})
    //   }
    // }
  }

  sendFeedBack =() => {
    const  { subject, description } = this.state;
    const data ={
      subject: subject,
      message: description
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }
    
    this.props.postFeedback(formBody)

  }

  deleteAccount= () => {
    Alert.alert(
      "Delete Account",
      "Do you want to delete this account ?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => {
          this.props.deleteProfile();
          if (this.props.user_info === null) {
            this.props.navigation.navigate("LoginScreen")
          }
        } }
      ],
      { cancelable: true }
    );
  }
}

const mapStateToProps = ({  delete_user, feedback, update_profile, premium }) => {
  return {  delete_user, feedback, update_profile, premium };
};

const mapDispatchToProps = dispatch => {
  return {
    putUpdateProfile: (params) => {
      dispatch(putUpdateProfile(params));
    },
    deleteProfile: (params) => {
      dispatch(deleteProfile(params));
    },
    postFeedback: (params) => {
      dispatch(postFeedback(params));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SettingsScreen);