import React, { Component, Fragment } from "react";
import { SafeAreaView, TouchableOpacity, ScrollView, View, Text, StatusBar, Button, Image,} from 'react-native';
import { GoogleSignin, GoogleSigninButton, statusCodes } from '@react-native-community/google-signin';
import { firebase } from '@react-native-firebase/auth';
import { LoginManager , LoginButton, AccessToken} from 'react-native-fbsdk';
import { scale } from "../constants/Scale";
import {connect} from 'react-redux';
import { postLogin } from "../actions/LoginAction";
import Loader from "../components/Loader";
import {facebookService} from '../components/FacebookService';

class LoginScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pushData: [],
      loggedIn: false,
      grant_type: "password",
      client_id: "xsBYPRPiqUAH6JT0jPeOAwjoMDAl1IR74P9XSmTz",
      client_secret: "g8Ymi2bfqMu57MqedipQWiUFzNb6YAS0qnLFrZ5nlvfavFYIlWcNHQ43yxXqI8GBaWJcT5MQnP7cgLBQabihj3weR3z5KMILcSB48FD7X1q7HD8zJiqKjj1cX2jfgwtX",
      username: "dreak",
      password: "misha",
      isSigninInProgress:  false,
      googleToken: ""
      
      // backend: "facebook",
      // token: "EAAJEZAtAwNr0BAKcoQDaZCfiQbjZB9M0Rh0Cxr6IiCZCMSPGRdPCQFwbBtt1YLvHMhPk5bc55TaHXua7lxHFpWCB0syEjMjvLSQolYWrTQeB2inSeVa3ROTua4Q4TGvfwN85WUknkdlt2NYZBzTIGCfFJ51L6VNcIMp6WxKrr9wZDZD",
    };
  }

  componentDidMount () {
    GoogleSignin.configure({
      // scopes: ['profile'],
      // scopes: ['https://www.googleapis.com/auth/drive.readonly'], // what API you want to access on behalf of the user, default is email and profile
      webClientId: '1031184950563-2hcp4k6ilqpvqassvg89gdgf4q340q7u.apps.googleusercontent.com', // client ID of type WEB for your server (needed to verify user ID and offline access)
      offlineAccess: true, // if you want to access Google API on behalf of the user FROM YOUR SERVER
      //hostedDomain: '', // specifies a hosted domain restriction
      //loginHint: '', // [iOS] The user's ID, or email address, to be prefilled in the authentication UI if possible. [See docs here](https://developers.google.com/identity/sign-in/ios/api/interface_g_i_d_sign_in.html#a0a68c7504c31ab0b728432565f6e33fd)
      forceConsentPrompt: true, // [Android] if you want to show the authorization prompt at each login.
      //accountName: '', // [Android] specifies an account name on the device that should be used
      //iosClientId: '<FROM DEVELOPER CONSOLE>', // [iOS] optional, if you want to specify the client ID of type iOS (otherwise, it is taken from GoogleService-Info.plist)
    });
  }

  componentDidUpdate(){
    if(this.props.login){
      if(this.props.login.success){
        this.props.navigation.navigate("BottomTab");
      }
    }
  }

  _signIn = async() => {
    try {
      await GoogleSignin.hasPlayServices();
      const userInfo = await GoogleSignin.signIn();
      const userToken = await GoogleSignin.getTokens();
      console.warn("Infoooo :" , userInfo )
      this.setState({ googleToken: userInfo.accessToken });
      const { idToken } = await GoogleSignin.signIn();
      const googleAccessToken = userToken.accessToken;
      console.warn("dsg :" , userInfo)

            

      const credential = firebase.auth.GoogleAuthProvider.credential(idToken, googleAccessToken);

      // console.warn("dsg :" , accessToken+"   "+credential)

      firebase.auth().signInWithCredential(credential);

      this.onLoginGoogle(googleAccessToken, userInfo);
    } catch (error) {
      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (e.g. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // some other error happened
      }
      console.warn("Errrorr :", error)
    }
  }

  render() {
    // console.log("LOGGGin :", this.props.login);
    const { loading, error } = this.props.login;
    return (
      <SafeAreaView style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <Loader loading={loading} />     
          <Image source={require("../assets/images/logo.png")}  style={{width: 150, height: 130, marginBottom: 100}} resizeMode={'contain'}/>

          <GoogleSigninButton
            style={{ width: 192, height: 50 }}
            size={GoogleSigninButton.Size.Wide}
            color={GoogleSigninButton.Color.Dark}
            onPress={()=>this._signIn()}
            disabled={this.state.isSigninInProgress}
          />

          {/* {facebookService.makeLoginButton((accessToken) => {
                    console.log("Accccess tokk fb :", accessToken)
                    this.login()
                  })} */}

          {/* <LoginButton
            readPermissions={["public_profile"]}
            onLoginFinished={(error, result) => {
              if (error) {

              } else if (result.isCancelled) {

              } else {
                AccessToken.getCurrentAccessToken()
                  .then((data) => {
                    callback(data.accessToken)
                  })
                  .catch(error => {
                    console.log(error)
                  })
              }
            }} /> */}

          {/* <LoginButton
          onLoginFinished={
            (error, result) => {
              
              if (error) {
                LoginManager.logInWithPermissions(["public_profile"]).then(
                function(result) {
                  if (result.isCancelled) {
                    console.log("Login cancelled");
                  } else {
                    console.log(
                      "Login success with permissions: " +
                        result.grantedPermissions.toString()
                    );
                  }
                },
                function(error) {
                  console.log("Login fail with error: " + error);
                }
              );
                console.log("login has error: ", result.error);
              } else if (result.isCancelled) {
                console.log("login is cancelled.");
              } else {
                AccessToken.getCurrentAccessToken().then(
                  (data) => {
                    console.warn("tokkk : ", data.accessToken.toString())
                  }
                )
              }
            }
          }
          onLogoutFinished={() => console.log("logout.")}/> */}

          {/* <TouchableOpacity 
            style={{borderRadius: scale(4), backgroundColor: '#349beb', marginTop: scale(8), elevation: 2}}
            onPress={()=> this.onLogin()}
          >
            <Text style={{fontWeight: 'bold', fontSize: scale(15), color: 'white', paddingHorizontal: 80, paddingVertical: 10}}>SKIP</Text>
          </TouchableOpacity>                     */}
      </SafeAreaView>
    );
  }

  onLoginGoogle=(accessToken, userInfo)=> {
    const data ={
      grant_type: "convert_token",
      client_id: "xsBYPRPiqUAH6JT0jPeOAwjoMDAl1IR74P9XSmTz",
      client_secret: "g8Ymi2bfqMu57MqedipQWiUFzNb6YAS0qnLFrZ5nlvfavFYIlWcNHQ43yxXqI8GBaWJcT5MQnP7cgLBQabihj3weR3z5KMILcSB48FD7X1q7HD8zJiqKjj1cX2jfgwtX",
      backend: "google-oauth2",
      token: accessToken
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }
    let formDataa = {formBody, userInfo} 
    this.props.postLogin(formDataa)

    if(this.props.login){
      if(this.props.login.success){
        this.props.navigation.navigate("BottomTab");
      }
    }
  }

  onLogin=()=> {
    const data ={
      grant_type: "convert_token",
      client_id: "xsBYPRPiqUAH6JT0jPeOAwjoMDAl1IR74P9XSmTz",
      client_secret: "g8Ymi2bfqMu57MqedipQWiUFzNb6YAS0qnLFrZ5nlvfavFYIlWcNHQ43yxXqI8GBaWJcT5MQnP7cgLBQabihj3weR3z5KMILcSB48FD7X1q7HD8zJiqKjj1cX2jfgwtX",
      backend: "google-oauth2",
      token: this.state.googleToken
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }

    this.props.postLogin(formBody)

    if(this.props.login){
      if(this.props.login.success){
        this.props.navigation.navigate("BottomTab");
      }
    }
  }

  login() {
    this.props.navigation.navigate('BottomTab')
  }
}

const mapStateToProps = ({ login }) => {
  return {
    login
  };
};

const mapDispatchToProps = dispatch => {
  return {
    postLogin: payload => {
      dispatch(postLogin(payload));
    }
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LoginScreen);


// this.props.navigation.navigate("BottomTab")