import React, {Component} from 'react';
import {View, StyleSheet, SafeAreaView, TouchableOpacity, TextInput, Text, FlatList, Image} from 'react-native';
import {connect} from 'react-redux';
import Icon from "react-native-vector-icons/Feather";
import Icon1 from "react-native-vector-icons/Ionicons";
import Icon2 from "react-native-vector-icons/Octicons";
import { scale } from '../constants/Scale';
import { postSearchVideo } from '../actions/SearchAction';
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import {family,size} from "../constants/Fonts";

export class SearchScreen extends Component {
  constructor(props) {
    super(props);
    this.state= {
        searchValue: ""
    }
  }

  render() {
    let videoData = this.props.search_video && this.props.search_video.searchResult && this.props.search_video.searchResult.results ? this.props.search_video.searchResult.results : [];
    let isLoading = this.props.search_video.loading ;
    let isError = this.props.search_video.error ;
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fafafa',}}>
      <Loader loading={isLoading} />  
        <View style={{flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 18, paddingVertical: 5, elevation: 3, backgroundColor: 'white', alignItems: 'center'}}>
            <TouchableOpacity style={{}} onPress={()=> this.props.navigation.goBack()}>
                <Icon1 name={'ios-arrow-back'} size={26} color={'#a9a9a9'}/>
            </TouchableOpacity>

            <TextInput
                placeholder={'Search Navedia'}
                placeholderTextColor={'#a9a9a9'}
                onChangeText={(val)=> this.setState({searchValue: val})}
                style={styles.searchBox}
             />   

            <TouchableOpacity style={{marginLeft: scale(15)}} onPress={()=> this.props.postSearch(this.state.searchValue)}>
                    <Icon2 name={'search'} color={'#a9a9a9'} size={22}/>
            </TouchableOpacity>
        </View>

        <View >
            <Text style={{color: '#262625', fontSize: 14, fontFamily: family.bold , marginBottom: 3, marginTop: 6, marginLeft: 5}}>
              Search results
            </Text>
            <SafeAreaView style={{}}>
            {videoData && videoData.length >0 ?
              <FlatList
                data={videoData}
                keyExtractor={(item, index)=> index.toString()}
                showsVerticalScrollIndicator={false}
                style={{marginLeft: 0}}
                renderItem={({item})=> 
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.id})}
                    style={styles.featuredBox}>
                      <ImageBackground
                        resizeMode={'cover'}
                        style={{
                          height: 220,
                          width: '100%'
                          }}
                        source={{uri: item.image}}>
                        <View style={{backgroundColor: 'black', borderWidth: 0.6, borderColor: '#fafafa', alignSelf: 'flex-end', margin: 3, opacity: 0.5}}>
                          <Text style={{color: '#fafafa', fontFamily: family.regular, paddingHorizontal: 5, paddingVertical: 3, fontSize: 12}}>{item.get_length}</Text>
                        </View>
                      </ImageBackground>  
                      <View style={{flexDirection: 'row'}}>
                        <Image style={{width: 50, height: 50, borderRadius: 100, marginLeft: 10, alignSelf: 'center', borderWidth: 1, borderColor: 'grey'}} source={{uri: item.channel.image}}/>
                        <View>
                          <Text style={{marginLeft: 10, marginTop: 10, fontFamily: family.regular}}>{item.name}</Text>
                          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
                            <Text style={{opacity: 0.7, marginVertical: 5, fontFamily: family.condensed_regular}}>
                              {item.channel.name}
                            </Text>
                            <Icon
                              name={'check-circle'}
                              size={12}
                              color={'grey'}
                              style={{marginHorizontal: 6}}
                            />
                          </View>
                          <View style={{flexDirection: 'row', marginLeft: 10, marginBottom: 10}}>
                            <View style={{flexDirection: 'row'}}>
                              <Icon name={'eye'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.views} views
                              </Text>
                            </View>

                            <View style={{flexDirection: 'row', marginLeft: 8}}>
                              <Icon name={'calendar'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.get_date}{' '}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>  
                      
                    </TouchableOpacity>
                }
              /> : null}
            </SafeAreaView>  
          </View> 
      </SafeAreaView>
    );
  }
}

//   onSearch() {
//     const { searchValue } = this.state;
//     const data ={
//       value: searchValue
//     };
  
//     let formBody = new FormData();
//     for (let key in data) {
//       formBody.append(key, data[key]);
//     }
//     this.props.postSearch(formBody);
//   }
// }

const styles = StyleSheet.create({ 
    searchBox: {
        backgroundColor: '#f0f0f0',
        borderRadius: 2,
        color: 'grey', width: '78%',
        paddingHorizontal: 8
    },
    featuredBox: {
      // height: 250, 
      marginVertical: 10,
      backgroundColor: 'white',
      // marginHorizontal: 20,
      overflow: 'hidden',
      // borderRadius: 10,
      // elevation: 1
    },
    videoContainer: {
      margin: 20
    },
})

const mapStateToProps = ({ search_video }) => {
    return {
        search_video
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      postSearch: (params) => {
        dispatch(postSearchVideo(params));
      }
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(SearchScreen);
