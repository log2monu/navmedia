import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  Dimensions,
  SafeAreaView,
  FlatList,
} from 'react-native';
import SwiperFlatList from 'react-native-swiper-flatlist';
import Icon from 'react-native-vector-icons/Feather';
import {connect} from 'react-redux';
import {NavigationEvents} from 'react-navigation';
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import {base_url} from '../constants/config';
import HeaderComponent from '../components/HeaderComponent';
import {getHomeData} from '../actions/HomeAction';
import {scale} from '../constants/Scale';
import { ScrollView } from 'react-native-gesture-handler';
import { family } from '../constants/Fonts';
import { getCategories, getCatSubCategories, getServiceSubCategories, getBlog } from '../actions/CategoryAction';
import { getFeaturedVideos } from "../actions/TrendingAction";

const {height, width} = Dimensions.get('window');

class ServiceSubCategoryScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      serCatId: this.props.navigation.getParam("ser_cat_id")
    };
  }

  callApiFunctions() {
    this.props.getServiceSubCategories(this.state.serCatId)
  }

  render() {
    const SerSubCategoryData = this.props.ser_sub_category && this.props.ser_sub_category.subCategoryList && this.props.ser_sub_category.subCategoryList.results ? this.props.ser_sub_category.subCategoryList.results : [];
    const blogsData= this.props.blog && this.props.blog.blogData && this.props.blog.blogData && this.props.blog.blogData.results ? this.props.blog.blogData.results : [];

    let isLoading = this.props.ser_sub_category.loading || this.props.blog.loading;
    let isError = this.props.ser_sub_category.error || this.props.blog.error;

    console.log("sjvs :", this.props.blog);

    return (
      <View style={styles.container}>
        {/* <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} /> */}
        <Loader loading={isLoading} />   
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()}/>
        <HeaderComponent navigation={this.props.navigation} />
        <ScrollView showsVerticalScrollIndicator={false}>
        <View >
            <View>
              <Text style={{padding: 5,color: '#262625', fontSize: 14, fontFamily: family.bold, marginVertical: 5}}>
                Service sub Categories
              </Text>
            </View>
            <SafeAreaView style={{flex:1}}>
            {SerSubCategoryData && SerSubCategoryData.length > 0 ?
              <SwiperFlatList
                autoplay
                autoplayDelay={2.5}
                index={0}
                autoplayLoop
                data={SerSubCategoryData}
                renderItem={({item}) => (
                  <TouchableOpacity
                  onPress={() => this.props.getBlogs(item.id)}
                    activeOpacity={0.5}
                    style={styles.channelsBox}>
                      <Image source={{uri: item.image}} style={{height: 60, width: 60, borderRadius: 100, borderColor: 'white', borderWidth: 6, resizeMode: 'cover'}} />
                      <Text style={{ marginTop: 3, fontFamily: family.condensed_regular, fontSize: 12, }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              /> : null}
            </SafeAreaView>  
          </View>
     
          <View >
            <View>
              <Text style={{padding: 5,color: '#262625', fontSize: 14, fontFamily: family.bold, marginVertical: 5}}>
                Blogs
              </Text>
            </View>
            <SafeAreaView style={{flex:1}}>
            {blogsData && blogsData.length > 0 ?
              <FlatList
                data={blogsData}
                keyExtractor={(item, index) => index.toString()}
                renderItem={({item}) => (
                  <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("BlogScreen", {"blog": item})}
                    activeOpacity={0.5}
                    style={styles.channelsBox1}>
                      <Image source={{uri: item.image}} style={{height: 60, width: 60, borderRadius: 10, borderColor: 'white', borderWidth: 6, resizeMode: 'cover'}} />
                      <Text style={{ marginTop: 3, fontFamily: family.condensed_regular, fontSize: 13, marginHorizontal: 5 }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              /> : null}
            </SafeAreaView>  
          </View>
         
        </ScrollView>
      </View> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  channelsBox: {
    marginHorizontal: 4,
    borderRadius: 50,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  channelsBox1: {
    marginHorizontal: 10,
    borderRadius: 8,
    padding: 0,
    alignItems: 'center',
    flexDirection: 'row',
    elevation: 1,
    marginBottom: 6,
    backgroundColor :'white', 
    paddingVertical: 8,
    paddingHorizontal: 10
  },
  subscribeButton: {
    borderRadius: 1,
    borderColor: '#349beb',
    borderWidth: 1,
    marginVertical: scale(8),
    alignItems: 'center',
    justifyContent: 'center',
    elevation: 1
  },
});

const mapStateToProps = ({  ser_sub_category, blog }) => {
  return {  ser_sub_category, blog };
};

const mapDispatchToProps = dispatch => {
  return {
    getServiceSubCategories: (params) => {
      dispatch(getServiceSubCategories(params));
    },
    getBlogs: (params) => {
      dispatch(getBlog(params));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ServiceSubCategoryScreen);


