import React, { Component } from 'react';
import { SafeAreaView, Text, View, Image, FlatList, TouchableOpacity, StyleSheet, Dimensions } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import HeaderComponent from '../components/HeaderComponent';
import {scale} from '../constants/Scale';
import Icon from 'react-native-vector-icons/Feather';
import { family } from '../constants/Fonts';

const {height, width} = Dimensions.get('window');

export default class CategoryContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      categoryName: this.props.navigation.getParam("category") ,
    
    };
  }

  render() {
    return (
      <View style={styles.container}>
      <HeaderComponent navigation={this.props.navigation} />
        <View style={{}}>
          <Text
            style={{
              color: '#262625',
              fontSize: 16,
              marginBottom: 10,
              margin: 20,
              fontFamily: family.bold
            }}>
            {this.state.categoryName}
          </Text>

          <FlatList
            data={this.state.categoryData}
            keyExtractor={(item, index) => index.toString()}
            showsVerticalScrollIndicator={false}
            style={{marginBottom: '24%'}}
            renderItem={({item}) => (
              <TouchableOpacity onPress={() => this.props.navigation.navigate('VideoScreen')} style={styles.featuredBox}>
                <Image
                  resizeMode={'cover'}
                  style={{
                    height: 220,
                    width: '100%',
                  }}
                  source={item.thumbnail_image}
                />
                <View style={{flexDirection: 'row', paddingHorizontal: scale(10)}}>
                  <View style={{alignItems: 'center', justifyContent: 'center'}}>
                    <Image
                      resizeMode={'cover'}
                      style={{
                        height: scale(50),
                        width: scale(50),
                        borderRadius: scale(100),
                      }}
                      source={item.channel_image}
                    />
                  </View>
                  <View>
                    <Text
                      style={{
                        marginLeft: scale(10),
                        marginTop: scale(10),
                        fontFamily: family.regular
                      }}>
                      {item.title}
                    </Text>
                    <View
                      style={{
                        flexDirection: 'row',
                        alignItems: 'center',
                        marginLeft: scale(10),
                      }}>
                      <Text
                        style={{
                          opacity: 0.8,
                          marginVertical: 2,
                          fontFamily: family.condensed_regular
                        }}>
                        {item.category_name}
                      </Text>
                      <Icon
                        name={'check-circle'}
                        size={12}
                        color={'grey'}
                        style={{marginHorizontal: 6}}
                      />
                    </View>
                    <View
                      style={{
                        flexDirection: 'row',
                        marginLeft: 10,
                        marginBottom: 10,
                      }}>
                      <View style={{flexDirection: 'row'}}>
                        <Icon name={'eye'} color={'#949494'} size={15} />
                        <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                          {item.views} views
                        </Text>
                      </View>

                      <View style={{flexDirection: 'row', marginLeft: 8}}>
                        <Icon name={'calendar'} color={'#949494'} size={15} />
                        <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                          {item.duration}{' '}
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </TouchableOpacity>
            )}
          />
        </View>
    </View>
    );
  }
  
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  featuredBox: {
    marginVertical: 10,
    // elevation:1,
    backgroundColor: 'white',
    // marginHorizontal: 20,
    overflow: 'hidden',
    // borderRadius: 10,
  },

});