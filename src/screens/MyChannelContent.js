import React, { Component } from "react";
import { SafeAreaView, Image, Dimensions, ImageBackground, Text, Picker, TextInput, StyleSheet, View, ScrollView, TouchableOpacity, FlatList, Alert } from 'react-native';
import HeaderComponent from "../components/HeaderComponent";
import { scale } from "../constants/Scale";
import Icon from 'react-native-vector-icons/Feather';
import Icon6 from 'react-native-vector-icons/AntDesign'
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import { family } from "../constants/Fonts";
import ImagePicker from 'react-native-image-crop-picker';
import MultiSelect from 'react-native-multiple-select';
import Modal from "react-native-modal";
import FloatingLabel from "react-native-floating-labels";
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { NavigationEvents } from "react-navigation";
import { getMyChannelDetails, getChannelAbout, getChannelPopular, deleteChannel, putChannel } from "../actions/ChannelsAction";
import {connect} from 'react-redux';
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import Toast from 'react-native-simple-toast';
import Icon1 from 'react-native-vector-icons/Entypo';
import { getCategories, getCatSubCategories } from "../actions/CategoryAction";
import { postVideoUpload } from "../actions/VideoUploadAction";
import { deleteVideo } from "../actions/VideoAction";

const {height, width} = Dimensions.get('window');

const lorem = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

class MyChannelContent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tabData: [
        {
          name: "Videos",
          id: 1
        },
        {
          name: "Popular Videos",
          id: 2
        },
        {
          name: "About",
          id: 3
        }
      ],
      selectedId: 1,
      isSelected: true,
      isPremium: true,
      video: null,
      catId : null,
      subCatId: null,
      videoTitle: "",
      videoDesc: "",
      videoThumbnail: null,
      isVideoModalVisible: false,
      isEditModalVisible: false,
      selectedItems: [],
      channelId: this.props.navigation.getParam("channel_id"),
      channelName: "",
      channelAbout: "",
      channelImage: null,
      bannerImage: null,
      subscribed: false
    };
  }

  UNSAFE_componentWillMount() {
    this.setState({channelId: this.props.navigation.getParam("channel_id")}) ;
  }

  callApiFunctions() {
    this.props.getChannelAbout(this.state.channelId)
    this.props.fetchChannelContent(this.state.channelId)
    this.props.getChannelPopularVideos(this.state.channelId);
    this.props.getCategories();
  }

  componentDidUpdate(prevProps) {
    if (this.props.del_channel) {
      if (this.props.del_channel.success &&
          !this.props.del_channel.loading &&
          !this.props.del_channel.error) {
            if(this.props.del_channel != prevProps.del_channel)
              this.props.navigation.goBack();
      }
    }
    if (this.props.video_upload) {
      if (this.props.video_upload.success &&
          !this.props.video_upload.loading &&
          !this.props.video_upload.error) {
            if(prevProps.video_upload != this.props.video_upload){
              this.toggleVideoModal();
              Toast.showWithGravity('Video uploaded successfully.', Toast.LONG, Toast.CENTER)
            }
            
      }
    }
  }

  pickVideo() {
    ImagePicker.openPicker({
      mediaType: "video",
    }).then((video) => {
      // this.setState({ video : video.path, videoExtention: fileType(video.path)})
      this.setState({ video : video.path,})

    });
  }

  pickThumbnailImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        videoThumbnail: image.path
      });
    }).catch(e => alert(e));
  }

  pickChannelImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        channelImage: image.path
      });
    }).catch(e => alert(e));
  }

  pickBannerImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        bannerImage: image.path
      });
    }).catch(e => alert(e));
  }

  render() {
    const {channel_popular, channel_details, channel_content, video_upload, category, cat_sub_category, del_channel, del_video, update_channel } = this.props;
    let catData = category && category.categoryList && category.categoryList.results && category.categoryList.results.length > 0 ? category.categoryList.results : [];
    let catSubCatData = cat_sub_category && cat_sub_category.subCategoryList && cat_sub_category.subCategoryList.results && cat_sub_category.subCategoryList.results.length > 0 ? cat_sub_category.subCategoryList.results : [];
    let videoData = channel_content && channel_content.data && channel_content.data.results && channel_content.data.results.length > 0 ? channel_content.data.results : [];
    let popData = channel_popular && channel_popular.data && channel_popular.data.results && channel_popular.data.results.length > 0 ? channel_popular.data.results : [];
    let aboutData = channel_details && channel_details.data && channel_details.data.category  ? channel_details.data.category : [];

    let isLoading = category.loading || cat_sub_category.loading || channel_popular.loading || channel_details.loading || channel_content.loading || video_upload.loading || del_channel.loading || del_video.loading || update_channel.loading ? true : false;
    let isError = category.error || cat_sub_category.error || channel_popular.error || channel_details.error || channel_content.error || video_upload.error || del_channel.error || del_video.error || update_channel.error ? true : false;

    return (
      <SafeAreaView style={{flex: 1, backgroundColor: "#fafafa"}}>
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
        <Loader loading={isLoading} />     
        <HeaderComponent navigation={this.props.navigation} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <ImageBackground style={{height: scale(120)}} source={{uri : aboutData.banner}}/>
          <TouchableOpacity
            style={{height: 100, alignItems: 'center', justifyContent: 'center', marginTop: -40}}
            onPress={()=> {}}
          >
            <Image style={{width: 90, height: 90, borderRadius: 100, borderWidth: 6, borderColor:'white'}} source={{uri: aboutData.image}}/>
            <Text style={{fontFamily: family.bold, fontSize: 14}}>{aboutData.name}</Text>
          </TouchableOpacity>  
        <View style={{justifyContent: 'center', alignItems: 'center', marginTop: 15}}>
          <FlatList
            data={this.state.tabData}
            keyExtractor={(item, index)=> index.toString()}
            horizontal={true}
            showsHorizontalScrollIndicator={false}
            style={{marginHorizontal: scale(20)}}
            renderItem={({item}) => 
              <View style={{marginHorizontal: scale(6)}}>
                {this.state.isSelected && this.state.selectedId == item.id? (
                <TouchableOpacity 
                  style={{
                    borderRadius: 3, 
                    backgroundColor: 'white', 
                    // borderColor: 'grey', 
                    // borderWidth: 2,
                    elevation: 2
                  }}
                  onPress={()=> this.setState({selectedId: item.id})}
                >
                  <Text style={{color: "#0089D0", paddingHorizontal: scale(15), paddingVertical: scale(6), fontFamily: family.condensed_bold}}>{item.name}</Text>
                </TouchableOpacity>
              ): (
                <TouchableOpacity 
                  style={{borderRadius: 2, backgroundColor: 'white'}}
                  onPress={()=> this.setState({selectedId: item.id})}
                >
                  <Text style={{opacity: 0.8, paddingHorizontal: scale(15), paddingVertical: scale(6), fontFamily: family.condensed_regular}}>{item.name}</Text>
                </TouchableOpacity>
              )}
              </View>
            }
          />  
          </View>
          {/* <View style={{height: scale(0.3), width: '100%', backgroundColor: 'grey'}}/> */}
          
          {this.state.selectedId === 1 && this.renderVideos(videoData)}
          {this.state.selectedId === 2 && this.renderPopularVideos(popData)}
          {this.state.selectedId === 3 && this.renderAbout(aboutData)}

        </ScrollView>
        <TouchableOpacity
          style={{
            justifyContent: "center",
            alignItems:'center',
            position: 'absolute',                                          
            bottom: scale(10),                                                    
            right: scale(10),
            backgroundColor: 'white',
            borderRadius: 100,
            elevation: 3,
          }}
          onPress={() => this.setState({isVideoModalVisible: !this.state.isVideoModalVisible})}
        >
          <Icon name={"plus"} color={'#0089D0'} size={27} style={{padding: 12}}/>
        </TouchableOpacity> 

        {this.renderVideoUploadModal(catData,catSubCatData)}
      </SafeAreaView>
    );
  }

  renderVideos(data) {
    return(
      <View style={{}}>
            <Text
              style={{
                color: '#262625',
                fontSize: 16,
                marginVertical: 10,
                marginHorizontal: 10,
                fontFamily: family.bold
              }}>
              Videos
            </Text>

            <FlatList
              data={data}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              style={{marginBottom: '24%'}}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.id})} style={styles.featuredBox}>
                  <ImageBackground
                    resizeMode={'cover'}
                    style={{
                      height: 220,
                      width: '100%'
                      }}
                    source={{uri: item.image}}>
                    <View style={{backgroundColor: 'black', borderWidth: 0.6, borderColor: '#fafafa', alignSelf: 'flex-end', margin: 3, opacity: 0.5}}>
                      <Text style={{color: '#fafafa', fontFamily: family.regular, paddingHorizontal: 5, paddingVertical: 3, fontSize: 12}}>{item.get_length}</Text>
                    </View>
                  </ImageBackground>
                  <View style={{flexDirection: 'row', paddingHorizontal: scale(10), justifyContent: 'space-between'}}>
                    <View>
                      <Text
                        style={{
                          marginLeft: scale(10),
                          marginTop: scale(10),
                          fontFamily: family.condensed_regular
                        }}>
                        {item.name}
                      </Text>
    
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          marginBottom: 10,
                        }}>
                        <View style={{flexDirection: 'row'}}>
                          <Icon name={'eye'} color={'#949494'} size={15} />
                          <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                            {item.views} views
                          </Text>
                        </View>

                        <View style={{flexDirection: 'row', marginLeft: 8}}>
                          <Icon name={'calendar'} color={'#949494'} size={15} />
                          <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                            {item.get_date}{' '}
                          </Text>
                        </View>
                      </View>
                    </View>
                    <TouchableOpacity onPress={()=> this.deleteVideo(item.id)}>
                      <Icon3 name={"delete"} color={"#0089D0"} size={20} style={{padding: 10}}/>
                    </TouchableOpacity>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
    )
  }

  renderPopularVideos(data) {
    return(
      <View style={{}}>
            <Text
              style={{
                color: '#262625',
                fontSize: 16,
                marginVertical: 10,
                marginLeft: 10,
                fontFamily: family.bold
              }}>
              Popular videos
            </Text>

            <FlatList
              data={data}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              style={{marginBottom: 0,}}
              renderItem={({item}) => (
                <TouchableOpacity onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.id})} style={styles.featuredBox}>
                  <ImageBackground
                    resizeMode={'cover'}
                    style={{
                      height: 220,
                      width: '100%'
                      }}
                    source={{uri: item.image}}>
                    <View style={{backgroundColor: 'black', borderWidth: 0.6, borderColor: '#fafafa', alignSelf: 'flex-end', margin: 3, opacity: 0.5}}>
                      <Text style={{color: '#fafafa', fontFamily: family.regular, paddingHorizontal: 5, paddingVertical: 3, fontSize: 12}}>{item.get_length}</Text>
                    </View>
                  </ImageBackground>
                  <View style={{flexDirection: 'row', paddingHorizontal: scale(10), justifyContent: 'space-between'}}>
                    <View>
                      <Text
                        style={{
                          marginLeft: scale(10),
                          marginTop: scale(10),
                          fontFamily: family.condensed_regular
                        }}>
                        {item.name}
                      </Text>
    
                      <View
                        style={{
                          flexDirection: 'row',
                          marginLeft: 10,
                          marginBottom: 10,
                        }}>
                        <View style={{flexDirection: 'row'}}>
                          <Icon name={'eye'} color={'#949494'} size={15} />
                          <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                            {item.views} views
                          </Text>
                        </View>

                        <View style={{flexDirection: 'row', marginLeft: 8}}>
                          <Icon name={'calendar'} color={'#949494'} size={15} />
                          <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_light}}>
                            {item.get_date}{' '}
                          </Text>
                        </View>
                      </View>
                    </View>
                    <TouchableOpacity onPress={()=> this.deleteVideo(item.id)}>
                      <Icon3 name={"delete"} color={"#0089D0"} size={20} style={{padding: 10}}/>
                    </TouchableOpacity>
                  </View>
                </TouchableOpacity>
              )}
            />
          </View>
    )
  }

  renderAbout(data) {
    return (
      <View >
       <View style={{
        borderRadius: 4,
        // borderWidth: 0.7,
        // borderColor: 'grey',
        backgroundColor: 'white',
        marginHorizontal: scale(20),
        marginTop: scale(25),
        marginBottom: scale(20),
        elevation: 1
      }}>
        <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
          <View style={{flexDirection: 'row', paddingHorizontal: 15, paddingVertical: 15}}>
            <Image source={{uri: data.image}} style={{width: 85, height: 85, borderRadius: 8}} />
            <View style={{marginHorizontal: 15, justifyContent: 'center'}}>
              <Text style={{ fontSize: scale(14), fontFamily: family.bold}}>{data.name}</Text>
              {/* <Text style={{ fontSize: scale(12), marginTop: scale(8), fontFamily: family.regular}}>Education</Text> */}
            </View>
          </View>
          <TouchableOpacity onPress={()=> this.toggleEditModal()}>
            <Icon name={"edit"} color={"#0089D0"} size={20} style={{padding: 10}}/>
          </TouchableOpacity>
        </View>
        

        <View style={{}}>
          <Text
            style={{
              color: '#262625',
              fontSize: 12,
              marginBottom: 10,
              marginHorizontal: 15,
              fontFamily: family.bold
            }}>
            About :
          </Text>
          <Text
            style={{
              color: '#262625',
              fontSize: 12,
              marginBottom: 20,
              marginHorizontal: 15,
              fontFamily: family.condensed_light
            }}>
            {data.about}
          </Text>

        </View>
      </View>
        <View style={{
        borderRadius: 4,
        // borderWidth: 0.7,
        // borderColor: 'grey',
        backgroundColor: 'white',
        marginHorizontal: scale(20),
        marginTop: scale(10),
        marginBottom: scale(10),
        elevation: 1
      }}>
        <TouchableOpacity onPress ={()=> this.deleteChannel()}>
          <Text style={{textAlign: 'center', color: 'red', fontFamily: family.bold, paddingVertical: 8}}>Delete channel</Text>
        </TouchableOpacity>
      </View>
      {this.renderEditChannelModal()}
      </View>
     
    )
  }

  deleteChannel() {
    Alert.alert(
      "Delete Channel",
      "Do you want to delete this channel ?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => this.props.deleteChannel(this.state.channelId) }
      ],
      { cancelable: true }
    );

  }

  deleteVideo(videoId) {
    Alert.alert(
      "Delete Video",
      "Do you want to delete this video ?",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => {
          this.props.deleteVideo(videoId);
          this.callApiFunctions();
        } }
      ],
      { cancelable: true }
    );

  }

  renderVideoUploadModal(catData, catSubCatData) {
    const categoryData = this.props.category.categoryList && this.props.category.categoryList.results ? this.props.category.categoryList.results : [];
    return (
      <Modal
        visible={this.state.isVideoModalVisible}
        onBackButtonPress={()=>this.toggleVideoModal()}
        onBackdropPress={()=> this.toggleVideoModal()}
        backdropColor="black"
        backdropOpacity={0.4}
        // style={{flex: 1, backgroundColor: 'grey'}}
        style={{justifyContent: 'center',margin: scale(0), backgroundColor: 'rgba(0,0,0,0.7)',}}
      >
        <View style={styles.modalContent}>
        <Text style={{fontFamily: family.bold, fontSize: scale(14), marginVertical: scale(15), color: '#0089D0'}}>Upload Video</Text>
        <View >
            <Icon name={"video"} color={"#0089D0"} size={35} style={{alignSelf: 'center'}}/>
            <View style={{paddingHorizontal: 45, paddingTop: 15}}>
              <Text style={{color: 'grey', fontFamily: family.regular}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
            </View>
          </View>
          <View style={{width: '85%', alignItems: 'center'}}>
        
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon3 name={"format-title"} color={'grey'} size={20} />
              <TextInput
                placeholder={'Video title'}
                placeholderTextColor={'grey'}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
                onChangeText={(value)=> this.setState({ videoTitle: value})}
              />  

            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon name={"file-text"} color={'grey'} size={20} />
              <TextInput
                placeholder={'Description'}
                placeholderTextColor={'grey'}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
                onChangeText={(value)=> this.setState({ videoDesc: value})}
              />  

            </View>  

            {/* <Picker
              selectedValue={this.state.catId}
              onValueChange={(itemValue, itemIndex) => 
                  this.setState({catId: itemValue})}>
                {catData.map(category => (
                  <Picker.Item label={category.name} value={category.id} />
              ))}
            </Picker> */}

            <View>
              <Text style={styles.inputLabelText}>Category </Text>
              <View style={styles.inputTextBorder}>
                <Picker
                  selectedValue={this.state.catId}
                  onValueChange={state => {
                    this.setState({ catId: state});
                    this.props.getCatSubCategories(state);
                  }}
                >
                  {catData.map(item => (
                    <Picker.Item label={item.name} value={item.id} />
                  ))}
                </Picker>
              </View>
            </View>

            {this.state.catId && 
            <View>
              <Text style={styles.inputLabelText}>Sub category </Text>
              <View style={styles.inputTextBorder}>
                <Picker
                  selectedValue={this.state.subCatId}
                  onValueChange={state => this.setState({ subCatId: state})}
                >
                  {catSubCatData.map(item => (
                    <Picker.Item label={item.name} value={item.id} />
                  ))}
                </Picker>
              </View>
            </View>}
  

            <View style={{  marginTop: 15, width : '100%', justifyContent: 'space-between', paddingHorizontal:  35}}>
            <TouchableOpacity 
              onPress={() => this.pickVideo()}
              style={{ backgroundColor: 'white', elevation: 1 , width: '100%', paddingVertical: 4, borderRadius: 5 }}
            >
              {!this.state.video ? 
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center',  paddingHorizontal: 20, paddingVertical: 4}}>
                <Text style={{alignSelf: 'center', marginRight: 5, color: 'grey'}}>Select video</Text>
                <Icon name={'video'} size={16} color={'grey'}/>
              </View>:
              <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'center',  paddingHorizontal: 20, paddingVertical: 4}}>
                <Text style={{alignSelf: 'center', marginRight: 5, color: 'grey'}}>Video selected</Text>
                <Icon6 name={'like2'} size={16} color={'#0089D0'}/>
              </View>}
            </TouchableOpacity>  
            {/* {this.state.video && 
              <Icon1 name={'check'} color={'green'} size={16} />  }  */}
          </View>


            <TouchableOpacity 
              onPress={() => this.pickThumbnailImage()}
              style={{  flexDirection : 'row', borderRadius: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', width: '75%', paddingVertical: 10, marginVertical: 10, elevation: 1, }}
            >
              <Icon1 name={"image"} color={'grey'} size={16} />
              <Text style={{  color: 'grey', fontFamily: family.regular, marginLeft: 6}}>Select image</Text>
            </TouchableOpacity> 
          

          {this.state.videoThumbnail && 
          <Image  
            style={{width: '90%', height: 100, borderRadius: 4, alignSelf: 'center'}}
            source={{uri: this.state.videoThumbnail}}
          /> } 

            <TouchableOpacity
              onPress={() => this.uploadVideo()}
              style={{
                borderRadius: 5,
                width: '80%',
                marginVertical: scale(15),
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                paddingVertical: 4,
                flexDirection: 'row',
                elevation: 1
              }}>
              <Icon1 name={"upload"} color={'#0089D0'} size={16} />
              <Text style={{color: '#0089D0', fontSize: scale(12), paddingHorizontal: scale(10), paddingVertical: scale(6), fontFamily: family.bold }}>Upload</Text>
            </TouchableOpacity> 
          </View> 
        </View>
      </Modal>
    )
  }

  renderEditChannelModal() {
    return (
      <Modal
        visible={this.state.isEditModalVisible}
        onBackButtonPress={()=>this.toggleEditModal()}
        onBackdropPress={()=> this.toggleEditModal()}
        backdropColor="black"
        backdropOpacity={0.4}
        // style={{flex: 1, backgroundColor: 'grey'}}
        style={{justifyContent: 'center',margin: scale(0), backgroundColor: 'rgba(0,0,0,0.7)',}}
      >
        <View style={styles.modalContent}>
        <Text style={{fontFamily: family.bold, fontSize: scale(14), marginVertical: scale(15), color: '#0089D0'}}>Edit Channel</Text>
        <View >
            <Icon name={"edit"} color={"#0089D0"} size={35} style={{alignSelf: 'center'}}/>
            <View style={{paddingHorizontal: 45, paddingTop: 15}}>
              <Text style={{color: 'grey', fontFamily: family.regular}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</Text>
            </View>
          </View>
          <View style={{width: '85%', alignItems: 'center'}}>
        
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon3 name={"format-title"} color={'grey'} size={20} />
              <TextInput
                placeholder={'Channel name'}
                placeholderTextColor={'grey'}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
                onChangeText={(value)=> this.setState({ channelName: value})}
              />  

            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon name={"file-text"} color={'grey'} size={20} />
              <TextInput
                placeholder={'About'}
                placeholderTextColor={'grey'}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
                onChangeText={(value)=> this.setState({ channelAbout: value})}
              />  

            </View> 

            <TouchableOpacity 
              onPress={() => this.pickChannelImage()}
              style={{  flexDirection : 'row', borderRadius: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', width: '75%', paddingVertical: 10, marginVertical: 10, elevation: 1, }}
            >
              <Icon1 name={"image"} color={'grey'} size={16} />
              <Text style={{  color: 'grey', fontFamily: family.regular, marginLeft: 6}}>Select image</Text>
            </TouchableOpacity> 
          

          {this.state.channelImage && 
          <Image  
            style={{width: '90%', height: 100, borderRadius: 4, alignSelf: 'center'}}
            source={{uri: this.state.channelImage}}
          /> } 

        <TouchableOpacity 
              onPress={() => this.pickBannerImage()}
              style={{  flexDirection : 'row', borderRadius: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', width: '75%', paddingVertical: 10, marginVertical: 10, elevation: 1, }}
            >
              <Icon1 name={"image"} color={'grey'} size={16} />
              <Text style={{  color: 'grey', fontFamily: family.regular, marginLeft: 6}}>Select banner</Text>
            </TouchableOpacity> 
          

          {this.state.bannerImage && 
          <Image  
            style={{width: '90%', height: 100, borderRadius: 4, alignSelf: 'center'}}
            source={{uri: this.state.bannerImage}}
          /> } 

            <TouchableOpacity
              onPress={() => this.editChannel()}
              style={{
                borderRadius: 5,
                width: '80%',
                marginVertical: scale(15),
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                paddingVertical: 4,
                flexDirection: 'row',
                elevation: 1
              }}>
              <Icon1 name={"upload"} color={'#0089D0'} size={16} />
              <Text style={{color: '#0089D0', fontSize: scale(12), paddingHorizontal: scale(10), paddingVertical: scale(6), fontFamily: family.bold }}>Update</Text>
            </TouchableOpacity> 
          </View> 
        </View>
      </Modal>
    )
  }

  editChannel =() => {
    const { channelName, channelAbout, channelImage, bannerImage, channelId } = this.state;
    const logoImageData = { uri: channelImage, name: "channelImage.jpeg", type: "image/jpeg"};
    const bannerImageData = { uri: bannerImage, name: "bannerImage.jpeg", type: "image/jpeg"};
    const data = {
      name : channelName,
      about: channelAbout,
      image:  logoImageData,
      banner: bannerImageData
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }

    let payload = {
      formBody, channelId
    }
    this.props.putChannel(payload);

    setTimeout(() => {
      if (this.props.update_channel) {
        if (this.props.update_channel.success) {
          this.toggleEditModal();
        }
      }
    }, 6600);

  }

  uploadVideo =() => {
    const { video, catId, subCatId, videoTitle, videoDesc, videoThumbnail, channelId } = this.state;
    const videoData = {uri: video, name: "channel_video.mp4", type: "video/mp4"}
    const thumbnailData = {uri: videoThumbnail, name: "thumbnail.jpeg", type: 'image/jpeg'};
    const data = {
      channel: channelId,
      cat: catId,
      subcat: subCatId,
      name: videoTitle,
      image: thumbnailData,
      vid: videoData,
      desc : videoDesc
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }

    this.props.postVideoUpload(formBody);

    setTimeout(() => {
      if (this.props.video_upload) {
        if (this.props.video_upload.success) {
          this.toggleVideoModal();
        }
      }
    }, 500);
    
  }

  toggleVideoModal = () => {
    this.setState({ isVideoModalVisible: !this.state.isVideoModalVisible });
  };

  toggleEditModal = () => {
    this.setState({ isEditModalVisible: !this.state.isEditModalVisible });
  };

  onSelectedItemsChange = (selectedItems) => {
    this.setState({ selectedItems });
  };

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#f5f5f5',
  },
  featuredBox: {
    marginVertical: 5,
    // elevation:1,
    backgroundColor: 'white',
    // marginHorizontal: 20,
    overflow: 'hidden',
    // borderRadius: 10,
  },
  modalContent:{
    backgroundColor: 'white',
    // padding: scale(10),
    // justifyContent: 'center',
    // alignItems: 'center',
    marginHorizontal: 20,
    borderRadius: scale(8),
    borderColor: 'rgba(0, 0, 0, 0.3)',
    borderWidth: 0.6,
    alignItems: 'center'
  },
  inputTextBorder: {
    borderWidth: 0.7,
    borderColor: 'grey',
    backgroundColor: 'white',
    height: 45,
    width: 250,
    marginHorizontal: 25,
    marginTop: 5,
    paddingLeft: 10
  },
  inputLabelText: {
    marginLeft: 25,
    color: 'grey',
    fontFamily: family.regular,
    marginTop: 15,
    marginRight: 30,
    fontSize: 15
  }
});

const mapStateToProps = ({ category, cat_sub_category, channel_content, channel_popular, channel_details, video_upload, del_channel, del_video, update_channel }) => {
  return { category, cat_sub_category, channel_content, channel_popular, channel_details, video_upload, del_channel, del_video, update_channel };
};

const mapDispatchToProps = dispatch => {
  return {
    getCategories: (params) => {
      dispatch(getCategories(params));
    },
    getCatSubCategories: (params) => {
      dispatch(getCatSubCategories(params));
    },
    fetchChannelContent: (params) => {
      dispatch(getMyChannelDetails(params));
    },
    getChannelAbout: (params) => {
      dispatch(getChannelAbout(params));
    },
    getChannelPopularVideos: (params) => {
      dispatch(getChannelPopular(params));
    },
    postVideoUpload: (params) => {
      dispatch(postVideoUpload(params));
    },
    deleteChannel: (params) => {
      dispatch(deleteChannel(params));
    },
    putChannel: (params) => {
      dispatch(putChannel(params));
    },
    deleteVideo: (params) => {
      dispatch(deleteVideo(params));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MyChannelContent);