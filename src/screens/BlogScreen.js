import React, { Component } from 'react';
import { SafeAreaView, Text, View, TouchableOpacity, ScrollView, Dimensions, Image } from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons'; 
import HTML from "react-native-render-html";

export default class BlogScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blog: this.props.navigation.getParam("blog")
    };
  }

  render() {
    console.log("Blogg :" , this.state.blog)
    return (
      <SafeAreaView style={{flex: 1, backgroundColor: '#fafafa',}}>
        <View style={{ flexDirection : 'row', paddingHorizontal: 15, width: '100%', backgroundColor: 'white', paddingVertical: 15, elevation: 2, alignItems: 'center'}}>
          <TouchableOpacity style={{}} onPress={()=> this.props.navigation.goBack()}>
            <Icon name={"ios-arrow-back"} color={"#0089D0"} size={30} />
          </TouchableOpacity>
          <Text style={{marginLeft: 20, fontSize: 20}}>{this.state.blog.name}</Text>
        </View>

        <ScrollView>
          <View style={{ padding: 10, flex: 1 }}>
            <Image source={{uri: this.state.blog.image}} style={{ width: '100%', height: 160, resizeMode: 'cover'}} />
            <HTML
              html={this.state.blog.content}
              imagesInitialDimensions={{ width: 300, height: 300 }}
              imagesMaxWidth={Dimensions.get("window").width - 20}
            />
          </View>
        </ScrollView>
      </SafeAreaView>
    );
  }
}
