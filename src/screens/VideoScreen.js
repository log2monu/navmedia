import React, { Component } from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView, Text, TouchableOpacity, Image, FlatList, StatusBar } from 'react-native';
import HeaderComponent from '../components/HeaderComponent';
import { scale } from '../constants/Scale';
import Icon from 'react-native-vector-icons/Feather';
import { Container } from "../components/video-components";
import Icons from "react-native-vector-icons/MaterialIcons";
import Icon1 from 'react-native-vector-icons/SimpleLineIcons';
import { TextInput } from 'react-native-gesture-handler';
import Video from "../components/video-components/Video"
import {family,size} from "../constants/Fonts";
import Modal from 'react-native-modal';
import {connect} from 'react-redux';
import {NavigationEvents} from 'react-navigation';
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import {base_url} from '../constants/config';
import { getVideoData } from '../actions/VideoAction';
import { getFeaturedVideos } from "../actions/TrendingAction";
import { putLikeVideo, putDislikeVideo, getComments, postComment } from '../actions/VideoControlAction';
import { getAdBanner2 } from '../actions/AdActions';
import { putSubscribe, getChannelAbout } from '../actions/ChannelsAction';
import Share from "react-native-share";
import moment from "moment";
import { getProfile } from '../actions/ProfileAction';
import Toast from "react-native-simple-toast";

// import RNBackgroundDownloader from 'react-native-background-downloader';


class VideoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
    
      comment: "",
      commentCount: "3.4k",
      isSubscribed: null,
      isFullScreen: false,
      selectedVideoId: 2,
      hasAd: this.props.premium,
      url: null,
      adUrl: `http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/ForBiggerJoyrides.mp4`,
      qualityUrl: "http://commondatastorage.googleapis.com/gtv-videos-bucket/sample/VolkswagenGTIReview.mp4",
      timer: 15,
      isAdSkipped: false,
      visibleModal : false,
      videoId: null,
      hasLiked: null,
      hasDisliked: null,
      likesCount: null,
      dislikesCount: null,
      videoId: this.props.navigation.getParam("videoId")
    };
    this.commentText = React.createRef();
  }

  UNSAFE_componentWillMount() {
    var videoDetails = this.props.video && this.props.video.videoList && this.props.video.videoList.vid ? this.props.video.videoList.vid : [];
    this.setState({ url: videoDetails.sd})
  }

  componentDidMount() {

    StatusBar.setHidden(true)
    // this.callApiFunctions();
    // var videoDetails = this.props.video && this.props.video.videoList && this.props.video.videoList.vid ? this.props.video.videoList.vid : [];
    // this.setState({ url: videoDetails.sd})
    this.interval = setInterval(
      () => this.setState((prevState)=> ({ timer: prevState.timer -1 })),
      1000
    );
  }

  

  callApiFunctions() {
    this.props.getVideoData(this.state.videoId);
    this.props.getAdBanner2();
    this.props.getComments(this.state.videoId);
    this.props.getFeaturedVideos();
    this.props.fetchProfile();
  }

  // onVideoChange = (videoId) => {
  //   this.setState({ videoId })
  //   this.props.getVideoData(videoId);
  //   this.props.getAdBanner2();
  //   this.props.getComments(videoId);
  //   this.props.getFeaturedVideos();
  //   this.props.fetchProfile();

  // }

  componentDidUpdate(prevProps){
    let videoDetails = this.props.video && this.props.video.videoList && this.props.video.videoList.vid ? this.props.video.videoList.vid : [];
    if (this.props.video) {
      if (this.props.video.success &&
          !this.props.video.loading &&
          !this.props.video.error) {    
          if(prevProps.video != this.props.video){
            this.setState({ 
              url: videoDetails.sd, 
              isSubscribed: videoDetails.channel.is_sub,
              hasLiked: videoDetails.has_liked,
              hasDisliked: videoDetails.has_disliked,
              likesCount: videoDetails.likesnum,
              dislikesCount: videoDetails.dislikesnum
            })
          }     
      }
    } 

    if (this.props.subscribe) {
      if (this.props.subscribe.success &&
          !this.props.subscribe.loading &&
          !this.props.subscribe.error) {    
          if(prevProps.subscribe != this.props.subscribe){
            this.setState({ isSubscribed: !this.state.isSubscribed})
          }     
      }
    } 

    if (this.props.post_comment) {
      if (this.props.post_comment.success &&
          !this.props.post_comment.loading &&
          !this.props.post_comment.error) {    
          if(prevProps.post_comment != this.props.post_comment){
            this.commentText.current.clear();
            Toast.showWithGravity('Comment posted!', Toast.LONG, Toast.CENTER)    
          }     
      }
    }

    if (this.props.like) {
      if (this.props.like.success &&
          !this.props.like.loading &&
          !this.props.like.error) {    
          if(prevProps.like != this.props.like){
            this.setState({ 
              hasLiked: this.props.like.data.result,
              hasDisliked: !this.props.like.data.result,
              likesCount: this.props.like.data.likes,
              dislikesCount: this.props.like.data.dislikes
            })
          }     
      }
    } 

    if (this.props.dislike) {
      if (this.props.dislike.success &&
          !this.props.dislike.loading &&
          !this.props.dislike.error) {    
          if(prevProps.dislike != this.props.dislike){
            this.setState({ 
              hasLiked: !this.props.dislike.data.result,
              hasDisliked: this.props.dislike.data.result,
              likesCount: this.props.dislike.data.likes,
              dislikesCount: this.props.dislike.data.dislikes
            })
          }     
      }
    } 

    if(this.state.timer === 1){ 
      clearInterval(this.interval); 
    }
  }

  componentWillUnmount(){
    StatusBar.setHidden(true)
    clearInterval(this.interval);
  }

  // onDownload = (task) => {
  //  task.begin((expectedBytes) => {
  //       console.log(`Going to download ${expectedBytes} bytes!`);
  //   }).progress((percent) => {
  //       console.log(`Downloaded: ${percent * 100}%`);
  //   }).done(() => {
  //       console.log('Download is done!');
  //   }).error((error) => {
  //       console.log('Download canceled due to error: ', error);
  //   });
    
  //   // Pause the task
  //   task.pause();
    
  //   // Resume after pause
  //   task.resume();
    
  //   // Cancel the task
  //   task.stop();
  // }

  render() {
    
    console.log("vidIddd :" , this.state.videoId)
    const { videoList } = this.props.video;
    var videoDetails = this.props.video && this.props.video.videoList && this.props.video.videoList.vid ? this.props.video.videoList.vid : [];
    var videoUrlSD = videoDetails.sd;
    var videoUrlHD = videoDetails.hd;
    // var hasAd = this.props.video && this.props.video.videoList && this.props.video.videoList.ad ? true : false ;
    var adUrl = this.props.video && this.props.video.videoList && this.props.video.videoList.ad ? this.props.video.videoList.ad.raw_vid : null;
    let adBanner = this.props.ad_banner_2 && this.props.ad_banner_2.data && this.props.ad_banner_2.data.banner2 ? this.props.ad_banner_2.data.banner2 : {};

    const commentsData = this.props.comments.data && this.props.comments.data.results ? this.props.comments.data.results : [];
    const featuredData = this.props.featured.featuredList && this.props.featured.featuredList.results ? this.props.featured.featuredList.results : [];
    let isLoading = this.props.video.loading || this.props.featured.loading || this.props.post_comment.loading || this.props.comments.loading || this.props.ad_banner_2.loading;
    let isError = this.props.video.error || this.props.featured.error || this.props.post_comment.error || this.props.comments.error || this.props.ad_banner_2.error;
    console.log("Video details :", this.props.video);
    const userImageData = this.props.profile && this.props.profile.userData && this.props.profile.userData.profile && this.props.profile.userData.profile.image ? this.props.profile.userData.profile.image : null;
    let videoQualityData = [{
      selectedId: 1,
      label: 'HD',
      quality: videoUrlHD,
      Addnlabel: null
    },
    {
      selectedId: 2,
      label: 'SD',
      quality: videoUrlSD,
      Addnlabel: null
    }];
    // let task = RNBackgroundDownloader.download({
    //   id: 'file123',
    //   url: videoDetails.sd,
    //   destination: `${RNBackgroundDownloader.directories.documents}/video.mp4`
    // });
    return (
      
      <SafeAreaView style={{flex:1, backgroundColor: 'white'}}>
        {/* <HeaderComponent navigation={this.props.navigation} /> */}
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
        <Loader loading={isLoading} />    
        <View  >
          
          {videoDetails &&
          <View style={styles.videoContainer}>
          <Container>
            <Video
              autoPlay
              url={!this.state.hasAd ? adUrl : this.state.url}
              rotateToFullScreen={true}
              // loop
              // playInBackground
              // playWhenInactive
              // inlineOnly
              // onMorePress={() => this.onMorePress()}
              onFullScreen={status => this.setState({isFullScreen: status})}
            />
            {!this.state.isAdSkipped && !this.state.hasAd ?
            <View >
              {this.state.timer > 1? 
              <View 
                style={{backgroundColor: 'black', opacity: 0.4, borderRadius: 3, borderColor: 'white', borderWidth: 0.6, alignSelf: 'flex-end', justifyContent: 'center', paddingHorizontal: 10, paddingVertical: 5, elevation: 2, position: 'absolute', bottom: 35, zIndex: 1000}}
              >
                <Text style={{color: 'white', fontFamily: family.regular}}>{this.state.timer}</Text>
              </View>   : 
                
              <TouchableOpacity 
                style={{backgroundColor: 'black', opacity: 0.4, borderRadius: 3, borderColor: 'white', borderWidth: 0.6, alignSelf: 'flex-end', justifyContent: 'center', paddingHorizontal: 14, paddingVertical: 7, elevation: 2, position: 'absolute', bottom: 35, zIndex: 1000}}
                onPress={()=> {
                  this.setState({hasAd: !this.state.hasAd, adUrl: videoDetails.sd, isAdSkipped: true })
                  // hasAd = false
                  }}
              >
                <Text style={{color: 'white', fontFamily: family.regular}}>Skip Ad.</Text>
              </TouchableOpacity> }
            </View> : 
            <TouchableOpacity 
                style={{borderRadius: 3,  alignSelf: 'flex-end', justifyContent: 'center', paddingHorizontal: 14, paddingVertical: 7, position: 'absolute', zIndex: 1000}}
                onPress={()=> this.toggleSettings()}
              >
                <Icon name={"settings"} color={'#ebebeb'} size={20}/>
              </TouchableOpacity>}
            
            </Container>
          </View>}

      <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 200}}>
          <View style={{marginHorizontal: 20, backgroundColor: 'white', padding: 10, borderRadius: 6,  marginTop: 20}}>
            <Text style={{fontFamily: family.medium, fontSize: scale(14)}}>{videoDetails.name}</Text>
            <View style={{flexDirection: 'row', alignItems: 'center', marginVertical: scale(5)}}>
              <Icon name={'eye'} color={'grey'} size={15} />
              <Text style={{color: 'grey', marginHorizontal: 10, fontFamily: family.condensed_regular}}>{videoDetails.views} views</Text>
            </View>

            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: "center"}}>
                {this.state.hasLiked  ? (
                  <TouchableOpacity 
                  style={{ alignItems: 'center', justifyContent: 'center'}}
                  onPress={()=> {
                    // this.setState({ hasLiked: !this.state.hasLiked})
                    this.props.putLike(this.state.videoId)
                    }}
                >
                  <Icon1 name={'like'} color={'#0089D0'} size={18} style={{padding: 8}}/>
                  <Text style={{color: '#0089D0', fontFamily: family.condensed_regular}}>{this.state.likesCount}</Text>
                </TouchableOpacity> ) :(
                  <TouchableOpacity 
                  style={{ alignItems: 'center', justifyContent: 'center'}}
                  onPress={()=> {
                    // this.setState({ hasLiked: !this.state.hasLiked})
                    this.props.putLike(this.state.videoId)
                    }}
                >
                  <Icon1 name={'like'} color={'grey'} size={18} style={{padding: 8}}/>
                  <Text style={{color: 'grey', fontFamily: family.condensed_regular}}>{this.state.likesCount}</Text>
                </TouchableOpacity>
                )}

                { this.state.hasDisliked  ? (
                  <TouchableOpacity 
                  style={{ alignItems: 'center', justifyContent: 'center'}}
                  onPress={()=> {
                    // this.setState({ hasDisliked: !this.state.hasDisliked})
                    this.props.putDislike(this.state.videoId)
                    }}
                >
                  <Icon1 name={'dislike'} color={'#0089D0'} size={18} style={{padding: 8}}/>
                  <Text style={{color: '#0089D0' , fontFamily: family.condensed_regular}}>{this.state.dislikesCount}</Text>
                </TouchableOpacity> ) : (
                  <TouchableOpacity 
                  style={{ alignItems: 'center', justifyContent: 'center'}}
                  onPress={()=> {
                    // this.setState({ hasDisliked: !this.state.hasDisliked})
                    this.props.putDislike(this.state.videoId)
                    }}
                >
                  <Icon1 name={'dislike'} color={'grey'} size={18} style={{padding: 8}}/>
                  <Text style={{color: 'grey' , fontFamily: family.condensed_regular}}>{this.state.dislikesCount}</Text>
                </TouchableOpacity> 
                )}

                {/* <TouchableOpacity 
                  style={{ alignItems: 'center', justifyContent: 'center'}}
                  onPress={()=> {}}
                  // onPress={()=> this.onDownload(task)}
                >
                  <Icon name={'download'} color={'grey'} size={18} style={{padding: 8}}/>
                  <Text style={{color: 'grey', fontFamily: family.condensed_regular}}>Download</Text>
                </TouchableOpacity> */}
                <View style={{width: 80}}></View>

                <TouchableOpacity 
                  style={{ alignItems: 'center', justifyContent: 'center'}}
                  onPress={() => {
                    Share.open({
                      title: "NavMedia",
                      message: "Watch video from link : ",
                      url: videoDetails.sd,
                      subject: "Share Link"
                    })
                      .then(res => {
                        // console.log(res);
                      })
                      .catch(err => {
                        err && console.log(err);
                      });
                  }}
                  >
                  <Icon1 name={'share'} color={'grey'} size={18} style={{padding: 8}}/>
                  <Text style={{color: 'grey', fontFamily: family.condensed_regular}}>Share</Text>
                </TouchableOpacity>
              </View>

          </View>

          {videoDetails && videoDetails.channel&&
            <View style={{
              backgroundColor: 'white', 
              borderRadius: 6, padding: 10, 
              alignItems: 'center', 
              justifyContent: 'space-between',
              flexDirection: 'row',
              marginHorizontal: scale(18),
              marginVertical: scale(4),
              // elevation: 1
              }}>
            <TouchableOpacity onPress={()=> this.props.navigation.navigate("ChannelContent", {"channel_id": videoDetails.channel.id})} style={{flexDirection: 'row', alignItems: 'center'}}>
              <Image
                // source={require('../assets/restaurant.jpg')}
                source={{uri: videoDetails.channel.image}}
                style={{height: 50, width: 50, borderRadius: 100}}
              />
              <Text style={{ marginHorizontal: scale(8), fontFamily: family.medium}}>{videoDetails.channel.name}</Text>
            </TouchableOpacity>  
            {this.state.isSubscribed ? 
                    (<TouchableOpacity
                      onPress={() => this.props.putSubscribe(videoDetails.channel.id)}
                      style={{
                        borderRadius: 4,
                        borderWidth: 0.5,
                        marginVertical: scale(8),
                        alignItems: 'center',
                        justifyContent: 'center',
                      }}>
                      <Text style={{opacity: 0.8, paddingVertical: scale(3), paddingHorizontal: 12, elevation: 1, fontSize: scale(13), fontFamily: family.condensed_regular }}>Subscribed</Text>
                      {/* <Icon name={'check'} color={'white'} size={18}  style={{marginLeft: scale(7)}}/> */}
                    </TouchableOpacity> 
                    ) :
                    (
                      <TouchableOpacity
                        onPress={() => this.props.putSubscribe(videoDetails.channel.id)}
                        style={{
                          borderRadius: 4,
                          borderColor: 'grey',
                          borderWidth: 0.4,
                          marginVertical: scale(8),
                          alignItems: 'center',
                          justifyContent: 'center',
                          backgroundColor: '#0089D0',
                          // elevation: 1
                        }}>
                        <Text style={{color: 'white', fontSize: scale(13), paddingHorizontal: scale(12), paddingVertical: scale(3), fontFamily: family.condensed_regular }}>Subscribe</Text>
                      </TouchableOpacity> 
                    )}            
          </View>}

          <View style={{
              backgroundColor: 'white', 
              borderRadius: 6, padding: 10, 
              justifyContent: 'space-between',
              marginHorizontal: scale(18),
              marginBottom: scale(4),
              // elevation: 1
              }}>
              <Text style={{fontFamily: family.medium, fontSize: scale(14)}}>Category :</Text>
              <Text style={{ color: 'grey', marginVertical: 12, fontFamily: family.regular}}>{videoDetails.cat && videoDetails.cat.name}</Text>
              <Text style={{fontFamily: family.medium, fontSize: scale(14)}}>About :</Text>
              <Text style={{ color: 'grey', marginVertical: 12, fontFamily: family.regular}}>{videoDetails.desc}</Text>
            </View>

            <View style={{marginHorizontal: 15, marginTop: 0, elevation: 2.5, backgroundColor: 'white', marginVertical: 6}}>
              <Image style={{width: '100%', height: 120, resizeMode: 'contain'}} source={{uri : adBanner.image}} />
              <View style={{paddingVertical: 4, marginHorizontal: 12}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <View style={{backgroundColor: '#0089D0', padding: 2.5}}>
                    <Text style={{color: 'white', fontFamily: family.bold}}>Ad.</Text>
                  </View>
                  <Text style={{fontFamily: family.regular, color: 'grey', opacity: 0.9, marginHorizontal: 8, fontSize: 12}}>{adBanner.title}</Text>
                </View>
                <Text style={{fontFamily: family.regular, color: 'grey', opacity: 0.9, fontSize: 12}}>{adBanner.content}</Text>
              </View>
            </View>

            <View style={{padding: 16, flex: 1}}>
            <Text style={{color: '#262625', fontSize: 14, fontFamily: family.medium, marginBottom: 3}}>
              Up Next
            </Text>

            {featuredData && 
            <SafeAreaView style={{flex:1}}>
              <FlatList
                data={featuredData}
                keyExtractor={(item, index) => index.toString()}
                showsVerticalScrollIndicator={false}
                renderItem={({item}) => (
                  <TouchableOpacity onPress={() => this.props.navigation.replace('VideoScreen', {"videoId": item.id})} style={styles.upNextBox}>
                    <Image
                      source={{uri : item.image}}
                      style={{
                        width: 122,
                        height: 77,
                      }}
                    />
                    <View 
                      style={{
                        flexDirection: 'column',
                        marginHorizontal: 18,
                        justifyContent: 'center',
                      }}>
                      <Text style={{fontFamily: family.medium}}>{item.name}</Text>
                      <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{opacity: 0.7, marginVertical: 5, fontFamily: family.regular}}>
                          {item.channel.name}
                        </Text>
                        <Icon
                          name={'check-circle'}
                          size={12}
                          color={'grey'}
                          style={{marginHorizontal: 6}}
                        />
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <View style={{flexDirection: 'row'}}>
                          <Icon name={'eye'} color={'#949494'} size={13} />
                          <Text style={{color: '#949494', marginLeft: 4, fontSize: 12, fontFamily: family.light}}>
                            {item.views} views
                          </Text>
                        </View>

                        <View style={{flexDirection: 'row', marginLeft: 10}}>
                          <Icon name={'calendar'} color={'#949494'} size={13} />
                          <Text style={{color: '#949494', marginLeft: 4, fontSize: 12, fontFamily: family.light}}>
                             {item.get_date}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                )}
              />
            </SafeAreaView>}
          </View>

          <View style={{
              backgroundColor: 'white', 
              borderRadius: 6, padding: 10, 
              justifyContent: 'space-between',
              marginHorizontal: scale(18),
              marginVertical: scale(4),
              // elevation: 1
              }}>
              {this.props.comments && this.props.comments.data &&
                <View style={{}}>
                  <Text style={{color: '#262625', fontSize: 16, fontFamily: family.condensed_bold, marginBottom: 10}}>
                    Comments   {this.props.comments.data.count}
                  </Text>
                </View>}

              <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
                <View style={{flexDirection: 'row', marginBottom: 10}}>
                  
                {(this.props.user_info && this.props.user_info.image) || userImageData ? 
                      <Image
                        style={{borderRadius: 100, width: 45, height: 45,}}
                        source={{uri: userImageData ? userImageData : this.props.user_info.image}}
                    />:
                    <Image
                        style={{borderRadius: 100, width: 45, height: 45,}}
                        source={require("../assets/user.png")}
                    />}
                  <View style={{width: '77%',}}>
                    <TextInput
                      ref={this.commentText}
                      placeholder={"Leave a comment."}
                      placeholderTextColor={'grey'}
                      style={{ marginLeft: scale(5), fontFamily: family.regular}}
                      onChangeText={(value)=> this.setState({ comment: value})}
                    /> 
                    <View style={{ backgroundColor: 'grey', height: 0.3, width: '100%'}} />
                  </View>
                   

                </View>
                <TouchableOpacity
                  style={{justifyContent: 'center', alignItems: 'center'}}
                  onPress={()=> this.postComment()}
                >
                  <Icon name={'send'} color={'#0089D0'} size={22} style={{marginRight: 15}}/>
                </TouchableOpacity>  
              </View>

              {commentsData &&
              <FlatList
                data={commentsData}
                keyExtractor={(item,index)=> index.toString()}
                showsVerticalScrollIndicator={false}
                renderItem={({item,index})=> 
                  <View style={{marginVertical: 8}}>
                    {item.user && item.user.profile_user != null ?
                      <View style={{flexDirection: 'row'}}>
                      <Image
                        source={{uri : item.user.profile_user.image}}
                        style={{height: 45, width: 45, borderRadius: 100}}
                      />
                      <View style={{marginHorizontal: 10}}>
                        <View style={{flexDirection: 'row', alignItems: 'center'}}>
                          <Text style={{fontFamily: family.condensed_regular}}>{item.user.profile_user.user.username}  .  </Text>
                          <Text style={{fontFamily: family.condensed_regular, fontSize: 12, color: 'grey'}}>{moment(item.time).calendar()}</Text>
                        </View>
                        <Text style={{color: 'grey', marginTop: 6, fontFamily: family.condensed_regular}}>{item.content}</Text>
                      </View>
                    </View> :
                    <View style={{flexDirection: 'row'}}>
                      <Image
                        source={require('../assets/logo.png')}
                        style={{height: 45, width: 45, borderRadius: 100}}
                      />
                      <View style={{marginHorizontal: 10}}>
                        <View style={{flexDirection: 'row'}}>
                          <Text style={{fontFamily: family.condensed_regular}}>Anonymous user  .  </Text>
                          <Text style={{fontFamily: family.condensed_regular, fontSize: 12, color: 'grey'}}>{moment(item.time).calendar()}</Text>
                        </View>  
                        <Text style={{color: 'grey', marginTop: 6, fontFamily: family.condensed_regular}}>{item.content}</Text>
                      </View>
                    </View>
                    }  
                    <View style={{ backgroundColor: 'grey', height: 0.3, width: '100%', marginTop: 15}} />
                  </View>   
                }
              /> }
          </View>
          </ScrollView>    
        </View>
        <Modal 
          onBackButtonPress={() => this.setState({ visibleModal: false })}
          onBackdropPress={() => this.setState({ visibleModal: false })}
          isVisible={this.state.visibleModal === true}
          style={{justifyContent: 'flex-end',margin: 0}}
        >
          <View style={{ backgroundColor: 'white',padding: 5,justifyContent: 'center'}}>
          <Text style={{fontSize: size.h5,fontFamily:family.medium,color:'#000', paddingHorizontal: 20, paddingVertical: 14, opacity: 0.4}}>Select video quality.</Text>
            <ScrollView scrollEventThrottle={16} showsVerticalScrollIndicator={false}>
              {videoQualityData.map((item, index) => (
                <View key={index} style={{paddingLeft:20,justifyContent: 'center',paddingVertical:16}}>
                  <TouchableOpacity onPress={() => this.setState({ visibleModal: false,selectedVideoId:item.selectedId, url: item.quality })}>
                  {this.state.selectedVideoId==item.selectedId?
                    <View style={{flexDirection:'row',alignSelf:'flex-start'}}>
                        <Icons
                          style={{paddingRight:8}}
                          name="done"
                          color="#0089D0"
                          size={24}
                        />
                      <Text style={{fontSize: size.h5,fontFamily:family.medium,color:'#0089D0'}}>{item.label}
                      {item.Addnlabel!=null?
                        <Text style={{fontSize: size.h6,fontFamily:family.light,color:'#8a9b8d'}}>{item.Addnlabel}</Text>:null
                      }
                      </Text>
                    </View> :
                    <View style={{flexDirection:'row',alignSelf:'flex-start'}}>
                        
                      <Text style={{fontSize: size.h5,fontFamily:family.medium,color:'black', opacity: 0.5}}>{item.label}
                      {item.Addnlabel!=null?
                        <Text style={{fontSize: size.h6,fontFamily:family.light,color:'#8a9b8d'}}>{item.Addnlabel}</Text>:null
                      }
                      </Text>
                    </View>
                     }
                  </TouchableOpacity>
                </View>
              ))}
            </ScrollView>
          </View>
        </Modal>
      </SafeAreaView>
    );
  }

  postComment = () => {
    const { videoId, comment, } = this.state;
    const data = {
      video: videoId,
      content: comment
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }

    this.props.postComment(formBody);
    this.props.getComments(videoId);
  }

  toggleSettings() {
    this.setState({ visibleModal: true })
  }
}

const styles = StyleSheet.create({
  videoContainer: {
    marginTop: scale(0),
    marginHorizontal: 0,
    backgroundColor: 'black',
    paddingBottom: scale(10),
    // height: '27%',
    justifyContent: 'center'
  },
  subscribeButton: {
    borderRadius: 1,
    borderColor: '#0089D0',
    borderWidth: 1,
    marginVertical: scale(0),
    elevation: 1
  },
  upNextBox: {
    flexDirection: 'row',
    marginVertical: 8,
    backgroundColor: 'white',
    borderRadius: 4,
    overflow: 'hidden',
    // elevation:1 
  },
})

const mapStateToProps = ({ video, featured, post_comment, comments,user_info, ad_banner_2, subscribe, profile, premium, like, dislike }) => {
  return { video, featured, post_comment, comments, user_info, ad_banner_2, subscribe, profile, premium, like, dislike };
};

const mapDispatchToProps = dispatch => {
  return {
    getVideoData: (params) => {
      dispatch(getVideoData(params));
    },
    getAdBanner2: (params) => {
      dispatch(getAdBanner2(params));
    },
    putLike: (params) => {
      dispatch(putLikeVideo(params));
    },
    putDislike: (params) => {
      dispatch(putDislikeVideo(params));
    },
    putSubscribe: (params) => {
      dispatch(putSubscribe(params));
    },
    getComments: (params) => {
      dispatch(getComments(params));
    },
    postComment: (params) => {
      dispatch(postComment(params));
    },
    getFeaturedVideos: (params) => {
      dispatch(getFeaturedVideos(params));
    },
    getChannelAbout: (params) => {
      dispatch(getChannelAbout(params))
    },
    fetchProfile: (postData) => {
      dispatch(getProfile(postData));
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VideoScreen);