import React, { Component } from "react";
import {View, StyleSheet, Image, TouchableOpacity, Text, Dimensions, FlatList, SafeAreaView, ScrollView, ImageBackground, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/Feather';

import SwiperFlatList from 'react-native-swiper-flatlist';
import {connect} from 'react-redux';
// import Video from "react-native-video";
import { NavigationEvents } from "react-navigation";
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import { base_url } from "../constants/config";
import HeaderComponent from '../components/HeaderComponent';
import { getChannels, putSubscribe } from "../actions/ChannelsAction";
import { getCategories, getServiceCategories } from "../actions/CategoryAction";
import { scale } from "../constants/Scale";
import Vimeo from "../components/video-components/Video";
import { Container } from "../components/video-components";
import { family } from "../constants/Fonts";
import ImagePicker from 'react-native-image-crop-picker';
import Modal from "react-native-modal";
import { getFeaturedVideos } from "../actions/TrendingAction";
import { getAdBanner, getAdBanner1 } from "../actions/AdActions";

const { width,height } = Dimensions.get("window");
var fullScreen = false;

class HomeScreen extends Component {

  constructor(props) {
    super(props);
    this.state = {
      vimeoFullscreen: false,
      fullScreen: false,  
    };
    
  }

  static navigationOptions = {
    tabBarVisible: fullScreen ? false : true , 
    animationEnabled: true
  }


  callApiFunctions() {
    this.props.getChannels();
    this.props.getCategories();
    this.props.getAdBanner1();
    this.props.getServiceCategories();
    this.props.getFeaturedVideos();
  }

  onFullScreen2(status) {
    this.setState({vimeoFullscreen:status})
    fullScreen = this.state.vimeoFullscreen;
    this.setFullScreen(status);
  }

  setFullScreen = status => {
    this.setState({ fullScreen: status });
  };

  onSubscribe = (channelId) => {
    this.props.putSubscribe(channelId)
    this.props.getChannels();
  }

  render() {

    let adBanner = this.props.ad_banner_1 && this.props.ad_banner_1.data && this.props.ad_banner_1.data.banner1 ? this.props.ad_banner_1.data.banner1 : {};


    const channelData = this.props.channels.channelsList && this.props.channels.channelsList.results ? this.props.channels.channelsList.results : [];
    const categoryData = this.props.category.categoryList && this.props.category.categoryList.results ? this.props.category.categoryList.results : [];
    // const bannerData = this.props.ad_banner.data && this.props.ad_banner.data.results ? this.props.ad_banner.data.results : [];
    const serviceCatData =this.props.service_category && this.props.service_category.serviceCategoryList && this.props.service_category.serviceCategoryList.results ? this.props.service_category.serviceCategoryList.results : [];
    const featuredData = this.props.featured.featuredList && this.props.featured.featuredList.results ? this.props.featured.featuredList.results : [];

    let isLoading = this.props.channels.loading || this.props.category.loading || this.props.ad_banner_1.loading || this.props.service_category.loading || this.props.featured.loading || this.props.subscribe.loading;
    let isError = this.props.channels.error || this.props.category.error || this.props.ad_banner_1.error || this.props.service_category.error || this.props.featured.error || this.props.subscribe.error;

    return (
      <SafeAreaView style={styles.container}>
          <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
          <Loader loading={isLoading} />     
          {this.state.vimeoFullscreen ? null :<HeaderComponent navigation={this.props.navigation}/>}
          <ScrollView  showsVerticalScrollIndicator={false}>
          <TouchableOpacity onPress={()=> this.props.navigation.navigate('LiveVideo')} style={styles.videoContainerBox}>
            {/* <Container >
              <Vimeo
                autoPlay
                fullscreen={true}
                // url={url}
                url={videos && videos.length > 0 ? videos[0].vid : ""}
                loop
                resizeMode="cover"
                onFullScreen={status => this.onFullScreen2(status)}
              />
            </Container> */}
            <ImageBackground style={{ paddingRight: 12, marginLeft: 20 ,width: '100%', height: '100%', padding: 8 }} source={require("../assets/live.png")}>
              <View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
                
                <View style={{flexDirection: 'row',backgroundColor: '#0089D0', elevation: 5,   paddingVertical: 5, alignItems: 'center', justifyContent: 'center', paddingHorizontal: 8}}  >
                  {/* <Icon1 name={'play'} color={'white'} size={28} style={{}}/> */}
                  <Text style={{fontFamily: family.bold, fontSize: 12, color: '#fff'}}>LIVE</Text>
                  <Icon name={"tv"} color={"#fff"} size={13} style={{marginLeft: 5, marginTop: -2}}/>
                </View>

              </View>
            </ImageBackground>            
          </TouchableOpacity>
          <View style={{padding: 0, flex: 1}}>

          <View  style={{}}>
            <Text style={{padding: 5,color: '#262625', fontSize: 14, fontFamily: family.bold, marginBottom: 3, }}>
              Channels
            </Text>
            </View>
            <SafeAreaView style={{flex:1}}>
            {channelData && channelData.length > 0 ?
              <FlatList
                horizontal
                data={channelData}
                keyExtractor={(item, index) => index.toString()}
                showsHorizontalScrollIndicator={false}
                style={{paddingLeft: 10}}
                renderItem={({item}) => (
                  <View 
                  style={{
                    backgroundColor: 'white', 
                    borderRadius: 6, padding: 10, 
                    alignItems: 'center', 
                    justifyContent: 'space-between',
                    // flexDirection: 'row',
                    marginHorizontal: scale(5),
                    marginVertical: scale(3),
                    paddingVertical: scale(12),
                    elevation: 1
                    }}>
                  <TouchableOpacity onPress={()=> this.props.navigation.navigate("ChannelContent", {"channel_id": item.id})} style={{ alignItems: 'center'}}>
                    <Image
                      source={{uri: item.image}}
                      style={{height: 70, width: 70, borderRadius: 100, borderWidth: 5, borderColor: 'white'}}
                    />
                    <Text style={{ marginHorizontal: scale(8), fontFamily: family.condensed_regular, marginVertical: 6, width: 70, textAlign: 'center', fontSize: 12}}>{item.name}</Text>
                  </TouchableOpacity>  
                  { item.is_sub == true? 
                    (<TouchableOpacity
                      onPress={() => this.onSubscribe(item.id)}
                      style={{
                        borderRadius: 5,
                        borderWidth: 0.5,                   
                        marginVertical: scale(10),
                        alignItems: 'center',
                        flexDirection: 'row',
                        justifyContent: 'space-around',
                        elevation: 1
                      }}>
                      <Text style={{opacity: 0.8, paddingVertical: scale(3), paddingHorizontal: scale(14),fontSize: scale(12), fontFamily: family.condensed_regular }}>Subscribed</Text>
                    </TouchableOpacity> 
                    ) :
                    (
                      <TouchableOpacity
                        onPress={() => this.onSubscribe(item.id)}
                        style={{
                          borderRadius: 4,
                          borderColor: 'grey',
                          borderWidth: 0.4,
                          marginVertical: scale(10),
                          alignItems: 'center',
                          justifyContent: 'center',
                          backgroundColor: '#0089D0',
                          elevation: 1
                        }}>
                        <Text style={{color: 'white', fontSize: scale(12), paddingHorizontal: scale(14), paddingVertical: scale(3), fontFamily: family.condensed_regular }}>Subscribe</Text>
                      </TouchableOpacity> 
                    )}            
                </View>
                )}
              />: null
            } 
            </SafeAreaView>  
          </View>
            
          {/* <View>
            <Text style={{padding: 5,color: '#262625', fontSize: 14, fontFamily: family.bold, marginTop: 6, marginBottom: 3}}>
              Categories
            </Text>
            </View>
            <SafeAreaView style={{flex:1, marginBottom: 10}}>
            {categoryData && categoryData.length > 0 ?
              <SwiperFlatList
                autoplay
                autoplayDelay={2}
                index={0}
                autoplayLoop
                data={categoryData}
                renderItem={({item}) => (
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate("CatSubCategory", {"cat_id": item.id})}
                    activeOpacity={0.5}
                    onPress={() => {}}
                    style={styles.channelsBox}>
                      <Image source={{uri: item.image}} style={{height: 60, width: 60, borderRadius: 100, borderColor: 'white', borderWidth: 6, resizeMode: 'cover'}} />
                      <Text style={{ marginTop: 3, fontFamily: family.condensed_regular, fontSize: 12, }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              /> : null}
            </SafeAreaView>   */}


            <View style={{marginHorizontal: 15, marginTop: 8, elevation: 2.5, backgroundColor: 'white', marginVertical: 6}}>
              <Image style={{width: '100%', height: 120, resizeMode: 'contain'}} source={{uri : adBanner.image}} />
              <View style={{paddingVertical: 4, marginHorizontal: 12}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <View style={{backgroundColor: '#0089D0', padding: 2.5}}>
                    <Text style={{color: 'white', fontFamily: family.bold}}>Ad.</Text>
                  </View>
                  <Text style={{fontFamily: family.regular, color: 'grey', opacity: 0.9, marginHorizontal: 8, fontSize: 12}}>{adBanner.title}</Text>
                </View>
                <Text style={{fontFamily: family.regular, color: 'grey', opacity: 0.9, fontSize: 12}}>{adBanner.content}</Text>
              </View>
            </View>

            <View>
            <Text style={{padding: 5,color: '#262625', fontSize: 14, fontFamily: family.bold, marginTop: 5, marginBottom: 3}}>
              Service Categories
            </Text>
            </View>
            <SafeAreaView style={{flex:1}}>
            {serviceCatData && serviceCatData.length > 0 ?
              <SwiperFlatList
                autoplay
                autoplayDelay={2.5}
                index={0}
                autoplayLoop
                data={serviceCatData}
                renderItem={({item}) => (
                  <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("ServiceSubCategoryScreen", {"ser_cat_id": item.id})}
                    activeOpacity={0.5}
                    style={styles.channelsBox}>
                      <Image source={{uri: item.image}} style={{height: 60, width: 60, borderRadius: 100, borderColor: 'white', borderWidth: 6, resizeMode: 'cover'}} />
                      <Text style={{ marginTop: 3, fontFamily: family.condensed_regular, fontSize: 12, }}>{item.name}</Text>
                  </TouchableOpacity>
                )}
              /> : null}
            </SafeAreaView> 

            
          <View >
            <Text style={{color: '#262625', fontSize: 14, fontFamily: family.bold , marginBottom: 3, marginTop: 6, marginLeft: 5}}>
              Featured Videos
            </Text>
            <SafeAreaView style={{}}>
            {featuredData && featuredData.length >0 ?
              <FlatList
                data={featuredData}
                keyExtractor={(item, index)=> index.toString()}
                showsVerticalScrollIndicator={false}
                style={{marginLeft: 0}}
                renderItem={({item})=> 
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.id})}
                    style={styles.featuredBox}>
                      <ImageBackground
                        resizeMode={'cover'}
                        style={{
                          height: 220,
                          width: '100%'
                          }}
                        source={{uri: item.image}}>
                        <View style={{backgroundColor: 'black', borderWidth: 0.6, borderColor: '#fafafa', alignSelf: 'flex-end', margin: 3, opacity: 0.5}}>
                          <Text style={{color: '#fafafa', fontFamily: family.regular, paddingHorizontal: 5, paddingVertical: 3, fontSize: 12}}>{item.get_length}</Text>
                        </View>
                      </ImageBackground>  
                      <View style={{flexDirection: 'row'}}>
                        <Image style={{width: 50, height: 50, borderRadius: 100, marginLeft: 10, alignSelf: 'center', borderWidth: 1, borderColor: 'grey'}} source={{uri: item.channel.image}}/>
                        <View>
                          <Text style={{marginLeft: 10, marginTop: 10, fontFamily: family.regular}}>{item.name}</Text>
                          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
                            <Text style={{opacity: 0.7, marginVertical: 5, fontFamily: family.condensed_regular}}>
                              {item.channel.name}
                            </Text>
                            <Icon
                              name={'check-circle'}
                              size={12}
                              color={'grey'}
                              style={{marginHorizontal: 6}}
                            />
                          </View>
                          <View style={{flexDirection: 'row', marginLeft: 10, marginBottom: 10}}>
                            <View style={{flexDirection: 'row'}}>
                              <Icon name={'eye'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.views} views
                              </Text>
                            </View>

                            <View style={{flexDirection: 'row', marginLeft: 8}}>
                              <Icon name={'calendar'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.get_date}{' '}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>  
                      
                    </TouchableOpacity>
                }
              /> : null}
            </SafeAreaView>  
          </View>  
          
        </ScrollView>
      </SafeAreaView>
    );
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  upNextBox: {
    flexDirection: 'row',
    marginVertical: 8,
    backgroundColor: 'white',
    marginHorizontal: 15,
    // borderRadius: 4,
    overflow: 'hidden',
    // elevation:1 
  },
  channelsBox: {
    marginHorizontal: 9,
    borderRadius: 50,
    padding: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  featuredBox: {
    // height: 250, 
    marginVertical: 0,
    backgroundColor: 'white',
    // marginHorizontal: 20,
    overflow: 'hidden',
    // borderRadius: 10,
    // elevation: 1
  },
  videoContainer: {
    margin: 20
  },
  videoContainerBox: {
    marginTop: scale(0),
    width: '100%',
    height: 220,
    marginHorizontal: 0,
    // backgroundColor: 'black',
    overflow: 'hidden', alignItems: 'center', justifyContent: 'center',
    paddingTop: 10, elevation: 2
  },
  modalContent:{
    backgroundColor: 'white',
    // padding: scale(10),
    // justifyContent: 'center',
    // alignItems: 'center',
    marginHorizontal: 20,
    borderRadius: scale(8),
    borderColor: 'rgba(0, 0, 0, 0.3)',
    borderWidth: 0.6,
    alignItems: 'center'
  },
});

const mapStateToProps = ({ channels, category, ad_banner_1, service_category, featured, user_info, subscribe}) => {
  return { channels, category, ad_banner_1, service_category, featured, user_info, subscribe };
};

const mapDispatchToProps = dispatch => {
  return {
    getChannels: (params) => {
      dispatch(getChannels(params));
    },
    putSubscribe: (params) => {
      dispatch(putSubscribe(params));
    },
    getCategories: (params) => {
      dispatch(getCategories(params));
    },
    getAdBanner1: (params) => {
      dispatch(getAdBanner1(params));
    },
    getServiceCategories: (params) => {
      dispatch(getServiceCategories(params));
    },
    getFeaturedVideos: (params) => {
      dispatch(getFeaturedVideos(params));
    },
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HomeScreen);