import React, {Component} from 'react';
import {
  View,
  StyleSheet,
  Image,
  TouchableOpacity,
  Text,
  Dimensions,
  FlatList,
  ImageBackground,
  RefreshControl
} from 'react-native';
import Icon from 'react-native-vector-icons/Feather';
import {connect} from 'react-redux';
import {NavigationEvents, ScrollView} from 'react-navigation';
import Loader from '../components/Loader';
import AlertView from '../components/AlertView';
import {base_url} from '../constants/config';
import HeaderComponent from '../components/HeaderComponent';
import { scale } from '../constants/Scale';
import { family } from '../constants/Fonts';
import { getTrendingVideos } from '../actions/TrendingAction';
import { getAdBanner3 } from "../actions/AdActions";
import Colors from '../constants/Colors';

const {height, width} = Dimensions.get('window');

class TrendingScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      page: 1,
      refreshing:false,
    };
  }
  
  componentDidUpdate = () => {
    if (!this.props.trending.loading && this.state.refreshing){
      this.setState({refreshing:false})
    }
  };

  handleRefresh = () => {
    this.setState(
      {
        page: 1,
        refreshing: true,
      },
      () => {
        this.callPagination()
      }
    );
  };

  handleLoadMore = () => {
    this.setState(
      {
        page: this.state.page + 1
      },
      () => {
        this.callPagination()
      }
    );
  };

  callPagination(){
    let {page}=this.state;
    let params=[];
    params.push("page" + "=" + page);
    params = params.join("&");
    this.props.getTrendingVideos({params,page});
  }

  callApiFunctions() {
    this.props.getAdBanner3();
    this.callPagination();
  }

  render() {
    let adBanner = this.props.ad_banner_3 && this.props.ad_banner_3.data && this.props.ad_banner_3.data.banner3 ? this.props.ad_banner_3.data.banner3 : {};
    console.log("Trending : ", this.props.trending);
    const trendingData = this.props.trending && this.props.trending.trendingList && this.props.trending.trendingList.results ? this.props.trending.trendingList.results : [];

    let isLoading = this.props.ad_banner_3.loading || this.props.trending.loading;
    let isError = this.props.ad_banner_3.error ||  this.props.trending.error;
    return (
      <View style={styles.container}>
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
        <Loader loading={isLoading} />     
        <HeaderComponent navigation={this.props.navigation} />
          <ScrollView 
            showsVerticalScrollIndicator={false} 
            style={{}} 
            refreshControl={
              <RefreshControl
                colors= {["#0089D0"]}
                refreshing={this.state.refreshing}
                onRefresh={this.handleRefresh}
              />
            }>
          <Text
              style={{
                color: '#262625',
                fontSize: 14,
                fontFamily: family.bold,
                marginBottom: 3,
                margin: 10,
              }}>
              Trending
            </Text>
            <View style={{marginHorizontal: 15, marginTop: 0, elevation: 2.5, backgroundColor: 'white', marginVertical: 6}}>
              <Image style={{width: '100%', height: 120, resizeMode: 'center'}} source={{uri : adBanner.image}} />
              <View style={{paddingVertical: 4, marginHorizontal: 12}}>
                <View style={{flexDirection: 'row', alignItems: 'center'}}>
                  <View style={{backgroundColor: '#0089D0', padding: 2.5}}>
                    <Text style={{color: 'white', fontFamily: family.bold}}>Ad.</Text>
                  </View>
                  <Text style={{fontFamily: family.regular, color: 'grey', opacity: 0.9, marginHorizontal: 8, fontSize: 12}}>{adBanner.title}</Text>
                </View>
                <Text style={{fontFamily: family.regular, color: 'grey', opacity: 0.9, fontSize: 12}}>{adBanner.content}</Text>
              </View>
            </View>

          <View style={{}}>
            {trendingData && trendingData.length > 0 ?
            <FlatList
              data={trendingData}
              keyExtractor={(item, index) => index.toString()}
              showsVerticalScrollIndicator={false}
              style={{}}
              extraData={this.props}
              onEndReached={this.handleLoadMore}
              onEndReachedThreshold={0.1}    
              renderItem={({item})=> 
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.id})}
                    style={styles.featuredBox}>
                      <ImageBackground
                        resizeMode={'cover'}
                        style={{
                          height: 220,
                          width: '100%'
                          }}
                        source={{uri: item.image}}>
                        <View style={{backgroundColor: 'black', borderWidth: 0.6, borderColor: '#fafafa', alignSelf: 'flex-end', margin: 3, opacity: 0.5}}>
                          <Text style={{color: '#fafafa', fontFamily: family.regular, paddingHorizontal: 5, paddingVertical: 3, fontSize: 12}}>{item.get_length}</Text>
                        </View>
                      </ImageBackground>  
                      <View style={{flexDirection: 'row'}}>
                        <Image style={{width: 50, height: 50, borderRadius: 100, marginLeft: 10, alignSelf: 'center', borderWidth: 1, borderColor: 'grey'}} source={{uri: item.channel.image}}/>
                        <View>
                          <Text style={{marginLeft: 10, marginTop: 10, fontFamily: family.regular}}>{item.name}</Text>
                          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
                            <Text style={{opacity: 0.7, marginVertical: 5, fontFamily: family.condensed_regular}}>
                              {item.channel.name}
                            </Text>
                            <Icon
                              name={'check-circle'}
                              size={12}
                              color={'grey'}
                              style={{marginHorizontal: 6}}
                            />
                          </View>
                          <View style={{flexDirection: 'row', marginLeft: 10, marginBottom: 10}}>
                            <View style={{flexDirection: 'row'}}>
                              <Icon name={'eye'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.views} views
                              </Text>
                            </View>

                            <View style={{flexDirection: 'row', marginLeft: 8}}>
                              <Icon name={'calendar'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.get_date}{' '}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>  
                      
                    </TouchableOpacity>
                }
            /> : null }
          </View>
          </ScrollView>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa',
  },
  featuredBox: {
    marginVertical: 0,
    // elevation:1,
    backgroundColor: 'white',
    // marginHorizontal: 20,
    overflow: 'hidden',
    // borderRadius: 10,
  },

});

const mapStateToProps = ({ ad_banner_3, trending }) => {
  return { ad_banner_3, trending };
};

const mapDispatchToProps = dispatch => {
  return {
    getAdBanner3: (params) => {
      dispatch(getAdBanner3(params));
    },
    getTrendingVideos: (params) => {
      dispatch(getTrendingVideos(params));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TrendingScreen);
