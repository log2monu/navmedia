import React, { Component } from 'react';
import { View, StyleSheet, SafeAreaView, ScrollView, Text, TouchableOpacity, Image, FlatList, StatusBar } from 'react-native';
import HeaderComponent from '../components/HeaderComponent';
import { scale } from '../constants/Scale';
import Icon from 'react-native-vector-icons/Feather';
import { Container } from "../components/video-components";
import Icon1 from 'react-native-vector-icons/SimpleLineIcons';
import { TextInput } from 'react-native-gesture-handler';
import Video from "../components/video-components/Video"
import { family } from '../constants/Fonts';
import {connect} from "react-redux";
import Loader from '../components/Loader';
import { NavigationEvents } from "react-navigation";
import { getFeaturedVideos } from "../actions/TrendingAction";

class LiveVideoScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {

      isFullScreen: false
    };
    this.videoPlayer = React.createRef();
  }

  componentDidMount() {
    StatusBar.setHidden(true)
  }

  componentWillUnmount(){
    StatusBar.setHidden(true)
  }

  callApiFunctions() {
    this.props.getFeaturedVideos();
  }

  render() {
    // const url =
    //   `http://94krv9j2qb6p-hls-live.wmncdn.net/abr/612a5eb791b43209a8eccdde569e8ee1.m3u8`;
    const url =
      `https://94krv9j2qb6p-hls-live.wmncdn.net/abr/612a5eb791b43209a8eccdde569e8ee1/playlist.m3u8`;
    const { featured } = this.props;
    const featuredData = this.props.featured.featuredList && this.props.featured.featuredList.results ? this.props.featured.featuredList.results : [];
    let isLoading = featured.loading ? true : false;
    let isError =  featured.error ? true : false;
    return (
      
      <SafeAreaView style={{flex:1, backgroundColor: 'white'}}>
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
        <Loader loading={isLoading} /> 
        {/* <HeaderComponent navigation={this.props.navigation} /> */}
        <View  >
          <View style={styles.videoContainer}>
          <Container>
            <Video
              ref={videoPlayer => (this.videoPlayer = videoPlayer)}
              autoPlay
              url={url}
              rotateToFullScreen={true}
              onFullScreen={status => this.setState({isFullScreen: status})}
            />
            </Container>
          </View>

          <ScrollView showsVerticalScrollIndicator={false} style={{marginBottom: 200}}>
          <View style={{marginHorizontal: 0}}>
        <Text
          style={{
            marginLeft: 10,
            color: '#262625',
            fontSize: 14,
            fontFamily: family.medium,
            marginTop: 25,
            marginBottom: 5
          }}>
          Popular Videos
        </Text>

        {featuredData && featuredData.length >0 ?
              <FlatList
                data={featuredData}
                keyExtractor={(item, index)=> index.toString()}
                showsVerticalScrollIndicator={false}
                style={{marginLeft: 0}}
                renderItem={({item})=> 
                  <TouchableOpacity onPress={() => this.props.navigation.replace('VideoScreen', {"videoId": item.id})} style={styles.upNextBox}>
                    <Image
                      source={{uri: item.image}}
                      style={{
                        width: 122,
                        height: 77,
                      }}
                    />
                    <View
                      style={{
                        flexDirection: 'column',
                        marginHorizontal: 18,
                        justifyContent: 'center',
                      }}>
                      <Text style={{fontFamily: family.medium, width: '60%'}}>{item.name}</Text>
                      <View style={{flexDirection: 'row', alignItems: 'center'}}>
                        <Text style={{opacity: 0.7, marginVertical: 5, fontFamily: family.regular}}>
                          {item.channel.name}
                        </Text>
                        <Icon
                          name={'check-circle'}
                          size={12}
                          color={'grey'}
                          style={{marginHorizontal: 6}}
                        />
                      </View>
                      <View style={{flexDirection: 'row'}}>
                        <View style={{flexDirection: 'row'}}>
                          <Icon name={'eye'} color={'#949494'} size={13} />
                          <Text style={{color: '#949494', marginLeft: 4, fontSize: 12, fontFamily: family.light}}>
                            {item.views} views
                          </Text>
                        </View>

                        <View style={{flexDirection: 'row', marginLeft: 10}}>
                          <Icon name={'calendar'} color={'#949494'} size={13} />
                          <Text style={{color: '#949494', marginLeft: 4, fontSize: 12, fontFamily: family.light}}>
                            {item.get_date}{' '}
                          </Text>
                        </View>
                      </View>
                    </View>
                  </TouchableOpacity>
                }
              /> : null}
      </View>

          </ScrollView>
        </View>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
    videoContainer: {
      marginTop: scale(0),
      marginHorizontal: 0,
      backgroundColor: 'black',
      paddingBottom: scale(10)
    },
    subscribeButton: {
      borderRadius: 1,
      borderColor: '#0089D0',
      borderWidth: 1,
      marginVertical: scale(0),
      elevation: 1
    },
    upNextBox: {
      flexDirection: 'row',
      marginVertical: 8,
      backgroundColor: 'white',
      borderRadius: 4,
      overflow: 'hidden',
      marginHorizontal: 20
      // elevation:1 
    },
  })

  const mapStateToProps = ({ featured }) => {
    return {
      featured
    };
  };
  
  const mapDispatchToProps = dispatch => {
    return {
      getFeaturedVideos: (params) => {
        dispatch(getFeaturedVideos(params));
      }
    }
  }
  
  export default connect(
    mapStateToProps,
    mapDispatchToProps
  )(LiveVideoScreen);