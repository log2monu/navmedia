import React, { Component } from 'react';
import { View, TouchableOpacity, Button } from 'react-native';
// import {LivePlayer} from "react-native-live-stream";
import {  NodePlayerView, NodeCameraView } from 'react-native-nodemediaclient';
import Icon from 'react-native-vector-icons/Entypo';

export default class CreateChannelScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isPublish: false,
      publishBtnTitle: "Start Publish"
    };
  }

  async componentWillMount() {
    await requestCameraPermission();
    await requestStoragePermission();
  }

  async requestCameraPermission() 
    {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.CAMERA,
          {
            'title': 'Example App',
            'message': 'Example App access to your camera '
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the camera")
          alert("You can use the camera");
        } else {
          console.log("camera permission denied")
          alert("camera permission denied");
        }
      } catch (err) {
        console.warn(err)
      }
    }
  

  async requestStoragePermission() 
    {
      try {
        const granted = await PermissionsAndroid.request(
          PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
          {
            'title': 'Example App',
            'message': 'Example App access to your storage '
          }
        )
        if (granted === PermissionsAndroid.RESULTS.GRANTED) {
          console.log("You can use the storage")
          alert("You can use the storage");
        } else {
          console.log("storage permission denied")
          alert("storage permission denied");
        }
      } catch (err) {
        console.warn(err)
      }
    }

  render() {
    const url =  "rtmp://bangalore.restream.io/live/re_1535870_0190059267368801f150";
    return (
      <View style={{flex: 1}}>
        {/* <BroadcastView
          publish="rtmp://bangalore.restream.io/live/re_1535870_0190059267368801f150"
          cameraPosition="front"
        /> */}

        {/* <LivePlayer source={{uri: url}}
          ref={(ref) => {
              this.player = ref
          }}
          style={{flex: 0.9}}
          paused={false}
          muted={false}
          bufferTime={300}
          maxBufferTime={1000}
          resizeMode={"contain"}
          onLoading={()=>{}}
          onLoad={()=>{}}
          onEnd={()=>{}}
        /> */}

        {/* <NodePlayerView 
          style={{ height: 200 }}
          ref={(vp) => { this.vp = vp }}
          inputUrl={"rtmp://192.168.0.10/live/stream"}
          scaleMode={"ScaleAspectFit"}
          bufferTime={300}
          maxBufferTime={1000}
          autoplay={true}
        /> */}

        <NodeCameraView 
          style={{ flex: 1 }}
          ref={(vb) => { this.vb = vb }}
          outputUrl = {url}
          camera={{ cameraId: 1, cameraFrontMirror: true }}
          audio={{ bitrate: 32000, profile: 1, samplerate: 44100 }}
          video={{ preset: 12, bitrate: 400000, profile: 1, fps: 15, videoFrontMirror: false }}
          autopreview={true}
        />

        <TouchableOpacity
          style={{ borderRadius: 100, borderWidth: 2, borderColor:'#999999', padding: 10, alignSelf:'center'}}
          onPress={() => {
            if (this.state.isPublish) {
              this.setState({ publishBtnTitle: 'Start Publish', isPublish: false });
              this.vb.stop();
            } else {
              this.setState({ publishBtnTitle: 'Stop Publish', isPublish: true });
              this.vb.start();
            }
          }}
        >
          {this.state.isPublish ? 
          <Icon name={'controller-stop'} color={'#e04638'} size={30} /> :
          <Icon name={'controller-play'} color={'#21b2de'} size={30} />}

        </TouchableOpacity>

      </View>
    );
  }
}
