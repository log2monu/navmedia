import React, { Component } from 'react';
import { TouchableOpacity, Image, View, Text, StyleSheet, Dimensions, SafeAreaView, ScrollView, FlatList, TextInput, ImageBackground } from 'react-native';
import HeaderComponent from '../components/HeaderComponent';
import Icon from 'react-native-vector-icons/Feather';
import Icon1 from 'react-native-vector-icons/Entypo';
import Icon3 from 'react-native-vector-icons/MaterialCommunityIcons';
import { scale } from '../constants/Scale';
import { family } from '../constants/Fonts';
import Modal from "react-native-modal";
import FloatingLabel from "react-native-floating-labels";
import ImagePicker from 'react-native-image-crop-picker';
import MultiSelect from 'react-native-multiple-select';
import {connect} from "react-redux";
import { postBannerAd, postVideoAd } from '../actions/AdActions';
import Loader from '../components/Loader';
import { NavigationEvents } from "react-navigation";
import { getFeaturedVideos } from "../actions/TrendingAction";
import { getProfile } from '../actions/ProfileAction';
import { postCreateChannel } from '../actions/ChannelsAction';
import Toast from 'react-native-simple-toast';
// import fileType from "react-native-file-type";

const {height, width} = Dimensions.get('window');

class ProfileScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isChannelModalVisible: false,
      isAdModalVisible: false,
      isBannerAdModalVisible: false,
      channel_name: "",
      channel_about: "",
      logoImage: null,
      channelBanner: null,
      videoAdTitle: "",
      videoAdDesc: "",
      video: null,
      videoImage: null,
      videoExtention: null,
      bannerTitle: "",
      bannerDesc: "",
      bannerImage: null,
      selectedAdCategories: [],
      adCategory: [
        {category: "Sports", id: "1"},
        {category: "Health", id: "2"},
        {category: "News", id: "3"},
        {category: "Climate", id: "4"},
        {category: "Entertainmet", id: "5"},
        {category: "Music", id: "6"},
      ],
      isMenuPressed: false,
      isMenuOpen: false,
      isPremium: this.props.premium
    };
  }

  callApiFunctions() {
    this.props.fetchProfile();
    this.props.getFeaturedVideos();
  }

  componentDidUpdate(prevProps) {
    if (this.props.create_channel) {
      if (this.props.create_channel.success &&
          !this.props.create_channel.loading &&
          !this.props.create_channel.error) {
            if(prevProps.create_channel != this.props.create_channel){
              this.toggleChannelModal();
              Toast.showWithGravity('Channel created successfully.', Toast.LONG, Toast.CENTER)
            }     
      }
    }
  }
    
  onSubscribe =(subscribe) => {
    this.setState({
      subscribed: !subscribe
    })
  }

  pickLogoImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        logoImage: image.path
      });
      // console.log("cdbjskv", image)
    }).catch(e => alert(e));
  }

  pickBannerImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        channelBanner: image.path
      });
    }).catch(e => alert(e));
  }

  pickAdVideoImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        videoImage: image.path
      });
    }).catch(e => alert(e));
  }
  pickAdBannerImage=()=> {
    ImagePicker.openPicker({
      multiple: false,
      waitAnimationEnd: false,
      includeExif: true,
      forceJpg: true,
      compressImageQuality: 0.8,
      mediaType: 'photo'
    }).then(image => {
      this.setState({
        bannerImage: image.path
      });
    }).catch(e => alert(e));
  }

  render() {
    const { ad_upload, banner_upload, create_channel, featured, profile } = this.props;
    const featuredData = this.props.featured.featuredList && this.props.featured.featuredList.results ? this.props.featured.featuredList.results : [];
    let isLoading = profile.loading || ad_upload.loading || banner_upload.loading || create_channel.loading || featured.loading ? true : false;
    let isError = profile.error || ad_upload.error || banner_upload.error || create_channel.error || featured.error ? true : false;
    console.log("Prooofille :" , this.props.profile) 
    const userName = profile && profile.userData && profile.userData.profile && profile.userData.profile.user ? profile.userData.profile.user.username : "";
    return (
      <SafeAreaView style={{backgroundColor: '#fafafa', flex: 1}}>
        <NavigationEvents onDidFocus={(payload)=> this.callApiFunctions()} />
        <Loader loading={isLoading} /> 
        <HeaderComponent navigation={this.props.navigation}/>
        <ScrollView showsVerticalScrollIndicator={false}>

          <View style={{flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center', marginVertical: 15}}>
            <TouchableOpacity style={{alignItems: 'center', justifyContent :'center'}} onPress={()=> this.setState({isChannelModalVisible: !this.state.isChannelModalVisible})}>
              <Image source={require("../assets/create_channel.png")} style={{width: 65, height: 65}} />
              <Text style={{fontFamily:  family.condensed_bold, fontSize: 12}}>Create Channel</Text>
            </TouchableOpacity>
            <TouchableOpacity style={{alignItems: 'center', justifyContent :'center'}} onPress={()=> this.props.navigation.navigate("MyChannel")}>
              <Image source={require("../assets/mychannels.png")} style={{width: 65, height: 65}} />
              <Text style={{fontFamily:  family.condensed_bold, fontSize: 12}}>My Channels</Text>
            </TouchableOpacity>
          </View>

          <View style={{  backgroundColor: 'white',  marginHorizontal: scale(15), paddingHorizontal: 12, paddingVertical: 10, borderRadius: 3, elevation: 2, marginVertical: 10}}>
            <View style={{flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center'}}>
              <View style={{flexDirection: 'row', alignItems: 'center'}}>
                <Text style={{ fontSize: scale(14), marginRight: scale(6), fontFamily: family.regular}}>{userName}</Text>
                <Icon
                  name={'check-circle'}
                  size={16}
                  color={'#0089D0'}
                />
              </View>
              <TouchableOpacity 
                style={{borderColor: 'grey', borderWidth: this.state.isMenuPressed ? scale(0.7) : scale(1.5), borderRadius: 4}}
                onPress={()=> this.setState({ isMenuPressed: !this.state.isMenuPressed})}
              >
                <Icon
                  name={'menu'}
                  size={24}
                  color={'grey'}
                  style={{marginHorizontal: 3, marginVertical: 1}}
                />
              </TouchableOpacity>  
            </View>
            

            {this.state.isMenuPressed ? (<View style={{marginVertical: 15}}>
              <TouchableOpacity
                onPress={()=> this.props.navigation.navigate("HistoryScreen")}
                style={{marginTop: 5, marginBottom: 9}}
              >
                <Text style={{ marginBottom: 5, fontSize: 14, fontFamily: family.condensed_regular}}>Watch history</Text>
                <View style={{height: 0.5, backgroundColor: '#f5f5f5', width: '100%'}}/>
              </TouchableOpacity>  

              <TouchableOpacity
                onPress={()=> this.props.navigation.navigate("Settings")}
                style={{marginTop: 10}}
              >
                <Text style={{ marginBottom: 5, fontSize: 14, fontFamily: family.condensed_regular}}>Account Settings</Text>
                <View style={{height: 0.5, backgroundColor: '#f5f5f5', width: '100%'}}/>
              </TouchableOpacity>   
            </View>) : (null)}
            
          </View>  

          <View style={{height: 0.4, width: '100%', backgroundColor: '#ababab', marginVertical: 20}}/>

          {this.renderMyVideos(featuredData)}

          <View style={{height: 0.4, width: '100%', backgroundColor: '#ababab', marginVertical: 20}}/>

          {this.renderChannelModal()}


        </ScrollView>
      </SafeAreaView>
    );
  }

  renderMyVideos(featuredData) {
    return (
      <View style={{marginHorizontal: 0}}>
        <Text
          style={{
            marginLeft: 10,
            color: '#262625',
            fontSize: 14,
            fontFamily: family.medium
          }}>
          My Videos
        </Text>

        {featuredData && featuredData.length >0 ?
              <FlatList
                data={featuredData}
                keyExtractor={(item, index)=> index.toString()}
                showsVerticalScrollIndicator={false}
                style={{marginLeft: 0}}
                renderItem={({item})=> 
                  <TouchableOpacity
                    onPress={() => this.props.navigation.navigate('VideoScreen', {"videoId": item.id})}
                    style={styles.featuredBox}>
                      <ImageBackground
                        resizeMode={'cover'}
                        style={{
                          height: 220,
                          width: '100%'
                          }}
                        source={{uri: item.image}}>
                        <View style={{backgroundColor: 'black', borderWidth: 0.6, borderColor: '#fafafa', alignSelf: 'flex-end', margin: 3, opacity: 0.5}}>
                          <Text style={{color: '#fafafa', fontFamily: family.regular, paddingHorizontal: 5, paddingVertical: 3, fontSize: 12}}>{item.get_length}</Text>
                        </View>
                      </ImageBackground>  
                      <View style={{flexDirection: 'row'}}>
                        <Image style={{width: 50, height: 50, borderRadius: 100, marginLeft: 10, alignSelf: 'center', borderWidth: 1, borderColor: 'grey'}} source={{uri: item.channel.image}}/>
                        <View>
                          <Text style={{marginLeft: 10, marginTop: 10, fontFamily: family.regular}}>{item.name}</Text>
                          <View style={{flexDirection: 'row', alignItems: 'center', marginLeft: 10}}>
                            <Text style={{opacity: 0.7, marginVertical: 5, fontFamily: family.condensed_regular}}>
                              {item.channel.name}
                            </Text>
                            <Icon
                              name={'check-circle'}
                              size={12}
                              color={'grey'}
                              style={{marginHorizontal: 6}}
                            />
                          </View>
                          <View style={{flexDirection: 'row', marginLeft: 10, marginBottom: 10}}>
                            <View style={{flexDirection: 'row'}}>
                              <Icon name={'eye'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.views} views
                              </Text>
                            </View>

                            <View style={{flexDirection: 'row', marginLeft: 8}}>
                              <Icon name={'calendar'} color={'#949494'} size={15} />
                              <Text style={{color: '#949494', marginLeft: 5, fontFamily: family.condensed_thin}}>
                                {item.get_date}{' '}
                              </Text>
                            </View>
                          </View>
                        </View>
                      </View>  
                      
                    </TouchableOpacity>
                }
              /> : null}
      </View>
    )
  }

  renderChannelModal() {
    return (
      <Modal
        visible={this.state.isChannelModalVisible}
        onBackButtonPress={()=>this.toggleChannelModal()}
        onBackdropPress={()=> this.toggleChannelModal()}
        backdropColor="black"
        backdropOpacity={0.4}
        // style={{flex: 1, backgroundColor: 'grey'}}
        style={{justifyContent: 'center',backgroundColor: 'rgba(0,0,0,0.7)', margin: 0}}
      >
        <View style={styles.modalContent}>
        <Text style={{fontFamily: family.bold, fontSize: scale(14), marginVertical: scale(15), color: '#0089D0'}}>Create channel</Text>
        { this.props.premium ? 
          <View >
            <Icon name={"tv"} color={"#0089D0"} size={35} style={{alignSelf: 'center'}}/>
            <View style={{paddingHorizontal: 45, paddingTop: 15}}>
              <Text style={{color: 'grey', fontFamily: family.regular}}>Create channel(s) for uploading videos.</Text>
            </View>
          </View>
           : null}
        
        {this.props.premium ?
          (
          <View style={{width: '80%', alignItems: 'center'}}>
        
          <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon3 name={"format-title"} color={'grey'} size={20} />
              <TextInput
                placeholder={'Channel name'}
                placeholderTextColor={'grey'}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
                onChangeText={(value)=> this.setState({ channel_name: value})}
              />  

            </View>
            <View style={{flexDirection: 'row', alignItems: 'center'}}>
              <Icon name={"file-text"} color={'grey'} size={20} />
              <TextInput
                placeholder={'About channel'}
                placeholderTextColor={'grey'}
                style={{width: '80%', borderColor: '#cfcfcf', borderBottomWidth: 0.7, marginHorizontal: 10, fontFamily: family.regular, fontSize: scale(14)}}
                onChangeText={(value)=> this.setState({ channel_about: value})}
              />  

            </View>  
          <View style={{ width : '100%', justifyContent: 'space-between', paddingHorizontal:  35, marginTop: 10}}>
          <TouchableOpacity 
                onPress={() => this.pickLogoImage()}
                style={{  flexDirection : 'row', borderRadius: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', width: '100%', paddingVertical: 8, marginVertical: 10, elevation: 1 }}
              >
                <Icon name={"image"} color={'grey'} size={16} />
                <Text style={{  color: 'grey', fontFamily: family.regular, marginLeft: 6}}>Select logo</Text>
              </TouchableOpacity> 
            {this.state.logoImage &&  
              <Image  
                  style={{width: '100%', height: 100, borderRadius: 4, alignSelf: 'center'}}
                  source={{uri: this.state.logoImage}}
                /> } 
          </View>

          <View style={{ width : '100%', justifyContent: 'space-between', paddingHorizontal:  35, marginTop: 10}}>
          <TouchableOpacity 
                onPress={() => this.pickBannerImage()}
                style={{  flexDirection : 'row', borderRadius: 5, backgroundColor: '#fff', alignItems: 'center', justifyContent: 'center', width: '100%', paddingVertical: 8, marginVertical: 10, elevation: 1 }}
              >
                <Icon name={"image"} color={'grey'} size={16} />
                <Text style={{  color: 'grey', fontFamily: family.regular, marginLeft: 6}}>Select banner</Text>
              </TouchableOpacity> 
            {this.state.channelBanner && 
              <Image  
                  style={{width: '100%', height: 100, borderRadius: 4, alignSelf: 'center'}}
                  source={{uri: this.state.channelBanner}}
                /> } 
          </View>
          
          <TouchableOpacity
            onPress={() => this.postCreateChannel()}
            style={{
                borderRadius: 5,
                width: '80%',
                marginVertical: scale(15),
                alignItems: 'center',
                alignSelf: 'center',
                justifyContent: 'center',
                backgroundColor: 'white',
                paddingVertical: 4,
                flexDirection: 'row',
                elevation: 1
              }}>
              <Text style={{color: '#0089D0', fontSize: scale(12), paddingHorizontal: scale(10), paddingVertical: scale(6), fontFamily: family.bold }}>Create</Text>
            </TouchableOpacity> 
          </View>   ) :
          (
            <View style={{}}>
              <Text style={{
                fontFamily: family.regular,                  
                  paddingHorizontal: scale(15),
                  paddingVertical: scale(6),
              }}>Only Premium members can create channel.</Text>
              <TouchableOpacity
              style={{
                borderRadius: 3,
                backgroundColor: 'white',
                borderColor: 'grey',
                elevation: 2,
                marginVertical: 20,
                alignItems: 'center'
              }}
              onPress={() => {
                this.toggleChannelModal()
                this.props.navigation.navigate("Settings")             
              }}>
              <Text
                style={{
                  fontFamily: family.condensed_bold,                  
                  paddingHorizontal: scale(15),
                  paddingVertical: scale(6),
                }}>
                Go Premium
              </Text>
            </TouchableOpacity>
            </View>
          )}
       
        </View>
      </Modal>
    )
  }

  postCreateChannel = () => {
    const {channelBanner, logoImage, channel_name, channel_about } = this.state;
    const logoImageData = {uri: logoImage, name: "banner.jpeg", type: 'image/jpeg'};
    const bannerImageData = {uri: channelBanner, name: "banner.jpeg", type: 'image/jpeg'};
    const data = {
      name: channel_name,
      about: channel_about,
      image: logoImageData,
      banner: bannerImageData
    };
    let formBody = new FormData();
    for (let key in data) {
      formBody.append(key, data[key]);
    }

    this.props.postChannelCreate(formBody);
    
  }

  toggleChannelModal = () => {
    this.setState({ isChannelModalVisible: !this.state.isChannelModalVisible });
  };

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fafafa'
  },
  featuredBox: {
    marginVertical: 0,
    // elevation:1,
    backgroundColor: 'white',
    overflow: 'hidden',
    // borderRadius: 10,
  },
  modalContent:{
    backgroundColor: 'white',
    // padding: scale(10),
    // justifyContent: 'center',
    // alignItems: 'center',
    marginHorizontal: 20,
    borderRadius: scale(8),
    borderColor: 'rgba(0, 0, 0, 0.3)',
    borderWidth: 0.6,
    alignItems: 'center'
  },

})



const mapStateToProps = ({ banner_upload, create_channel, ad_upload, featured, user_info, profile, premium }) => {
  return {
    banner_upload,
    ad_upload,
    create_channel,
    featured,
    user_info,
    profile,
    premium
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchProfile: (postData) => {
      dispatch(getProfile(postData));
    },
    postChannelCreate: (postData) => {
      dispatch(postCreateChannel(postData));
    },
    getFeaturedVideos: (params) => {
      dispatch(getFeaturedVideos(params));
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfileScreen);