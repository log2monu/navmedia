import React from "react";
import { Provider } from "react-redux";
import { View, StyleSheet, StatusBar, Platform } from "react-native";
import initStore from "./src/store/configure-store";
import { SafeAreaView } from "react-navigation";
import AppNavigator from "./src/navigation/app-navigator";
import NavigationService from "./src/navigation/navigation-service";
import { PersistGate } from "redux-persist/integration/react";
import { getStatusBarHeight } from "react-native-status-bar-height";
import Colors from "./src/constants/Colors";
import Icon from 'react-native-vector-icons/MaterialIcons';
import { MenuProvider } from 'react-native-popup-menu';

const statusBarHeight = getStatusBarHeight();
const { store, persistor } = initStore();
const STATUS_BAR_HEIGHT = Platform.OS === "ios" ? 45 : StatusBar.currentHeight;

Icon.loadFont();

export default App = () => {
  console.disableYellowBox = true;
  return(
    <Provider store={store}>
      <PersistGate persistor={persistor} loading={null}>
        <SafeAreaView
          style={{ flex: 1 }}
          forceInset={{ bottom: "never", top: "never" }}
        >
          {Platform.OS === "ios" && (
            <View
              style={{
                width: "100%",
                height: STATUS_BAR_HEIGHT,
                backgroundColor: Colors.primaryColor,
                opacity: 0.9
              }}
            >
              <StatusBar translucent backgroundColor="transparent" barStyle="light-content"  />
            </View>
          )}
          <StatusBar  backgroundColor="#0089D0" barStyle="light-content" />
          {/* <StatusBar backgroundColor= {Colors.primaryColor} 
            barStyle="light-content" /> */}
          <MenuProvider>
            <AppNavigator
              ref={navigatorRef => {
                NavigationService.setNavigator(navigatorRef);
              }}
            /> 
          </MenuProvider>
        </SafeAreaView>
      </PersistGate>
    </Provider>
  )
};

const styles = StyleSheet.create({
  statusBar: {
    backgroundColor: "black",
    height: statusBarHeight
  }
});

